package app.ayo.golf;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.ArrayList;

import app.ayo.golf.adapter.CourseAdapter;
import app.ayo.golf.retrofit.pojo.Course;
import app.ayo.golf.retrofit.pojo.ResponseServer;
import app.ayo.golf.retrofit.service.ApiServices;
import app.ayo.golf.retrofit.service.InitRetrofit;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends AppCompatActivity {

    @BindView(R.id.rvcourse)
    RecyclerView rvcourse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);

        //untuk menunggu server response, supaya user tdk bingung saat proses berjalan
        final ProgressDialog dialog = new ProgressDialog(MapsActivity.this);
        //set message dialog
        dialog.setMessage("loading");
        //supaya gabisa di cancel
        dialog.setCancelable(false);
        //tampilkan
        dialog.show();

        //get konfigurasi dari retrofit
        ApiServices api = InitRetrofit.getInstanceRetrofit();

        //get request
        Call<ResponseServer> call = api.get_course();

        //get response
        call.enqueue(new Callback<ResponseServer>() {
            @Override
            public void onResponse(Call<ResponseServer> call, Response<ResponseServer> response) {
                dialog.dismiss();
                if (response.isSuccessful()){
                    ArrayList<Course> data = response.body().getCourse();
                    CourseAdapter ar = new CourseAdapter(data, rvcourse, MapsActivity.this);
                    rvcourse.setAdapter(ar);
                    rvcourse.setLayoutManager(new LinearLayoutManager(MapsActivity.this));
                }
                else{
                    Toast.makeText(MapsActivity.this, response.errorBody().toString(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseServer> call, Throwable t) {

            }
        });


    }
}
