package app.ayo.golf;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonArray;

import java.util.ArrayList;

import app.ayo.golf.adapter.BookingItemsAdapter;
import app.ayo.golf.helper.SessionManager;
import app.ayo.golf.retrofit.pojo.APIResponse;
import app.ayo.golf.retrofit.pojo.BookingCourseResult;
import app.ayo.golf.retrofit.pojo.Golfer;
import app.ayo.golf.retrofit.service.ApiServices;
import app.ayo.golf.retrofit.service.InitRetrofit;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Haries on 23/11/17.
 */

public class InProgressFragment extends Fragment {
    @BindView(R.id.rv_booking_progress)
    RecyclerView mRvProgress;
    @BindView(R.id.ll_empty)
    View mLlEmpty;

    Unbinder unbinder;
    boolean isViewActive;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_in_progress, container, false);

        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        initRecycleView();
        getTransaction();
    }

    private void getTransaction() {
        Golfer golfer = SessionManager.getProfile(getContext());
        if (golfer == null)
            return;

        JsonArray array = new JsonArray();
        array.add("unpaid");
        ApiServices api = InitRetrofit.getInstanceRetrofit();
        Call<APIResponse<ArrayList<BookingCourseResult>>> call = api.transactHistory(golfer.golfer_id, array.toString());
        call.enqueue(new Callback<APIResponse<ArrayList<BookingCourseResult>>>() {
            @Override
            public void onResponse(Call<APIResponse<ArrayList<BookingCourseResult>>> call, Response<APIResponse<ArrayList<BookingCourseResult>>> response) {
                if (response.isSuccessful()
                        && response.body().isSuccessful()
                        && isViewActive) {

                    // update ui
                    ((BookingItemsAdapter) mRvProgress.getAdapter()).setItems(response.body().data);
                    showEmptyView();
                } else {
                    // TODO error handler
                }
            }

            @Override
            public void onFailure(Call<APIResponse<ArrayList<BookingCourseResult>>> call, Throwable t) {
                // TODO error handler
            }
        });
    }

    private void initRecycleView() {
        mRvProgress.setLayoutManager(
                new LinearLayoutManager(mRvProgress.getContext(), LinearLayoutManager.VERTICAL, false));
        BookingItemsAdapter adapter = new BookingItemsAdapter();
        mRvProgress.setAdapter(adapter);

        showEmptyView();
    }

    private void showEmptyView() {
        if (((BookingItemsAdapter) mRvProgress.getAdapter()).isEmpty()) {
            mLlEmpty.setVisibility(View.VISIBLE);
            mRvProgress.setVisibility(View.GONE);
        } else {
            mLlEmpty.setVisibility(View.GONE);
            mRvProgress.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onPause() {
        isViewActive = false;
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        isViewActive = true;
    }
}
