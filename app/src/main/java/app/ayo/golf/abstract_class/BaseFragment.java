package app.ayo.golf.abstract_class;

import android.support.v4.app.Fragment;

/**
 * Created by Hendi on 08/21/18.
 */
public class BaseFragment extends Fragment {
    protected boolean isViewActive;

    @Override
    public void onStart() {
        super.onStart();
        isViewActive = true;
    }

    @Override
    public void onStop() {
        isViewActive = false;
        super.onStop();
    }
}
