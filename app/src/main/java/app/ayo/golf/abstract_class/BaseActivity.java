package app.ayo.golf.abstract_class;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Hendi on 08/21/18.
 */
public class BaseActivity extends AppCompatActivity{
    protected boolean isViewActive;

    @Override
    public void onStart() {
        super.onStart();
        isViewActive = true;
    }

    @Override
    public void onStop() {
        isViewActive = false;
        super.onStop();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
