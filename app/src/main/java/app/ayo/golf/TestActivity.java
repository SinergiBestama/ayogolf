package app.ayo.golf;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.ArrayList;

import app.ayo.golf.adapter.BannerAdapter;
import app.ayo.golf.helper.SessionManager;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.pojo.APIResponse;
import app.ayo.golf.retrofit.pojo.Banner;
import app.ayo.golf.retrofit.pojo.Golfer;
import app.ayo.golf.retrofit.service.ApiServices;
import app.ayo.golf.retrofit.service.InitRetrofit2;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class TestActivity extends AppCompatActivity {

    @BindView(R.id.vpbanner)
    ViewPager vpbanner;
    @BindView(R.id.circleindicator)
    CircleIndicator circleindicator;
    Golfer mOwn;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_test);

        ButterKnife.bind(this);

        Util.setCustomActionBar(this)
                .setTitle(R.string.app_name)
                .setLeftButton(null, null)
                .setRightButton(null, null)
                .setTransparent(true)
                .enableElevation(true);

        mOwn = SessionManager.getProfile(this);

        ApiServices api = InitRetrofit2.getInstanceRetrofit();

        Call<APIResponse<ArrayList<Banner>>> call = api.get_banner();
        call.enqueue(new Callback<APIResponse<ArrayList<Banner>>>() {
            @Override
            public void onResponse(Call<APIResponse<ArrayList<Banner>>> call, Response<APIResponse<ArrayList<Banner>>> response) {
                if (response.isSuccessful() && response.body().isSuccessful()) {
                    Log.d("TestActivity", "Succes");
                    ArrayList<Banner> data = response.body().data;
                    BannerAdapter mAdapter = new BannerAdapter(data, TestActivity.this);
                    vpbanner.setAdapter(mAdapter);
                    vpbanner.setPageMargin((int) Util.convertDpToPixel(5f, TestActivity.this));
                    vpbanner.setPageMarginDrawable(android.R.color.white);
                    circleindicator.setViewPager(vpbanner);
                    mAdapter.registerDataSetObserver(circleindicator.getDataSetObserver());
                }
            }

            @Override
            public void onFailure(Call<APIResponse<ArrayList<Banner>>> call, Throwable t) {

            }
        });

        vpbanner.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }
}
