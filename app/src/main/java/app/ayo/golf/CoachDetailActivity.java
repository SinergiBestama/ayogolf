package app.ayo.golf;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.pojo.Coach;
import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Haries on 11/9/17.
 */

public class CoachDetailActivity extends AppCompatActivity {
    public final static String EXTRA_COACH_DETAIL = "EXTRA_COACH_DETAIL";
    Coach mCoach;

    @BindView(R.id.iv_coach) ImageView mIvCoach;
    @BindView(R.id.tv_coach_name) TextView mTvCoachName;
    @BindView(R.id.tv_coach_title) TextView mTvCoachTitle;
    @BindView(R.id.tv_coach_desc) TextView mTvCoachDesc;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        setContentView(R.layout.activity_coach_detail);
        ButterKnife.bind(this);

        //init actionbar
        Util.setCustomActionBar(this)
                .setTitle(null)
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                })
                .setRightButton(null, null)
                .setTransparent(true);

        mCoach = getIntent().getParcelableExtra(EXTRA_COACH_DETAIL);

        if(mCoach != null) {
            mTvCoachName.setText(mCoach.coach_name);
            mTvCoachTitle.setText(mCoach.coach_title);
            mTvCoachDesc.setText(mCoach.coach_desc);
            RequestOptions opt = RequestOptions.placeholderOf(R.drawable.default_picture);
            Glide.with(this)
                    .load(mCoach.coach_image)
                    .apply(opt)
                    .into(mIvCoach);
        }
    }
}
