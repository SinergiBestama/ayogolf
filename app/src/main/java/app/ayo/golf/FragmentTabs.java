package app.ayo.golf;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by arifina on 9/22/2017.
 */

public class FragmentTabs extends FragmentStatePagerAdapter {
    public FragmentTabs(FragmentManager supportFragmentManager) {
        super(supportFragmentManager);

    }

    @Override
    public Fragment getItem(int position) {
        if(position==0){
            return new CoachTipsFragment();
        }
        else if(position==1){
            return new CoachBookFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
