
package app.ayo.golf;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import java.util.ArrayList;

import app.ayo.golf.adapter.CCAdapter;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.pojo.APIResponse;
import app.ayo.golf.retrofit.pojo.CoachingClinic;
import app.ayo.golf.retrofit.pojo.Tutorial;
import app.ayo.golf.retrofit.service.ApiServices;
import app.ayo.golf.retrofit.service.InitRetrofit;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CCVideoActivity extends AppCompatActivity {

    @BindView(R.id.video)
    VideoView video;
    @BindView(R.id.txtjudul)
    TextView txtjudul;
    @BindView(R.id.rvccvideo2)
    RecyclerView rvccvideo2;

    Tutorial tutorial;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_ccvideo);
        ButterKnife.bind(this);

        //init actionbar
        Util.setCustomActionBar(this)
                .setTitle("Tips")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                })
                .setRightButton(null, null);

        tutorial = getIntent().getParcelableExtra("tutorial");
        String titles = tutorial.title;
        String videos = tutorial.video;


        txtjudul.setText(titles);
        video.setVideoPath(videos);
        video.setMediaController(new MediaController(this));
        video.requestFocus();
        video.start();

        //untuk menunggu server response, supaya user tdk bingung saat proses berjalan
        final ProgressDialog dialog = new ProgressDialog(CCVideoActivity.this);
        //set message dialog
        dialog.setMessage("loading");
        //supaya gabisa di cancel
        dialog.setCancelable(false);
        //tampilkan
        dialog.show();

        //get konfigurasi dari retrofit
        ApiServices api = InitRetrofit.getInstanceRetrofit();

        Call<APIResponse<CoachingClinic>> call = api.get_cctutorial();

        call.enqueue(new Callback<APIResponse<CoachingClinic>>() {
            @Override
            public void onResponse(Call<APIResponse<CoachingClinic>> call, Response<APIResponse<CoachingClinic>> response) {
                if (response.isSuccessful()) {
                    dialog.dismiss();
                    ArrayList<Tutorial> data = response.body().data.tutorial;
                    CCAdapter ar = new CCAdapter(CCVideoActivity.this, rvccvideo2, data);
                    rvccvideo2.setAdapter(ar);
                    rvccvideo2.setLayoutManager(new LinearLayoutManager(CCVideoActivity.this));
                }

            }

            @Override
            public void onFailure(Call<APIResponse<CoachingClinic>> call, Throwable t) {

            }
        });
    }
}