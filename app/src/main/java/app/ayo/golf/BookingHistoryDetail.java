package app.ayo.golf;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.pojo.APIResponse;
import app.ayo.golf.retrofit.pojo.BookingCourseDetails;
import app.ayo.golf.retrofit.pojo.BookingCourseResult;
import app.ayo.golf.retrofit.pojo.BookingCourseTransaction;
import app.ayo.golf.retrofit.service.InitRetrofit;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BookingHistoryDetail extends AppCompatActivity {

    @BindView(R.id.txtcourse2)
    TextView txtcourse1;
    @BindView(R.id.txtbookdate2)
    TextView txtbookdate1;
    @BindView(R.id.txtflight2)
    TextView txtflight1;
    @BindView(R.id.txtcaddy2)
    TextView txtcaddy1;
    @BindView(R.id.iv_caddy_pic2)
    CircleImageView ivCaddyPic1;
    @BindView(R.id.txttotal2)
    TextView txttotal1;
    @BindView(R.id.txtplayer2)
    TextView txtplayer1;
    @BindView(R.id.btnconfirm2)
    View btnconfirm1;
    @BindView(R.id.rv_flight_detail2)
    RecyclerView rvFlightDetail1;

    public BookingCourseDetails bookingCourseDetails = new BookingCourseDetails();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_history_detail);

        ButterKnife.bind(this);
        Util.setCustomActionBar(this)
                .setTitle("Detail Booking")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                })
                .setRightButton(null, null);

//        final BookingCourseResult detailBooking = getIntent().getParcelableExtra("transaction");

        txtcourse1.setText(bookingCourseDetails.course_name);
        txtbookdate1.setText(bookingCourseDetails.booked_date);
        txtcaddy1.setText(bookingCourseDetails.caddy);


        loadDetailBooking();

//        txtcourse1.append(detailBooking.course_name);
    }

    private void loadDetailBooking() {

//        Call<APIResponse<BookingCourseDetails>> call = InitRetrofit.getInstanceRetrofit().bookedDetail("", "", "", "", "","");
//
//        call.enqueue(new Callback<APIResponse<BookingCourseDetails>>() {
//            @Override
//            public void onResponse(Call<APIResponse<BookingCourseDetails>> call, Response<APIResponse<BookingCourseDetails>> response) {
//                if (response.body().isSuccessful()) {
//                    Toast.makeText(BookingHistoryDetail.this, "OK", Toast.LENGTH_LONG).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<APIResponse<BookingCourseDetails>> call, Throwable t) {
//
//            }
//        });
//        Call<APIResponse<ArrayList<BookingCourseResult>>> call = InitRetrofit.getInstanceRetrofit().transactHistory("golfer_id", "payment_status");
//        call.enqueue(new Callback<APIResponse<ArrayList<BookingCourseResult>>>() {
//            @Override
//            public void onResponse(Call<APIResponse<ArrayList<BookingCourseResult>>> call, Response<APIResponse<ArrayList<BookingCourseResult>>> response) {
//                int status = response.code();
//                APIResponse<ArrayList<BookingCourseResult>> response1 = response.body();
//                APIResponse<ArrayList<BookingCourseResult>> courseResult = response1;
//                txtcourse1.append(courseResult.code);
//
//                Log.d("Respon", "OK" + status);
//            }
//
//            @Override
//            public void onFailure(Call<APIResponse<ArrayList<BookingCourseResult>>> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "Response Gagal :" + t, Toast.LENGTH_LONG).show();
//            }
//        });

    }

    public void toPayment(View view) {
        Intent paymentIntent = new Intent(this, PaymentActivity.class);
        startActivity(paymentIntent);
    }
}
