package app.ayo.golf;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CourseActivity extends AppCompatActivity {

    @BindView(R.id.textView40)
    TextView textView40;
    @BindView(R.id.buttonbookpalmhill)
    Button buttonbookpalmhill;
    @BindView(R.id.imageView6)
    ImageView imageView6;
    @BindView(R.id.textView31)
    TextView textView31;
    @BindView(R.id.textView41)
    TextView textView41;
    @BindView(R.id.buttondet81)
    Button buttondet81;
    @BindView(R.id.button81)
    Button button81;
    @BindView(R.id.imageView8)
    ImageView imageView8;
    @BindView(R.id.textView3)
    TextView textView3;
    @BindView(R.id.textView4)
    TextView textView4;
    @BindView(R.id.buttondet8)
    Button buttondet8;
    @BindView(R.id.button8)
    Button button8;
    @BindView(R.id.buttondetailsenayan)
    Button buttondetailsenayan;
    @BindView(R.id.buttondetailpalmhill)
    Button buttondetailpalmhill;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_course);
        ButterKnife.bind(this);

        Button booksenayan = (Button) findViewById(R.id.buttonbooksenayan);

        //final Intent pindah5 = new Intent(this,CoFormActivity.class);
        booksenayan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //pindah activity dan mengangkut data
                Intent intent = new Intent(CourseActivity.this, LoginActivity.class);
                //untuk mengangkut data dari class ke class lain
                intent.putExtra("course", "Senayan National Golf Club");
                intent.putExtra("price", "300000");
                //eksekusi intent
                startActivity(intent);
                //startActivity(pindah5);
            }
        });

        Button bookpalm = (Button) findViewById(R.id.buttonbookpalmhill);
        //final Intent pindah5 = new Intent(this,CoFormActivity.class);
        bookpalm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //pindah activity dan mengangkut data
                Intent intent = new Intent(CourseActivity.this, LoginActivity.class);
                //untuk mengangkut data dari class ke class lain
                intent.putExtra("course", "Palm Hill Golf");
                intent.putExtra("price", "585000");
                //eksekusi intent
                startActivity(intent);
                //startActivity(pindah5);
                //startActivity(pindah5);
            }
        });
    }


    @OnClick({R.id.buttondetailsenayan, R.id.buttondetailpalmhill})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttondetailsenayan:
                Intent intent = new Intent(CourseActivity.this,DetailCourseActivity.class);
                intent.putExtra("course","Senayan National Golf Club");
                intent.putExtra("harga", R.drawable.price_senayan);
                intent.putExtra("price", "300000");
                startActivity(intent);
                break;
            case R.id.buttondetailpalmhill:
                Intent intent2 = new Intent(CourseActivity.this,DetailCourseActivity.class);
                intent2.putExtra("course","Palm Hill Golf");
                intent2.putExtra("harga", R.drawable.price);
                intent2.putExtra("price", "585000");
                startActivity(intent2);
                break;
        }
    }
}