package app.ayo.golf;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import app.ayo.golf.adapter.MarketItemAdapter;
import app.ayo.golf.helper.GridSpacingItemDecoration;
import app.ayo.golf.helper.Util;
import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MarketActivity extends AppCompatActivity {
    @BindView(R.id.rv_market_item) RecyclerView mRvMarketItem;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_marketplace);
        ButterKnife.bind(this);

        // init actionbar
        Util.setCustomActionBar(this)
                .setTitle("Marketplace")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                })
                .setRightButton(R.drawable.button_cart, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(MarketActivity.this, "Under construction", Toast.LENGTH_SHORT).show();
                    }
                })
                .setMenus(R.menu.menu_market, new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.menu_search:
                                Toast.makeText(MarketActivity.this, "Under construction", Toast.LENGTH_SHORT).show();
                                break;
                        }
                        return true;
                    }
                });

        initRecyclerView();
    }

    void initRecyclerView(){
        final int column = Util.getScreenSize(this)[0] > 1080 ? 3 : 2;
        mRvMarketItem.setLayoutManager(new GridLayoutManager(this, column, GridLayoutManager.VERTICAL, false));
        int spacingInPixels = (int) Util.convertDpToPixel(10f, this);
        mRvMarketItem.addItemDecoration(new GridSpacingItemDecoration(GridSpacingItemDecoration.GRID_SPACING_FULL, column, spacingInPixels, true, 0));
        mRvMarketItem.setAdapter(new MarketItemAdapter(null));
    }
}
