package app.ayo.golf;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import app.ayo.golf.abstract_class.BaseFragment;
import app.ayo.golf.adapter.CCAdapter;
import app.ayo.golf.retrofit.pojo.APIResponse;
import app.ayo.golf.retrofit.pojo.CoachingClinic;
import app.ayo.golf.retrofit.pojo.Tutorial;
import app.ayo.golf.retrofit.service.ApiServices;
import app.ayo.golf.retrofit.service.InitRetrofit;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class CoachTipsFragment extends BaseFragment {


    @BindView(R.id.rvccvideo)
    RecyclerView rvccvideo;

    CoachingClinic item;

    Unbinder unbinder;

    public CoachTipsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_coach_tips, container, false);

        ApiServices api = InitRetrofit.getInstanceRetrofit();

        Call<APIResponse<CoachingClinic>> call = api.get_cctutorial();

        call.enqueue(new Callback<APIResponse<CoachingClinic>>() {
            @Override
            public void onResponse(Call<APIResponse<CoachingClinic>> call, Response<APIResponse<CoachingClinic>> response) {
                if (isViewActive && response.isSuccessful()) {
                    ArrayList<Tutorial> data = new ArrayList<>();
                    data.addAll(response.body().data.tutorial);
                    CCAdapter ar = new CCAdapter(getActivity(), rvccvideo, data);
                    rvccvideo.setAdapter(ar);
                    rvccvideo.setLayoutManager(new LinearLayoutManager(getContext()));
                }
            }

            @Override
            public void onFailure(Call<APIResponse<CoachingClinic>> call, Throwable t) {
                Log.e("Error coach book", t.getLocalizedMessage());
            }
        });

        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
