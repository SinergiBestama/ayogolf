package app.ayo.golf.entitytiket.searchflight;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 12/03/2018.
 */

public class Returns {

    @SerializedName("resultFlight")
    @Expose
    private List<ResultFlight> resultFlight = null;

    public List<ResultFlight> getResultFlight() {
        return resultFlight;
    }

    public void setResultFlight(List<ResultFlight> resultFlight) {
        this.resultFlight = resultFlight;
    }
}
