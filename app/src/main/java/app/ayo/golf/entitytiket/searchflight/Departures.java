package app.ayo.golf.entitytiket.searchflight;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 12/03/2018.
 */

public class Departures {

    @SerializedName("result")
    @Expose
    private List<Object> result = null;

    public List<Object> getResult() {
        return result;
    }

    public void setResult(List<Object> result) {
        this.result = result;
    }
}
