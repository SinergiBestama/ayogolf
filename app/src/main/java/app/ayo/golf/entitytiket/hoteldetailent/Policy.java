package app.ayo.golf.entitytiket.hoteldetailent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 06/03/2018.
 */

public class Policy {

    @Override
    public String toString() {
        return "Policy{" +
                "name='" + name + '\'' +
                ", tierOne='" + tierOne + '\'' +
                ", tierTwo='" + tierTwo + '\'' +
                '}';
    }

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("tier_one")
    @Expose
    private String tierOne;
    @SerializedName("tier_two")
    @Expose
    private String tierTwo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTierOne() {
        return tierOne;
    }

    public void setTierOne(String tierOne) {
        this.tierOne = tierOne;
    }

    public String getTierTwo() {
        return tierTwo;
    }

    public void setTierTwo(String tierTwo) {
        this.tierTwo = tierTwo;
    }
}
