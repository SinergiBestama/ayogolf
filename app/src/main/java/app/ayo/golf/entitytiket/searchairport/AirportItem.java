package app.ayo.golf.entitytiket.searchairport;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import app.ayo.golf.entitytiket.findhotel.Diagnostic;

/**
 * Created by admin on 07/03/2018.
 */

public class AirportItem {

    @Override
    public String toString() {
        return "AirportItem{" +
                "diagnostic=" + diagnostic +
                ", outputType='" + outputType + '\'' +
                ", allAirport=" + allAirport +
                ", loginStatus='" + loginStatus + '\'' +
                ", token='" + token + '\'' +
                '}';
    }

    @SerializedName("diagnostic")
    @Expose
    private Diagnostic diagnostic;
    @SerializedName("output_type")
    @Expose
    private String outputType;
    @SerializedName("all_airport")
    @Expose
    private AllAirport allAirport;
    @SerializedName("login_status")
    @Expose
    private String loginStatus;
    @SerializedName("token")
    @Expose
    private String token;

    public Diagnostic getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(Diagnostic diagnostic) {
        this.diagnostic = diagnostic;
    }

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public AllAirport getAllAirport() {
        return allAirport;
    }

    public void setAllAirport(AllAirport allAirport) {
        this.allAirport = allAirport;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
