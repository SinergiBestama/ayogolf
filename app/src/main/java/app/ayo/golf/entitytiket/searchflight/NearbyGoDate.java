package app.ayo.golf.entitytiket.searchflight;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 12/03/2018.
 */

public class NearbyGoDate {

    @SerializedName("nearby")
    @Expose
    private List<Nearby> nearby = null;

    public List<Nearby> getNearby() {
        return nearby;
    }

    public void setNearby(List<Nearby> nearby) {
        this.nearby = nearby;
    }
}
