package app.ayo.golf.entitytiket.hoteldetailent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import app.ayo.golf.entitytiket.findhotel.Diagnostic;

/**
 * Created by admin on 06/03/2018.
 */

public class DetailHotel {

    @Override
    public String toString() {
        return "DetailHotel{" +
                "diagnostic=" + diagnostic +
                ", outputType='" + outputType + '\'' +
                ", primaryPhotos='" + primaryPhotos + '\'' +
                ", breadcrumb=" + breadcrumb +
                ", results=" + results +
                ", addinfos=" + addinfos +
                ", allPhoto=" + allPhoto +
                ", primaryPhotosLarge='" + primaryPhotosLarge + '\'' +
                ", availFacilities=" + availFacilities +
                ", nearbyAttractions=" + nearbyAttractions +
                ", policy=" + policy +
                ", listInternalReview=" + listInternalReview +
                ", summaryInternalReview=" + summaryInternalReview +
                ", general=" + general +
                ", loginStatus='" + loginStatus + '\'' +
                ", token='" + token + '\'' +
                '}';
    }

    @SerializedName("diagnostic")
    @Expose
    private Diagnostic diagnostic;
    @SerializedName("output_type")
    @Expose
    private String outputType;
    @SerializedName("primaryPhotos")
    @Expose
    private String primaryPhotos;
    @SerializedName("breadcrumb")
    @Expose
    private Breadcrumb breadcrumb;
    @SerializedName("results")
    @Expose
    private ResultsDetail results;
    @SerializedName("addinfos")
    @Expose
    private Addinfos addinfos;
    @SerializedName("all_photo")
    @Expose
    private AllPhoto allPhoto;
    @SerializedName("primaryPhotos_large")
    @Expose
    private String primaryPhotosLarge;
    @SerializedName("avail_facilities")
    @Expose
    private AvailFacilities availFacilities;
    @SerializedName("nearby_attractions")
    @Expose
    private NearbyAttractions nearbyAttractions;
    @SerializedName("policy")
    @Expose
    private List<Policy> policy = null;
    @SerializedName("list_internal_review")
    @Expose
    private List<Object> listInternalReview = null;
    @SerializedName("summary_internal_review")
    @Expose
    private SummaryInternalReview summaryInternalReview;
    @SerializedName("general")
    @Expose
    private General general;
    @SerializedName("login_status")
    @Expose
    private String loginStatus;
    @SerializedName("token")
    @Expose
    private String token;

    public Diagnostic getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(Diagnostic diagnostic) {
        this.diagnostic = diagnostic;
    }

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public String getPrimaryPhotos() {
        return primaryPhotos;
    }

    public void setPrimaryPhotos(String primaryPhotos) {
        this.primaryPhotos = primaryPhotos;
    }

    public Breadcrumb getBreadcrumb() {
        return breadcrumb;
    }

    public void setBreadcrumb(Breadcrumb breadcrumb) {
        this.breadcrumb = breadcrumb;
    }

    public ResultsDetail getResults() {
        return results;
    }

    public void setResults(ResultsDetail results) {
        this.results = results;
    }

    public Addinfos getAddinfos() {
        return addinfos;
    }

    public void setAddinfos(Addinfos addinfos) {
        this.addinfos = addinfos;
    }

    public AllPhoto getAllPhoto() {
        return allPhoto;
    }

    public void setAllPhoto(AllPhoto allPhoto) {
        this.allPhoto = allPhoto;
    }

    public String getPrimaryPhotosLarge() {
        return primaryPhotosLarge;
    }

    public void setPrimaryPhotosLarge(String primaryPhotosLarge) {
        this.primaryPhotosLarge = primaryPhotosLarge;
    }

    public AvailFacilities getAvailFacilities() {
        return availFacilities;
    }

    public void setAvailFacilities(AvailFacilities availFacilities) {
        this.availFacilities = availFacilities;
    }

    public NearbyAttractions getNearbyAttractions() {
        return nearbyAttractions;
    }

    public void setNearbyAttractions(NearbyAttractions nearbyAttractions) {
        this.nearbyAttractions = nearbyAttractions;
    }

    public List<Policy> getPolicy() {
        return policy;
    }

    public void setPolicy(List<Policy> policy) {
        this.policy = policy;
    }

    public List<Object> getListInternalReview() {
        return listInternalReview;
    }

    public void setListInternalReview(List<Object> listInternalReview) {
        this.listInternalReview = listInternalReview;
    }

    public SummaryInternalReview getSummaryInternalReview() {
        return summaryInternalReview;
    }

    public void setSummaryInternalReview(SummaryInternalReview summaryInternalReview) {
        this.summaryInternalReview = summaryInternalReview;
    }

    public General getGeneral() {
        return general;
    }

    public void setGeneral(General general) {
        this.general = general;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
