package app.ayo.golf.entitytiket.findhotel;

import java.io.Serializable;

/**
 * Created by root on 2/19/18.
 */

public class TicketCurrency implements Serializable {
    private String id;
    private String lang;
    private String code;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "TicketCurrency{" +
                "id='" + id + '\'' +
                ", lang='" + lang + '\'' +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
