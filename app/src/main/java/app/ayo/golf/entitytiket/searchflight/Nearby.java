package app.ayo.golf.entitytiket.searchflight;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 12/03/2018.
 */

public class Nearby {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("price")
    @Expose
    private Integer price;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
