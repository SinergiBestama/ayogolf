package app.ayo.golf.entitytiket.hoteldetailent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 06/03/2018.
 */

public class AvailFacilities {

    @Override
    public String toString() {
        return "AvailFacilities{" +
                "availFacilitiy=" + availFacilitiy +
                '}';
    }

    @SerializedName("avail_facilitiy")
    @Expose
    private List<AvailFacilitiy> availFacilitiy = null;

    public List<AvailFacilitiy> getAvailFacilitiy() {
        return availFacilitiy;
    }

    public void setAvailFacilitiy(List<AvailFacilitiy> availFacilitiy) {
        this.availFacilitiy = availFacilitiy;
    }
}
