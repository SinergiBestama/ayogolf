package app.ayo.golf.entitytiket.hoteldetailent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 06/03/2018.
 */

public class ResultsDetail {

    @Override
    public String toString() {
        return "ResultsDetail{" +
                "result=" + result +
                '}';
    }

    @SerializedName("result")
    @Expose
    private List<ResultDetail> result = null;

    public List<ResultDetail> getResult() {
        return result;
    }

    public void setResult(List<ResultDetail> result) {
        this.result = result;
    }
}
