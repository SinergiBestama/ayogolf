package app.ayo.golf.entitytiket.hoteldetailent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 06/03/2018.
 */

public class Addinfos {
    @Override
    public String toString() {
        return "Addinfos{" +
                "addinfo=" + addinfo +
                '}';
    }

    @SerializedName("addinfo")
    @Expose
    private List<String> addinfo = null;

    public List<String> getAddinfo() {
        return addinfo;
    }

    public void setAddinfo(List<String> addinfo) {
        this.addinfo = addinfo;
    }
}
