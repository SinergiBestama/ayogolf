package app.ayo.golf.entitytiket.hoteldetailent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 06/03/2018.
 */

public class NearbyAttractions {

    @Override
    public String toString() {
        return "NearbyAttractions{" +
                "nearbyAttraction=" + nearbyAttraction +
                '}';
    }

    @SerializedName("nearby_attraction")
    @Expose
    private List<NearbyAttraction> nearbyAttraction = null;

    public List<NearbyAttraction> getNearbyAttraction() {
        return nearbyAttraction;
    }

    public void setNearbyAttraction(List<NearbyAttraction> nearbyAttraction) {
        this.nearbyAttraction = nearbyAttraction;
    }
}
