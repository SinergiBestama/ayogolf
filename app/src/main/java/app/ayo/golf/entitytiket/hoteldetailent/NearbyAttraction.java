package app.ayo.golf.entitytiket.hoteldetailent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 06/03/2018.
 */

public class NearbyAttraction {

    @Override
    public String toString() {
        return "NearbyAttraction{" +
                "distance=" + distance +
                ", businessPrimaryPhoto='" + businessPrimaryPhoto + '\'' +
                ", businessName='" + businessName + '\'' +
                ", businessUri='" + businessUri + '\'' +
                ", businessLat='" + businessLat + '\'' +
                ", businessLong='" + businessLong + '\'' +
                '}';
    }

    @SerializedName("distance")
    @Expose
    private Integer distance;
    @SerializedName("business_primary_photo")
    @Expose
    private String businessPrimaryPhoto;
    @SerializedName("business_name")
    @Expose
    private String businessName;
    @SerializedName("business_uri")
    @Expose
    private String businessUri;
    @SerializedName("business_lat")
    @Expose
    private String businessLat;
    @SerializedName("business_long")
    @Expose
    private String businessLong;

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public String getBusinessPrimaryPhoto() {
        return businessPrimaryPhoto;
    }

    public void setBusinessPrimaryPhoto(String businessPrimaryPhoto) {
        this.businessPrimaryPhoto = businessPrimaryPhoto;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessUri() {
        return businessUri;
    }

    public void setBusinessUri(String businessUri) {
        this.businessUri = businessUri;
    }

    public String getBusinessLat() {
        return businessLat;
    }

    public void setBusinessLat(String businessLat) {
        this.businessLat = businessLat;
    }

    public String getBusinessLong() {
        return businessLong;
    }

    public void setBusinessLong(String businessLong) {
        this.businessLong = businessLong;
    }
}
