package app.ayo.golf.entitytiket.searchflight;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 12/03/2018.
 */

public class ArrAirport_ {

    @SerializedName("airport_code")
    @Expose
    private String airportCode;
    @SerializedName("international")
    @Expose
    private String international;
    @SerializedName("trans_name_id")
    @Expose
    private String transNameId;
    @SerializedName("banner_image")
    @Expose
    private String bannerImage;
    @SerializedName("short_name_trans_id")
    @Expose
    private String shortNameTransId;
    @SerializedName("business_name")
    @Expose
    private String businessName;
    @SerializedName("business_name_trans_id")
    @Expose
    private String businessNameTransId;
    @SerializedName("business_country")
    @Expose
    private String businessCountry;
    @SerializedName("business_id")
    @Expose
    private String businessId;
    @SerializedName("country_name")
    @Expose
    private String countryName;
    @SerializedName("city_name")
    @Expose
    private String cityName;
    @SerializedName("province_name")
    @Expose
    private String provinceName;
    @SerializedName("short_name")
    @Expose
    private String shortName;
    @SerializedName("location_name")
    @Expose
    private String locationName;

    public String getAirportCode() {
        return airportCode;
    }

    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public String getInternational() {
        return international;
    }

    public void setInternational(String international) {
        this.international = international;
    }

    public String getTransNameId() {
        return transNameId;
    }

    public void setTransNameId(String transNameId) {
        this.transNameId = transNameId;
    }

    public String getBannerImage() {
        return bannerImage;
    }

    public void setBannerImage(String bannerImage) {
        this.bannerImage = bannerImage;
    }

    public String getShortNameTransId() {
        return shortNameTransId;
    }

    public void setShortNameTransId(String shortNameTransId) {
        this.shortNameTransId = shortNameTransId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessNameTransId() {
        return businessNameTransId;
    }

    public void setBusinessNameTransId(String businessNameTransId) {
        this.businessNameTransId = businessNameTransId;
    }

    public String getBusinessCountry() {
        return businessCountry;
    }

    public void setBusinessCountry(String businessCountry) {
        this.businessCountry = businessCountry;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
}
