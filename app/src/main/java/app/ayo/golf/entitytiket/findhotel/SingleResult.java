package app.ayo.golf.entitytiket.findhotel;

import java.util.List;

import app.ayo.golf.entitytiket.findhotel.Diagnostic;

/**
 * Created by root on 2/19/18.
 */

public class SingleResult<T> {
    private Diagnostic diagnostic;
    private String output_type;
    private String login_status;
    private String token;
    private ResultList<T> results;

    @Override
    public String toString() {
        return "SingleResult{" +
                "diagnostic=" + diagnostic +
                ", output_type='" + output_type + '\'' +
                ", login_status='" + login_status + '\'' +
                ", token='" + token + '\'' +
                ", results=" + results +
                '}';
    }

    public ResultList<T> getResults() {
        return results;
    }

    public void setResults(ResultList<T> results) {
        this.results = results;
    }

    public Diagnostic getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(Diagnostic diagnostic) {
        this.diagnostic = diagnostic;
    }

    public String getOutput_type() {
        return output_type;
    }

    public void setOutput_type(String output_type) {
        this.output_type = output_type;
    }

    public String getLogin_status() {
        return login_status;
    }

    public void setLogin_status(String login_status) {
        this.login_status = login_status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public class ResultList<T> {
        private List<T> result;

        public List<T> getResult() {
            return result;
        }

        public void setResult(List<T> result) {
            this.result = result;
        }

        @Override
        public String toString() {
            return "ResultList{" +
                    "result=" + result +
                    '}';
        }
    }
}
