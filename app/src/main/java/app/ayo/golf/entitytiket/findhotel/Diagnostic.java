package app.ayo.golf.entitytiket.findhotel;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * Created by root on 2/19/18.
 */

public class Diagnostic implements Serializable {

    private int status;
    private String elapsetime;
    private String memoryusage;
    private BigInteger unix_timestamp;
    private String confirm;
    private String lang;
    private String currency;

    public boolean isSuccess() {
        return status == 200;
    }

    @Override
    public String toString() {
        return "Diagnostic{" +
                "status=" + status +
                ", elapsetime='" + elapsetime + '\'' +
                ", memoryusage='" + memoryusage + '\'' +
                ", unix_timestamp=" + unix_timestamp +
                ", confirm='" + confirm + '\'' +
                ", lang='" + lang + '\'' +
                ", currency='" + currency + '\'' +
                '}';
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getElapsetime() {
        return elapsetime;
    }

    public void setElapsetime(String elapsetime) {
        this.elapsetime = elapsetime;
    }

    public String getMemoryusage() {
        return memoryusage;
    }

    public void setMemoryusage(String memoryusage) {
        this.memoryusage = memoryusage;
    }

    public BigInteger getUnix_timestamp() {
        return unix_timestamp;
    }

    public void setUnix_timestamp(BigInteger unix_timestamp) {
        this.unix_timestamp = unix_timestamp;
    }

    public String getConfirm() {
        return confirm;
    }

    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
