package app.ayo.golf.entitytiket.hoteldetailent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 06/03/2018.
 */

public class SummaryInternalReview {

    @Override
    public String toString() {
        return "SummaryInternalReview{" +
                "average='" + average + '\'' +
                ", maxRating=" + maxRating +
                ", detail=" + detail +
                '}';
    }

    @SerializedName("average")
    @Expose
    private String average;
    @SerializedName("max_rating")
    @Expose
    private Integer maxRating;
    @SerializedName("detail")
    @Expose
    private List<Detail> detail = null;

    public String getAverage() {
        return average;
    }

    public void setAverage(String average) {
        this.average = average;
    }

    public Integer getMaxRating() {
        return maxRating;
    }

    public void setMaxRating(Integer maxRating) {
        this.maxRating = maxRating;
    }

    public List<Detail> getDetail() {
        return detail;
    }

    public void setDetail(List<Detail> detail) {
        this.detail = detail;
    }
}
