package app.ayo.golf.entitytiket.hoteldetailent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 06/03/2018.
 */

public class ResultDetail {

    @Override
    public String toString() {
        return "ResultDetail{" +
                "id='" + id + '\'' +
                ", roomAvailable='" + roomAvailable + '\'' +
                ", extSource='" + extSource + '\'' +
                ", roomId='" + roomId + '\'' +
                ", currency='" + currency + '\'' +
                ", minimumStays='" + minimumStays + '\'' +
                ", withBreakfasts='" + withBreakfasts + '\'' +
                ", roomDescription='" + roomDescription + '\'' +
                ", allPhotoRoom=" + allPhotoRoom +
                ", photoUrl='" + photoUrl + '\'' +
                ", roomName='" + roomName + '\'' +
                ", oldprice='" + oldprice + '\'' +
                ", price='" + price + '\'' +
                ", bookUri='" + bookUri + '\'' +
                ", roomFacility=" + roomFacility +
                ", additionalSurchargeCurrency='" + additionalSurchargeCurrency + '\'' +
                '}';
    }

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("room_available")
    @Expose
    private String roomAvailable;
    @SerializedName("ext_source")
    @Expose
    private String extSource;
    @SerializedName("room_id")
    @Expose
    private String roomId;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("minimum_stays")
    @Expose
    private String minimumStays;
    @SerializedName("with_breakfasts")
    @Expose
    private String withBreakfasts;
    @SerializedName("room_description")
    @Expose
    private String roomDescription;
    @SerializedName("all_photo_room")
    @Expose
    private List<String> allPhotoRoom = null;
    @SerializedName("photo_url")
    @Expose
    private String photoUrl;
    @SerializedName("room_name")
    @Expose
    private String roomName;
    @SerializedName("oldprice")
    @Expose
    private String oldprice;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("bookUri")
    @Expose
    private String bookUri;
    @SerializedName("room_facility")
    @Expose
    private List<String> roomFacility = null;
    @SerializedName("additional_surcharge_currency")
    @Expose
    private String additionalSurchargeCurrency;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoomAvailable() {
        return roomAvailable;
    }

    public void setRoomAvailable(String roomAvailable) {
        this.roomAvailable = roomAvailable;
    }

    public String getExtSource() {
        return extSource;
    }

    public void setExtSource(String extSource) {
        this.extSource = extSource;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMinimumStays() {
        return minimumStays;
    }

    public void setMinimumStays(String minimumStays) {
        this.minimumStays = minimumStays;
    }

    public String getWithBreakfasts() {
        return withBreakfasts;
    }

    public void setWithBreakfasts(String withBreakfasts) {
        this.withBreakfasts = withBreakfasts;
    }

    public String getRoomDescription() {
        return roomDescription;
    }

    public void setRoomDescription(String roomDescription) {
        this.roomDescription = roomDescription;
    }

    public List<String> getAllPhotoRoom() {
        return allPhotoRoom;
    }

    public void setAllPhotoRoom(List<String> allPhotoRoom) {
        this.allPhotoRoom = allPhotoRoom;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getOldprice() {
        return oldprice;
    }

    public void setOldprice(String oldprice) {
        this.oldprice = oldprice;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getBookUri() {
        return bookUri;
    }

    public void setBookUri(String bookUri) {
        this.bookUri = bookUri;
    }

    public List<String> getRoomFacility() {
        return roomFacility;
    }

    public void setRoomFacility(List<String> roomFacility) {
        this.roomFacility = roomFacility;
    }

    public String getAdditionalSurchargeCurrency() {
        return additionalSurchargeCurrency;
    }

    public void setAdditionalSurchargeCurrency(String additionalSurchargeCurrency) {
        this.additionalSurchargeCurrency = additionalSurchargeCurrency;
    }
}
