package app.ayo.golf.entitytiket.searchflight;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 12/03/2018.
 */

public class FlightInfos {

    @SerializedName("flight_info")
    @Expose
    private List<FlightInfo> flightInfo = null;

    public List<FlightInfo> getFlightInfo() {
        return flightInfo;
    }

    public void setFlightInfo(List<FlightInfo> flightInfo) {
        this.flightInfo = flightInfo;
    }
}
