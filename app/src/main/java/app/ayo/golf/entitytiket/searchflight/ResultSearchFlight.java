package app.ayo.golf.entitytiket.searchflight;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import app.ayo.golf.entitytiket.findhotel.Diagnostic;

/**
 * Created by admin on 12/03/2018.
 */

public class ResultSearchFlight implements Serializable {

    private Diagnostic diagnostic;
    private String outputType;
    private Boolean roundTrip;
    private SearchQueries searchQueries;
    private GoDet goDet;
    private RetDet retDet;
    private Departures departures;
    private Returns returns;
    private NearbyGoDate nearbyGoDate;
    private NearbyRetDate nearbyRetDate;
    private String loginStatus;
    private String token;

    public Diagnostic getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(Diagnostic diagnostic) {
        this.diagnostic = diagnostic;
    }

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public Boolean getRoundTrip() {
        return roundTrip;
    }

    public void setRoundTrip(Boolean roundTrip) {
        this.roundTrip = roundTrip;
    }

    public SearchQueries getSearchQueries() {
        return searchQueries;
    }

    public void setSearchQueries(SearchQueries searchQueries) {
        this.searchQueries = searchQueries;
    }

    public GoDet getGoDet() {
        return goDet;
    }

    public void setGoDet(GoDet goDet) {
        this.goDet = goDet;
    }

    public RetDet getRetDet() {
        return retDet;
    }

    public void setRetDet(RetDet retDet) {
        this.retDet = retDet;
    }

    public Departures getDepartures() {
        return departures;
    }

    public void setDepartures(Departures departures) {
        this.departures = departures;
    }

    public Returns getReturns() {
        return returns;
    }

    public void setReturns(Returns returns) {
        this.returns = returns;
    }

    public NearbyGoDate getNearbyGoDate() {
        return nearbyGoDate;
    }

    public void setNearbyGoDate(NearbyGoDate nearbyGoDate) {
        this.nearbyGoDate = nearbyGoDate;
    }

    public NearbyRetDate getNearbyRetDate() {
        return nearbyRetDate;
    }

    public void setNearbyRetDate(NearbyRetDate nearbyRetDate) {
        this.nearbyRetDate = nearbyRetDate;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
