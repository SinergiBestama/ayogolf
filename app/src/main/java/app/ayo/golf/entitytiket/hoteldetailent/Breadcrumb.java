package app.ayo.golf.entitytiket.hoteldetailent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 06/03/2018.
 */

public class Breadcrumb {

    @Override
    public String toString() {
        return "Breadcrumb{" +
                "businessId='" + businessId + '\'' +
                ", businessUri='" + businessUri + '\'' +
                ", businessName='" + businessName + '\'' +
                ", areaId='" + areaId + '\'' +
                ", areaName='" + areaName + '\'' +
                ", kelurahanId='" + kelurahanId + '\'' +
                ", kelurahanName='" + kelurahanName + '\'' +
                ", kecamatanId='" + kecamatanId + '\'' +
                ", kecamatanName='" + kecamatanName + '\'' +
                ", cityId='" + cityId + '\'' +
                ", cityName='" + cityName + '\'' +
                ", provinceId='" + provinceId + '\'' +
                ", provinceName='" + provinceName + '\'' +
                ", countryId='" + countryId + '\'' +
                ", countryName='" + countryName + '\'' +
                ", continentId='" + continentId + '\'' +
                ", continentName='" + continentName + '\'' +
                ", kelurahanUri='" + kelurahanUri + '\'' +
                ", kecamatanUri='" + kecamatanUri + '\'' +
                ", provinceUri='" + provinceUri + '\'' +
                ", countryUri='" + countryUri + '\'' +
                ", continentUri='" + continentUri + '\'' +
                ", starRating='" + starRating + '\'' +
                '}';
    }

    @SerializedName("business_id")
    @Expose
    private String businessId;
    @SerializedName("business_uri")
    @Expose
    private String businessUri;
    @SerializedName("business_name")
    @Expose
    private String businessName;
    @SerializedName("area_id")
    @Expose
    private String areaId;
    @SerializedName("area_name")
    @Expose
    private String areaName;
    @SerializedName("kelurahan_id")
    @Expose
    private String kelurahanId;
    @SerializedName("kelurahan_name")
    @Expose
    private String kelurahanName;
    @SerializedName("kecamatan_id")
    @Expose
    private String kecamatanId;
    @SerializedName("kecamatan_name")
    @Expose
    private String kecamatanName;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("city_name")
    @Expose
    private String cityName;
    @SerializedName("province_id")
    @Expose
    private String provinceId;
    @SerializedName("province_name")
    @Expose
    private String provinceName;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("country_name")
    @Expose
    private String countryName;
    @SerializedName("continent_id")
    @Expose
    private String continentId;
    @SerializedName("continent_name")
    @Expose
    private String continentName;
    @SerializedName("kelurahan_uri")
    @Expose
    private String kelurahanUri;
    @SerializedName("kecamatan_uri")
    @Expose
    private String kecamatanUri;
    @SerializedName("province_uri")
    @Expose
    private String provinceUri;
    @SerializedName("country_uri")
    @Expose
    private String countryUri;
    @SerializedName("continent_uri")
    @Expose
    private String continentUri;
    @SerializedName("star_rating")
    @Expose
    private String starRating;

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getBusinessUri() {
        return businessUri;
    }

    public void setBusinessUri(String businessUri) {
        this.businessUri = businessUri;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getKelurahanId() {
        return kelurahanId;
    }

    public void setKelurahanId(String kelurahanId) {
        this.kelurahanId = kelurahanId;
    }

    public String getKelurahanName() {
        return kelurahanName;
    }

    public void setKelurahanName(String kelurahanName) {
        this.kelurahanName = kelurahanName;
    }

    public String getKecamatanId() {
        return kecamatanId;
    }

    public void setKecamatanId(String kecamatanId) {
        this.kecamatanId = kecamatanId;
    }

    public String getKecamatanName() {
        return kecamatanName;
    }

    public void setKecamatanName(String kecamatanName) {
        this.kecamatanName = kecamatanName;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getContinentId() {
        return continentId;
    }

    public void setContinentId(String continentId) {
        this.continentId = continentId;
    }

    public String getContinentName() {
        return continentName;
    }

    public void setContinentName(String continentName) {
        this.continentName = continentName;
    }

    public String getKelurahanUri() {
        return kelurahanUri;
    }

    public void setKelurahanUri(String kelurahanUri) {
        this.kelurahanUri = kelurahanUri;
    }

    public String getKecamatanUri() {
        return kecamatanUri;
    }

    public void setKecamatanUri(String kecamatanUri) {
        this.kecamatanUri = kecamatanUri;
    }

    public String getProvinceUri() {
        return provinceUri;
    }

    public void setProvinceUri(String provinceUri) {
        this.provinceUri = provinceUri;
    }

    public String getCountryUri() {
        return countryUri;
    }

    public void setCountryUri(String countryUri) {
        this.countryUri = countryUri;
    }

    public String getContinentUri() {
        return continentUri;
    }

    public void setContinentUri(String continentUri) {
        this.continentUri = continentUri;
    }

    public String getStarRating() {
        return starRating;
    }

    public void setStarRating(String starRating) {
        this.starRating = starRating;
    }
}
