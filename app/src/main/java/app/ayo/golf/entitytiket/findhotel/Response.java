package app.ayo.golf.entitytiket.findhotel;

import java.util.List;

import app.ayo.golf.entitytiket.findhotel.Diagnostic;

/**
 * Created by root on 2/19/18.
 */

public class Response<T> {

    private Diagnostic diagnostic;
    private String output_type;
    private String login_status;
    private String token;
    private List<T> result;

    @Override
    public String toString() {
        return "Response{" +
                "diagnostic=" + diagnostic +
                ", output_type='" + output_type + '\'' +
                ", login_status='" + login_status + '\'' +
                ", token='" + token + '\'' +
                ", result=" + result +
                '}';
    }

    public List<T> getResult() {
        return result;
    }

    public void setResult(List<T> result) {
        this.result = result;
    }

    public Diagnostic getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(Diagnostic diagnostic) {
        this.diagnostic = diagnostic;
    }

    public String getOutput_type() {
        return output_type;
    }

    public void setOutput_type(String output_type) {
        this.output_type = output_type;
    }

    public String getLogin_status() {
        return login_status;
    }

    public void setLogin_status(String login_status) {
        this.login_status = login_status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
