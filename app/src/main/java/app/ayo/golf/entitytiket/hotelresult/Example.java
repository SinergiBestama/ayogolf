package app.ayo.golf.entitytiket.hotelresult;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import app.ayo.golf.entitytiket.findhotel.Diagnostic;

/**
 * Created by admin on 28/02/2018.
 */

public class Example implements Serializable {
    @Override
    public String toString() {
        return "Example{" +
                "diagnostic=" + diagnostic +
                ", outputType='" + outputType + '\'' +
                ", searchQueries=" + searchQueries +
                ", results=" + results +
                ", pagination=" + pagination +
                ", loginStatus='" + loginStatus + '\'' +
                ", token='" + token + '\'' +
                '}';
    }

    @SerializedName("diagnostic")
    @Expose
    private Diagnostic diagnostic;
    @SerializedName("output_type")
    @Expose
    private String outputType;
    @SerializedName("search_queries")
    @Expose
    private SearchQueries searchQueries;
    @SerializedName("results")
    @Expose
    private Results results;
    @SerializedName("pagination")
    @Expose
    private Pagination pagination;
    @SerializedName("login_status")
    @Expose
    private String loginStatus;
    @SerializedName("token")
    @Expose
    private String token;

    public Diagnostic getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(Diagnostic diagnostic) {
        this.diagnostic = diagnostic;
    }

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public SearchQueries getSearchQueries() {
        return searchQueries;
    }

    public void setSearchQueries(SearchQueries searchQueries) {
        this.searchQueries = searchQueries;
    }

    public Results getResults() {
        return results;
    }

    public void setResults(Results results) {
        this.results = results;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
