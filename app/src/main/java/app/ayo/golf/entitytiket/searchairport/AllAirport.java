package app.ayo.golf.entitytiket.searchairport;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 07/03/2018.
 */

public class AllAirport {

    @Override
    public String toString() {
        return "AllAirport{" +
                "airport=" + airport +
                '}';
    }

    @SerializedName("airport")
    @Expose
    private List<Airport> airport = null;

    public List<Airport> getAirport() {
        return airport;
    }

    public void setAirport(List<Airport> airport) {
        this.airport = airport;
    }
}
