package app.ayo.golf.entitytiket.searchflight;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 12/03/2018.
 */

public class RetDet {

    @SerializedName("dep_airport")
    @Expose
    private DepAirport_ depAirport;
    @SerializedName("arr_airport")
    @Expose
    private ArrAirport_ arrAirport;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("formatted_date")
    @Expose
    private String formattedDate;

    public DepAirport_ getDepAirport() {
        return depAirport;
    }

    public void setDepAirport(DepAirport_ depAirport) {
        this.depAirport = depAirport;
    }

    public ArrAirport_ getArrAirport() {
        return arrAirport;
    }

    public void setArrAirport(ArrAirport_ arrAirport) {
        this.arrAirport = arrAirport;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFormattedDate() {
        return formattedDate;
    }

    public void setFormattedDate(String formattedDate) {
        this.formattedDate = formattedDate;
    }
}
