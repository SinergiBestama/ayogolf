package app.ayo.golf.entitytiket.hoteldetailent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 06/03/2018.
 */

public class Photo {

    @Override
    public String toString() {
        return "Photo{" +
                "fileName='" + fileName + '\'' +
                ", photoType='" + photoType + '\'' +
                '}';
    }

    @SerializedName("file_name")
    @Expose
    private String fileName;
    @SerializedName("photo_type")
    @Expose
    private String photoType;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getPhotoType() {
        return photoType;
    }

    public void setPhotoType(String photoType) {
        this.photoType = photoType;
    }
}
