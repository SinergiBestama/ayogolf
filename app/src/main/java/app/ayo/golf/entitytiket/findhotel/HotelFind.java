package app.ayo.golf.entitytiket.findhotel;

import java.io.Serializable;

/**
 * Created by root on 2/19/18.
 */

public class HotelFind implements Serializable {
    private String id;
    private String label;
    private String value;
    private String category;

    @Override
    public String toString() {
        return "HotelFind{" +
                "id='" + id + '\'' +
                ", label='" + label + '\'' +
                ", value='" + value + '\'' +
                ", category='" + category + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
