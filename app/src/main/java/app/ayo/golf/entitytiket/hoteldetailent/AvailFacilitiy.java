package app.ayo.golf.entitytiket.hoteldetailent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 06/03/2018.
 */

public class AvailFacilitiy {

    @Override
    public String toString() {
        return "AvailFacilitiy{" +
                "facilityType='" + facilityType + '\'' +
                ", facilityName='" + facilityName + '\'' +
                '}';
    }

    @SerializedName("facility_type")
    @Expose
    private String facilityType;
    @SerializedName("facility_name")
    @Expose
    private String facilityName;

    public String getFacilityType() {
        return facilityType;
    }

    public void setFacilityType(String facilityType) {
        this.facilityType = facilityType;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }
}
