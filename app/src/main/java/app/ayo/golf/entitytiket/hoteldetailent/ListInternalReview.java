package app.ayo.golf.entitytiket.hoteldetailent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 09/03/2018.
 */

public class ListInternalReview {

    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("person")
    @Expose
    private String person;
    @SerializedName("nationality_flag")
    @Expose
    private String nationalityFlag;
    @SerializedName("nationality")
    @Expose
    private String nationality;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("max_rating")
    @Expose
    private Integer maxRating;
    @SerializedName("testimonial_text")
    @Expose
    private String testimonialText;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getNationalityFlag() {
        return nationalityFlag;
    }

    public void setNationalityFlag(String nationalityFlag) {
        this.nationalityFlag = nationalityFlag;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public Integer getMaxRating() {
        return maxRating;
    }

    public void setMaxRating(Integer maxRating) {
        this.maxRating = maxRating;
    }

    public String getTestimonialText() {
        return testimonialText;
    }

    public void setTestimonialText(String testimonialText) {
        this.testimonialText = testimonialText;
    }
}
