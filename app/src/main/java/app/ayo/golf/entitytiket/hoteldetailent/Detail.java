package app.ayo.golf.entitytiket.hoteldetailent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 06/03/2018.
 */

public class Detail {

    @Override
    public String toString() {
        return "Detail{" +
                "category='" + category + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("value")
    @Expose
    private String value;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
