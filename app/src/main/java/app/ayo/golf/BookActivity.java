package app.ayo.golf;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;

import java.util.ArrayList;

import app.ayo.golf.adapter.BookingItemsAdapter;
import app.ayo.golf.helper.SessionManager;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.pojo.APIResponse;
import app.ayo.golf.retrofit.pojo.BookingCourseResult;
import app.ayo.golf.retrofit.pojo.Golfer;
import app.ayo.golf.retrofit.service.ApiServices;
import app.ayo.golf.retrofit.service.InitRetrofit;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BookActivity extends AppCompatActivity {

    @BindView(R.id.tv_title_book_activity)
    TextView mTitleBookActivity;
    @BindView(R.id.tv_bookHistory)
    TextView mBookHistory;
    @BindView(R.id.rv_booking_history)
    RecyclerView mRvHistory;
    @BindView(R.id.ll_empty)
    View mLlEmpty;
    @BindView(R.id.progress_bar)
    View mProgressBar;
    @BindView(R.id.swipeLayout)
    SwipeRefreshLayout swipeLayout;
    @BindView(R.id.tv_golf_course)
    TextView tvGolfCourse;
    @BindView(R.id.tv_hotel_book)
    TextView tvHotelBook;
    @BindView(R.id.tv_flight_book)
    TextView tvFlightBook;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        ButterKnife.bind(this);

        // init action bar
        Util.setCustomActionBar(this)
                .setTitle("Booking")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                })
                .setRightButton(null, null);

        //================= Only For View ====================================//
        mBookHistory.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        mTitleBookActivity.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        tvGolfCourse.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        tvHotelBook.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        tvFlightBook.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        // init tabs
//        mTabs.addTab(mTabs.newTab().setText("In Progress"));
//        mTabs.addTab(mTabs.newTab().setText("History"));
//        mTabs.setSelectedTabIndicatorColor(Color.WHITE);
//        ViewCompat.setElevation(mTabs, 10);
//
//        // init pager
//        PagerAdapter adapter = new BookingTabs(getSupportFragmentManager());
//        mPager.setAdapter(adapter);
//        mPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabs));
//        mTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                mPager.setCurrentItem(tab.getPosition());
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//
//            }
//        });

        swipeLayout.setColorSchemeColors(Color.DKGRAY, Color.YELLOW);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeLayout.setRefreshing(true);
                recreate(mRvHistory);

                if (swipeLayout.isRefreshing()) {
                    swipeLayout.setRefreshing(false);
                    getTransaction();
                }
            }
        });

        initRecycleView();
        getTransaction();
    }

    private RecyclerView recreate(RecyclerView mRvHistory) {
        return mRvHistory;
    }

    @OnClick({R.id.v_booking_course, R.id.v_weather_report, R.id.v_booking_hotel, R.id.v_booking_flight})
    public void click(View v) {
        switch (v.getId()) {
            case R.id.v_booking_course:
                Intent bookCourseIntent = new Intent(this, Course2Activity.class);
                startActivity(bookCourseIntent);
                break;
            case R.id.v_weather_report:
                Toast.makeText(this, "Under Construction", Toast.LENGTH_SHORT).show();
                break;
            case R.id.v_booking_hotel:
                Uri uriHotel = Uri.parse("https://m.tiket.com/hotel/");
                Intent launchBrowserHotel = new Intent(Intent.ACTION_VIEW, uriHotel);
                startActivity(launchBrowserHotel);
//                Intent bookHotelIntent = new Intent(this, HotelActivity.class);
//                startActivity(bookHotelIntent);
                //Toast.makeText(this, "Under Construction", Toast.LENGTH_SHORT).show();
                break;
            case R.id.v_booking_flight:
//                Intent bookFlightIntent = new Intent(this, FlightActivity.class);
//                startActivity(bookFlightIntent);
                Uri uriFlight = Uri.parse("https://m.tiket.com/pesawat/");
                Intent launchBrowserFlight = new Intent(Intent.ACTION_VIEW, uriFlight);
                startActivity(launchBrowserFlight);
                //Toast.makeText(this, "Under Construction", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public class BookingTabs extends FragmentStatePagerAdapter {
        BookingTabs(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return position == 0 ? new InProgressFragment() : new HistoryFragment();
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    private void getTransaction() {
        Golfer golfer = SessionManager.getProfile(this);
        if (golfer == null)
            return;

        JsonArray array = new JsonArray();
        array.add("canceled");
        array.add("completed");
        ApiServices api = InitRetrofit.getInstanceRetrofit();
        Call<APIResponse<ArrayList<BookingCourseResult>>> call = api.transactHistory(golfer.golfer_id, array.toString());
        mProgressBar.setVisibility(View.VISIBLE);
        call.enqueue(new Callback<APIResponse<ArrayList<BookingCourseResult>>>() {
            @Override
            public void onResponse(Call<APIResponse<ArrayList<BookingCourseResult>>> call, Response<APIResponse<ArrayList<BookingCourseResult>>> response) {
                if (response.isSuccessful() && response.body().isSuccessful()) {
                    // update ui
                    ((BookingItemsAdapter) mRvHistory.getAdapter()).setItems(response.body().data);
                }

                showEmptyView();
                mProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<APIResponse<ArrayList<BookingCourseResult>>> call, Throwable t) {
                // TODO error handler
                showEmptyView();
                mProgressBar.setVisibility(View.GONE);
            }
        });
    }

    private void initRecycleView() {
        mRvHistory.setLayoutManager(new LinearLayoutManager(mRvHistory.getContext(), LinearLayoutManager.VERTICAL, false));
        BookingItemsAdapter adapter = new BookingItemsAdapter();
        adapter.disableClick(true);
        mRvHistory.setAdapter(adapter);
        showEmptyView();
    }

    private void showEmptyView() {
        if (((BookingItemsAdapter) mRvHistory.getAdapter()).isEmpty()) {
            mLlEmpty.setVisibility(View.VISIBLE);
            mRvHistory.setVisibility(View.GONE);
        } else {
            mLlEmpty.setVisibility(View.GONE);
            mRvHistory.setVisibility(View.VISIBLE);
        }
    }
}
