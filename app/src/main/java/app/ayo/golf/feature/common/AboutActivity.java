package app.ayo.golf.feature.common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import app.ayo.golf.R;
import app.ayo.golf.abstract_class.BaseActivity;

public class AboutActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        getSupportActionBar().hide();
    }

    public void close(View view){
        finish();
    }
}
