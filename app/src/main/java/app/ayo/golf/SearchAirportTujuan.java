package app.ayo.golf;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import app.ayo.golf.adapter.SearchAirportTujuanAdapter;
import app.ayo.golf.entitytiket.searchairport.Airport;
import app.ayo.golf.entitytiket.searchairport.AirportItem;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.pojo.BookingCourseTransaction;
import app.ayo.golf.retrofit.service.InitRetrofitTiket;
import app.ayo.golf.retrofit.service.flight_api;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SearchAirportTujuan extends AppCompatActivity implements SearchAirportTujuanAdapter.FlightListener {

    private static final String TOKEN = "71cf2d3dec294394e267fbb0bf28916f4198f8d6";
    private SearchAirportTujuanAdapter airportTujuanAdapter;
    private List<Airport> listTujuan = new ArrayList<>();
    private ProgressDialog progressDialog;
    private RecyclerView rvAirportTujuan;
    private EditText etAirportTujuan;
    private int REQUEST_CODE;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_airport_tujuan);
        ButterKnife.bind(this);
        REQUEST_CODE = getIntent().getExtras().getInt("REQUEST_CODE");

        Util.setCustomActionBar(this)
                .setTitle("Search Flight")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                })
                .setRightButton(null, null);

        rvAirportTujuan = (RecyclerView) findViewById(R.id.rv_searchAirportTujuan);
        etAirportTujuan = (EditText) findViewById(R.id.et_searchAirportTujuan);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvAirportTujuan.setLayoutManager(layoutManager);
        rvAirportTujuan.setHasFixedSize(true);

        airportTujuanAdapter = new SearchAirportTujuanAdapter(listTujuan, this);
        rvAirportTujuan.setAdapter(airportTujuanAdapter);

        etAirportTujuan.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                searchAirportTujuan(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        searchAirportTujuan("");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait....");
        progressDialog.show();
    }

    //INI kan activitynya ?
    private void searchAirportTujuan(String s) {
        Call<AirportItem> call = InitRetrofitTiket.getServiceTiket(flight_api.class).searchAirport(TOKEN, "json", s);
        call.enqueue(new Callback<AirportItem>() {
            @Override
            public void onResponse(Call<AirportItem> call, Response<AirportItem> response) {
                if (response.isSuccessful()) {
                    List<Airport> airports = response.body().getAllAirport().getAirport();
                    airportTujuanAdapter.updateList(airports);
                    airportTujuanAdapter.notifyDataSetChanged();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<AirportItem> call, Throwable t) {
                Toast.makeText(SearchAirportTujuan.this, t.getMessage().toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void flightSelected(Airport airport) {
        setResult(REQUEST_CODE, new Intent().setData(Uri.parse(new Gson().toJson(airport))));
        finish();
    }
}
