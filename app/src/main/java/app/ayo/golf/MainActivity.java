package app.ayo.golf;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import app.ayo.golf.adapter.BannerAdapter;
import app.ayo.golf.feature.common.AboutActivity;
import app.ayo.golf.helper.SessionManager;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.pojo.APIResponse;
import app.ayo.golf.retrofit.pojo.Banner;
import app.ayo.golf.retrofit.pojo.Golfer;
import app.ayo.golf.retrofit.service.ApiServices;
import app.ayo.golf.retrofit.service.InitRetrofit;
import app.ayo.golf.retrofit.service.InitRetrofit2;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    int[] datagambar = {R.drawable.banner1, R.drawable.banner2, R.drawable.banner3, R.drawable.banner4, R.drawable.banner5};

    //=============== This Variable Only For View ================================//
    @BindView(R.id.tv_book_main)
    TextView tvBookMain;
    @BindView(R.id.tv_cc_main)
    TextView tvCcMain;
    @BindView(R.id.tv_profile_main)
    TextView tvProfileMain;
    @BindView(R.id.tv_ne_main)
    TextView tvNeMain;
    @BindView(R.id.tv_entertaint_main)
    TextView tvEntertaintMain;
    @BindView(R.id.tv_marketplace_main)
    TextView tvMarketplaceMain;
    //============================================================================//
    @BindView(R.id.vpbanner)
    ViewPager vpbanner;
    @BindView(R.id.circleindicator)
    CircleIndicator circleindicator;
    @BindView(R.id.llbook)
    LinearLayout llbook;
    @BindView(R.id.llcoaching)
    LinearLayout llcoaching;
    @BindView(R.id.llnews)
    LinearLayout llnews;
    @BindView(R.id.llprofile)
    LinearLayout llprofile;
    @BindView(R.id.llentertainment)
    LinearLayout llentertainment;
    @BindView(R.id.llmarketplace)
    LinearLayout llmarketplace;
    //DialogInterface.OnClickListener listener;
    private long lastPressedTime;
    private static final int PERIOD = 2000;
    SessionManager sesi;
    Golfer mOwn;

    //====== Banner Variable =====//
    private int mCurrentPosition;
    private int mScrollState;
    private int currentPage = -1;
    private static final int NUM_PAGES = 0;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // init actionbar
        Util.setCustomActionBar(this)
                .setTitle(R.string.app_name)
                .setLeftButton(null, null)
                .setRightButton(null, null)
                .setTransparent(true)
                .enableElevation(true)
                .setMenus(R.menu.menu_main, new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_about:
                                startActivity(new Intent(MainActivity.this, AboutActivity.class));
                                break;
                        }
                        return true;
                    }
                });
        //========================== Only For View ==========================================//
        tvBookMain.setTypeface(Typeface.DEFAULT_BOLD);
        tvCcMain.setTypeface(Typeface.DEFAULT_BOLD);
        tvEntertaintMain.setTypeface(Typeface.DEFAULT_BOLD);
        tvMarketplaceMain.setTypeface(Typeface.DEFAULT_BOLD);
        tvNeMain.setTypeface(Typeface.DEFAULT_BOLD);
        tvProfileMain.setTypeface(Typeface.DEFAULT_BOLD);
        //=================================================================================//
        mOwn = SessionManager.getProfile(this);
        //get konfigurasi dari retrofit
        ApiServices api = InitRetrofit.getInstanceRetrofit();
        //get request
        Call<APIResponse<ArrayList<Banner>>> call = api.get_banner();
        call.enqueue(new Callback<APIResponse<ArrayList<Banner>>>() {
            @Override
            public void onResponse(Call<APIResponse<ArrayList<Banner>>> call, Response<APIResponse<ArrayList<Banner>>> response) {
                if (response.isSuccessful() && response.body().isSuccessful()) {
                    Log.d("MainActivity", "Success");
                    ArrayList<Banner> data = response.body().data;
                    BannerAdapter mAdapter = new BannerAdapter(data, MainActivity.this);
                    vpbanner.setAdapter(mAdapter);
                    vpbanner.setPageMargin((int) Util.convertDpToPixel(5f, MainActivity.this));
                    vpbanner.setPageMarginDrawable(android.R.color.white);
                    circleindicator.setViewPager(vpbanner);
                    mAdapter.registerDataSetObserver(circleindicator.getDataSetObserver());
                }
            }

            @Override
            public void onFailure(Call<APIResponse<ArrayList<Banner>>> call, Throwable t) {
                Log.d("MainActivity", t.getMessage());
            }
        });

        vpbanner.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                handleScrollState(state);
                mScrollState = state;
            }
        });

        final Handler handler = new Handler();
        final Runnable update = new Runnable() {

            @Override
            public void run() {
                if (currentPage == 8) {
                    currentPage = 0;
                }
                vpbanner.setCurrentItem(currentPage++, true);
                //vpbanner.setCurrentItem(currentPage == 0 ? 8 : 0, true);
            }
        };
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(update);
            }
        }, 4000, 4000);


        /*BannerAdapter mAdapter = new BannerAdapter(datagambar, MainActivity.this);
        vpbanner.setAdapter(mAdapter);
        circleindicator.setViewPager(vpbanner);
        mAdapter.registerDataSetObserver(circleindicator.getDataSetObserver());*/

        /*Button book = (Button) findViewById(R.id.button);

        Button coach = (Button) findViewById(R.id.button2);

        Button news = (Button) findViewById(R.id.button3);

        Button maps = (Button) findViewById(R.id.button4);

        Button ent = (Button) findViewById(R.id.button5);

        Button market = (Button) findViewById(R.id.button6);

        //FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);




        book.setOnClickListener(new View.OnClickListener() {

            public void onClick(View Arg0) {

                Intent i = new Intent(getApplicationContext(), BookActivity.class);

                startActivity(i);
            }
        });

        coach.setOnClickListener(new View.OnClickListener() {

            public void onClick(View Arg0) {
                Intent i = new Intent(getApplicationContext(), CoachingClinicActivity.class);

                startActivity(i);
            }
        });

        news.setOnClickListener(new View.OnClickListener() {

            public void onClick(View Arg0) {
                Intent i = new Intent(getApplicationContext(), NewsActivity.class);

                startActivity(i);
            }

        });

        maps.setOnClickListener(new View.OnClickListener() {

            public void onClick(View Arg0) {
                Intent i = new Intent(getApplicationContext(), CourseLocatorActivity.class);

                startActivity(i);
            }

        });

        ent.setOnClickListener(new View.OnClickListener() {

            public void onClick(View Arg0) {
                Intent i = new Intent(getApplicationContext(), EntActivity.class);

                startActivity(i);
            }
        });

        market.setOnClickListener(new View.OnClickListener() {

            public void onClick(View Arg0) {
                Intent i = new Intent(getApplicationContext(), MarketActivity.class);

                startActivity(i);
            }
        });

        /*fab.setOnClickListener(new View.OnClickListener() {

            public void onClick(View Arg0) {
                Intent i = new Intent(getApplicationContext(), ChatActivity.class);

                startActivity(i);
            }
        });*/
    }

    private void handleScrollState(int state) {
        if (state == ViewPager.SCROLL_STATE_IDLE) {
            setNextItemIfNeeded();
        }
    }

    private void setNextItemIfNeeded() {
        if (!isScrollStateSettling()) {
            handleSetNextItem();
        }
    }

    private void handleSetNextItem() {
        final int lastPosition = vpbanner.getAdapter().getCount() - 1;
        if (mCurrentPosition == 0) {
            vpbanner.setCurrentItem(lastPosition, false);
        } else if (mCurrentPosition == lastPosition) {
            vpbanner.setCurrentItem(0, false);
        }
    }

    private boolean isScrollStateSettling() {
        return mScrollState == ViewPager.SCROLL_STATE_SETTLING;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            switch (event.getAction()) {
                case KeyEvent.ACTION_DOWN:
                    if (event.getDownTime() - lastPressedTime < PERIOD) {
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "Press again to exit",
                                Toast.LENGTH_SHORT).show();
                        lastPressedTime = event.getEventTime();
                    }
                    return true;
            }
        }
        return false;
    }

    @OnClick({R.id.llbook, R.id.llcoaching, R.id.llnews, R.id.llprofile, R.id.llentertainment, R.id.llmarketplace})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llbook:
                startActivity(new Intent(getApplicationContext(), BookActivity.class));
                break;
            case R.id.llcoaching:
                startActivity(new Intent(getApplicationContext(), CoachingClinicActivity.class));
                break;
            case R.id.llnews:
                startActivity(new Intent(getApplicationContext(), NewsActivity.class));
                break;
            case R.id.llprofile:
                mOwn = SessionManager.getProfile(this);
                if (mOwn != null) {
                    startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
                } else {
                    startActivityForResult(new Intent(getApplicationContext(), LoginActivity.class), 202);
                }
                //startActivity(new Intent(getApplicationContext(),CourseLocatorActivity.class));
                break;
            case R.id.llentertainment:
                startActivity(new Intent(getApplicationContext(), EntActivity.class));
                break;
//                startActivity(new Intent(getApplicationContext(), TestActivity.class));
//                break;
            case R.id.llmarketplace:
                startActivity(new Intent(getApplicationContext(), MarketActivity.class));
//                Toast.makeText(this, "Under Construction", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 202 && resultCode == RESULT_OK) {
            // sukses login
            startActivity(new Intent(MainActivity.this, ProfileActivity.class));
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    /*@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //jika tombol BACK ditekan
        if(keyCode == KeyEvent.KEYCODE_BACK){
            Keluar();
        }
        return super.onKeyDown(keyCode,event );
    }
    //method untuk keluar aplikasi menggunakan dialog terlebih dahulu
    private void Keluar() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Exit AyoGolf?");
        builder.setCancelable(false);//tombol BACK tidak bisa tekan

        //Membuat listener untuk tombol DIALOG
        listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which==dialog.BUTTON_POSITIVE){
                    finish(); //keluar aplikasi
                }else if(which== dialog.BUTTON_NEGATIVE){
                    dialog.cancel(); //batal keluar
                }
            }
        };

        //menerapkan listener pada tombol ya dan tidak
        builder.setPositiveButton("Ya", listener);
        builder.setNegativeButton("Tidak", listener);
        builder.show(); //menampilkan dialog
    }*/

}