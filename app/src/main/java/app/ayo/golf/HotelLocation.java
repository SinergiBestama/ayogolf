package app.ayo.golf;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import app.ayo.golf.adapter.HotelAdapter;
import app.ayo.golf.helper.Util;
import app.ayo.golf.entitytiket.findhotel.HotelFind;
import app.ayo.golf.entitytiket.findhotel.SingleResult;
import app.ayo.golf.retrofit.pojo.Hotel;
import app.ayo.golf.retrofit.service.InitRetrofitTiket;
import app.ayo.golf.retrofit.service.search;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HotelLocation extends AppCompatActivity {

    public static final String TOKEN = "71cf2d3dec294394e267fbb0bf28916f4198f8d6";
    private RecyclerView tv;
    private EditText et;
    private HotelAdapter hotelAdapter;
    List<HotelFind> list = new ArrayList<>();
    private ProgressDialog dialog;

//    private static final String LOG_TAG = HotelLocation.class.getName();
//    private HotelAdapter hotelAdapter;
//    RecyclerView rvHotelLocation;
//    List<HotelFind> hotelList = new ArrayList<>();
//    ProgressDialog dialog;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_location);
        ButterKnife.bind(this);

        Util.setCustomActionBar(this)
                .setTitle("Hotel Location")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                })
                .setRightButton(null, null);

        tv = (RecyclerView) findViewById(R.id.home_text);
        et = (EditText) findViewById(R.id.et_find);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        tv.setLayoutManager(layoutManager);
        tv.setItemAnimator(new DefaultItemAnimator());
        tv.setHasFixedSize(true);

        hotelAdapter = new HotelAdapter(list);
        tv.setAdapter(hotelAdapter);


        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tesSearch(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        tesSearch("");

        //----------------------------Progress Dialog----------------------------------------

        dialog = new ProgressDialog(this);
        dialog.setMessage("Please Wait....");
        dialog.setCancelable(true);
        dialog.show();
    }

    private void tesSearch(String param) {

        Call<SingleResult<HotelFind>> call = InitRetrofitTiket
                .getServiceTiket(search.class)
                .findHotel(TOKEN, "json", param);
        call.enqueue(new Callback<SingleResult<HotelFind>>() {
            @Override
            public void onResponse(Call<SingleResult<HotelFind>> call, Response<SingleResult<HotelFind>> response) {
                if (response.body().getDiagnostic().isSuccess()) {
                    SingleResult.ResultList result = response.body().getResults();
                    List<HotelFind> hotels = (List<HotelFind>) response.body().getResults().getResult();

                    hotelAdapter.updateList(hotels);
                    hotelAdapter.notifyDataSetChanged();

                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<SingleResult<HotelFind>> call, Throwable t) {

            }
        });

    }

//        rvHotelLocation = (RecyclerView) findViewById(R.id.rv_hotel_location); //ini lik
//        LinearLayoutManager lm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true);
//        rvHotelLocation.setLayoutManager(lm);
//
//        hotelAdapter = new HotelAdapter(hotelList);
//        rvHotelLocation.setAdapter(hotelAdapter);
//
//        loadHotelLocation();
//    }
//
//    private void loadHotelLocation() {
//        ApiServicesTiket apiServicesTiket = ApiTiket.getApiServicesTiket();
//        retrofit2.Call<APIResponseTiket<List<HotelFind>>> call = apiServicesTiket.getVideoMusic();
//        call.enqueue(new Callback<APIResponseTiket<List<HotelFind>>>() {
//            @Override
//            public void onResponse(retrofit2.Call<APIResponseTiket<List<HotelFind>>> call, Response<APIResponseTiket<List<HotelFind>>> response) {
//                List<HotelFind> result = response.body().data;
//                updateList(result);
//
//                Toast.makeText(getApplicationContext(), "API Tiket Response : " + result.size(), Toast.LENGTH_LONG).show();
//            }
//
//            @Override
//            public void onFailure(retrofit2.Call<APIResponseTiket<List<HotelFind>>> call, Throwable t) {
//
//            }
//        });
//
//    }
//
//    private void updateList(List<HotelFind> li) {
//        hotelList = li;
//        hotelAdapter.setHotelList(hotelList);
//        hotelAdapter.notifyDataSetChanged(); //ini fungsiya sebagai apa ya lik
//
//    }
}
