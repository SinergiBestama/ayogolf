package app.ayo.golf;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import app.ayo.golf.helper.Util;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ForgotPasswordActivity extends AppCompatActivity {

    @BindView(R.id.txtemail)
    TextInputEditText txtemail;
    @BindView(R.id.btnresetpassword)
    Button btnresetpassword;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);

        //init actionbar
        Util.setCustomActionBar(this)
                .setTitle("Reset Password")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                })
                .setRightButton(null, null);
    }

    @OnClick(R.id.btnresetpassword)
    public void onViewClicked() {

    }

    public void onClose(View view) {
        finish();
    }
}
