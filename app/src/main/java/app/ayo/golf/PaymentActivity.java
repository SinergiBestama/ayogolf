package app.ayo.golf;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import app.ayo.golf.helper.PermissionUtil;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.pojo.BookingCourseResult;
import app.ayo.golf.retrofit.pojo.DataBookingDetail;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PaymentActivity extends AppCompatActivity {
    @BindView(R.id.txttotal)
    TextView txttotal;

    private TextView mConfirmItalic;

    private Button btnUploadPic;
    private String picLoad;
    private static final int UPLOAD_PICT = 222;
    private static final int REQUEST_PERMISSION = 252;

    //String[] permissons = {Manifest.permission.READ_EXTERNAL_STORAGE};

    /*@BindView(R.id.txtcode)
    TextView txtcode;
    @BindView(R.id.txtcourse)
    TextView txtcourse;
    @BindView(R.id.txttotal)
    TextView txttotal;
    @BindView(R.id.txtstatus)
    TextView txtstatus;
    @BindView(R.id.txtpayment)
    TextView txtpayment;
    @BindView(R.id.buttonupload)
    Button buttonupload;
    @BindView(R.id.buttonconfirm)
    Button buttonconfirm;
    @BindView(R.id.txttran)
    TextView txttran;
    @BindView(R.id.bank)
    TextView bank;
    @BindView(R.id.btnok)
    Button btnok;*/

    BookingCourseResult bookingResult;
    DataBookingDetail dataBookingDetail;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);

        // init actionbar
        Util.setCustomActionBar(this)
                .setTitle("Payment")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                })
                .setRightButton(null, null);

        mConfirmItalic = (TextView) findViewById(R.id.tv_confirmItalic);
        btnUploadPic = (Button) findViewById(R.id.btn_upload_file);

        mConfirmItalic.setTypeface(null, Typeface.ITALIC);

        // get extras
        bookingResult = getIntent().getParcelableExtra("booking_result");

        // set total price
        setTotalPrice();

        /*Intent in = getIntent();
        String code = in.getStringExtra("code");
        String course = in.getStringExtra("course2");
        String total = in.getStringExtra("total2");
        String status = in.getStringExtra("status");
        String payment = in.getStringExtra("payment");

        txtcode.setText(code);
        txtcourse.setText("Course: " + course);
        txtstatus.setText("Status: " + status);
        txtpayment.setText("Payment: " + payment);
        txttotal.setText("Total: Rp. " + total);

        if (payment.equals("On Course Payment")) {
            txttran.setVisibility(View.GONE);
            bank.setVisibility(View.GONE);
            buttonconfirm.setVisibility(View.GONE);
            buttonupload.setVisibility(View.GONE);
        } else {
            btnok.setVisibility(View.GONE);
        }*/

        btnUploadPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentUploadFile = new Intent(PaymentActivity.this, UploadFileTransact.class);
                startActivity(intentUploadFile);
            }
        });

    }

    private void picLoad() {
        // Do Action

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION) {
            if (PermissionUtil.hashPermission(this, permissions)) {
                picLoad();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    void setTotalPrice() {
        long totalPrice = Long.parseLong(bookingResult.total);
//        NumberFormat format = NumberFormat.getCurrencyInstance();
//        format.setCurrency(Currency.getInstance("IDR"));
//        format.setMinimumFractionDigits(0);
        txttotal.setText(Util.formatToCurrency(totalPrice));
    }

    @OnClick({R.id.btnconfirm})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnconfirm:
                Intent intent = new Intent(this, BookActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                finish();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
