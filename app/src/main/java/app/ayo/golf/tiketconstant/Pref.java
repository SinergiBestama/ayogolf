package app.ayo.golf.tiketconstant;

/**
 * Created by admin on 21/02/2018.
 */

public class Pref {

    public static final String ADULT = "adult";
    public static final String CHILD = "child";
    public static final String END_DATE = "end_date";
    public static final String IS_LOGGED_IN = "is_logged_in";
    public static final String IS_SOCIAL_MEDIA_LOGIN = "is_social_media_login";
    public static final String LAST_JSON_BANNER = "last_json_banner";
    public static final String MY_EMAIL = "my_email";
    public static final String MY_FIRSTNAME = "my_firstname";
    public static final String MY_LASTNAME = "my_lastname";
    public static final String MY_LOGGED_ID = "my_logged_id";
    public static final String MY_PHONE_AREA = "my_phone_area";
    public static final String MY_PHONE_SUFFIX = "my_phone_suffix";
    public static final String MY_SALUTATION = "my_salutation";
    public static final String NEAREST_AIRPORT_CODE = "nearest_airport_code";
    public static final String NIGHT = "night";
    public static final String NOTIF = "notif";
    public static final String ORDER_HASH = "orderHash";
    public static final String ORDER_ID = "orderId";
    public static final String ROOM = "room";
    public static final String SORT = "sort";
    public static final String START_DATE = "start_date";
    public static final String TOPIC_PREFERENCES = "topic_pref";
    public static final String URL_ONE_KLIK = "oneKlikURL";

    public Pref() {
    }
}
