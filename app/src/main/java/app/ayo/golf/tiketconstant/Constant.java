package app.ayo.golf.tiketconstant;

/**
 * Created by admin on 21/02/2018.
 */

public class Constant {

    public static final String YOUTUBE_THUMBNAIL = "https://img.youtube.com/vi/%s/mqdefault.jpg";
    public static final int CACHE_TIME_15_MIN = 900000;
    public static final int CACHE_TIME_1_DAY = 86400000;
    public static final int CACHE_TIME_1_HOUR = 3600000;
    public static final int CACHE_TIME_6_HOUR = 21600000;
    public static final int CACHE_TIME_HALF_MONTH = 1209600000;
    public static final String ERROR = "Error";
    public static final int ERROR_CODE_ORDER_EXPIRED = 207;
    public static final int ERROR_CODE_ORDER_PAID = 210;
    public static final String JSON_PARSE_ERROR = " \"Oops! Something went wrong.\" - Send error report to help us improve your experience";
    public static final String PATTERN_DATE = "yyyy-MM-dd";
    public static final String PATTERN_DATE_TIME = "yyyy-MM-dd kk:mm:ss";
    public static final String SECRET_KEY = "7057c1b2a3705791166735b55db2325d";
    public static final int TIME_250MS = 250;
    public static final int TIME_500MS = 500;
}
