package app.ayo.golf;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import app.ayo.golf.adapter.NewsAdapter;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.pojo.APIResponse;
import app.ayo.golf.retrofit.pojo.News;
import app.ayo.golf.retrofit.service.ApiServices;
import app.ayo.golf.retrofit.service.InitRetrofit;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Hendi on 10/1/17.
 */

public class NewsDetailActivity extends AppCompatActivity {
    //@BindView(R.id.wv_news) WebView wvNews;
    News news;
    @BindView(R.id.txttitle)
    TextView txttitle;
    @BindView(R.id.txtdate)
    TextView txtdate;
    @BindView(R.id.imgnews)
    ImageView imgnews;
    @BindView(R.id.txtcontent)
    TextView txtcontent;
    @BindView(R.id.txtsource)
    TextView txtsource;
    @BindView(R.id.rv_news)
    RecyclerView rvNews;

    ArrayList<News> data = new ArrayList<>();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
        ButterKnife.bind(this);

        // init actionbar
//        getSupportActionBar().setTitle("News");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Util.setCustomActionBar(this)
                .setTitle("News")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                })
                .setRightButton(null, null);

        news = getIntent().getParcelableExtra("news");
        txttitle.setText(news.news_title);
        txtdate.setText(news.publish_date);

        RequestOptions opt = new RequestOptions().placeholder(R.drawable.no_photo);
        Glide.with(getApplicationContext())
                .load(news.news_image)
                .apply(opt)
                .into(imgnews);
        txtcontent.setText(news.news_content);
        txtsource.setText("Source:\n"+news.source_url);
        //wvNews.loadUrl(news.source_url);
//        initRecycleView();
    }


    private void initRecycleView(){
        //get konfigurasi dari retrofit
        ApiServices api = InitRetrofit.getInstanceRetrofit();

        //get request
        Call<APIResponse<ArrayList<News>>> call = api.get_news();

        call.enqueue(new Callback<APIResponse<ArrayList<News>>>() {
            @Override
            public void onResponse(Call<APIResponse<ArrayList<News>>> call, Response<APIResponse<ArrayList<News>>> response) {
                ArrayList<News> data = response.body().data;
                NewsAdapter ar = new NewsAdapter(data,rvNews,NewsDetailActivity.this);
                rvNews.setAdapter(ar);
                rvNews.setLayoutManager(new LinearLayoutManager(NewsDetailActivity.this));

            }

            @Override
            public void onFailure(Call<APIResponse<ArrayList<News>>> call, Throwable t) {
                Log.e("News response", t.getMessage());
            }
        });
        /*NewsAdapter ar = new NewsAdapter(data, rvNews, NewsDetailActivity.this);
        rvNews.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvNews.setAdapter(ar);*/
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                this.finish();
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }
}
