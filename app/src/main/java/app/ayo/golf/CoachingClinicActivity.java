package app.ayo.golf;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import app.ayo.golf.helper.Util;
import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CoachingClinicActivity extends AppCompatActivity {
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.pager)
    ViewPager pager;

    //@BindView(R.id.wvvideo)
    //WebView wvvideo;
    /*@BindView(R.id.rvccvideo)
    RecyclerView rvccvideo;*/


    //String url = "https://www.youtube.com/results?search_query=golf+tips";

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_coach);
        ButterKnife.bind(this);

        Util.setCustomActionBar(this)
                .setTitle("Coaching Clinic")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                })
                .setRightButton(null, null)
                .enableElevation(false);

        tabs.addTab(tabs.newTab().setText("Tips"));
        tabs.addTab(tabs.newTab().setText("Coach"));
        tabs.setSelectedTabIndicatorColor(Color.WHITE);
        ViewCompat.setElevation(tabs, 10);

        PagerAdapter adapter = new FragmentTabs(getSupportFragmentManager());
        pager.setAdapter(adapter);

        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));

        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        /*wvvideo.getSettings().setJavaScriptEnabled(true);
        wvvideo.setWebViewClient(new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url){
                view.loadUrl(url);
                return super.shouldOverrideUrlLoading(view,url);
            }
        });
        wvvideo.loadUrl(url);*/
        //untuk menunggu server response, supaya user tdk bingung saat proses berjalan
        /*final ProgressDialog dialog = new ProgressDialog(CoachingClinicActivity.this);
        //set message dialog
        dialog.setMessage("loading");
        //supaya gabisa di cancel
        dialog.setCancelable(false);
        //tampilkan
        dialog.show();

        //get konfigurasi dari retrofit
        ApiServices api = InitRetrofit.getInstanceRetrofit();

        //get request
        Call<ResponseServer> call = api.get_coachingclinic();

        call.enqueue(new Callback<ResponseServer>() {
            @Override
            public void onResponse(Call<ResponseServer> call, Response<ResponseServer> response) {
                dialog.dismiss();
                if (response.isSuccessful()) {
                    ArrayList<CoachingClinic> data = response.body().getCoachingClinic();
                    CCAdapter ar = new CCAdapter(CoachingClinicActivity.this, rvccvideo, data);
                    rvccvideo.setAdapter(ar);
                    rvccvideo.setLayoutManager(new LinearLayoutManager(CoachingClinicActivity.this));
                } else {
                    Toast.makeText(CoachingClinicActivity.this, response.errorBody().toString(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseServer> call, Throwable t) {
                Log.d("error data", t.getMessage());
                dialog.dismiss();
            }
        });*/

    }
}
