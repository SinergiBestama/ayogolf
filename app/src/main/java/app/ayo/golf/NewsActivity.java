package app.ayo.golf;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import app.ayo.golf.adapter.NewsAdapter;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.pojo.APIResponse;
import app.ayo.golf.retrofit.pojo.News;
import app.ayo.golf.retrofit.service.ApiServices;
import app.ayo.golf.retrofit.service.InitRetrofit;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class NewsActivity extends AppCompatActivity {

    @BindView(R.id.rv_news)
    RecyclerView rvNews;
    ArrayList<News> data = new ArrayList<>();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        ButterKnife.bind(this);
        // init actionbar
//        getSupportActionBar().setTitle("");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Util.setCustomActionBar(this)
                .setTitle("News and Event")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                })
                .setRightButton(null, null);

        // init recycle view
        initRecycleView();

        //untuk menunggu server response, supaya user tdk bingung saat proses berjalan
        final ProgressDialog dialog = new ProgressDialog(NewsActivity.this);
        //set message dialog
        dialog.setMessage("loading");
        //supaya gabisa di cancel
        dialog.setCancelable(false);
        //tampilkan
        dialog.show();

        //get konfigurasi dari retrofit
        ApiServices api = InitRetrofit.getInstanceRetrofit();

        //get request
        Call<APIResponse<ArrayList<News>>> call = api.get_news();

        //get response
        call.enqueue(new Callback<APIResponse<ArrayList<News>>>() {
            @Override
            public void onResponse(Call<APIResponse<ArrayList<News>>> call, Response<APIResponse<ArrayList<News>>> response) {
                dialog.dismiss();

                if (response.isSuccessful() && response.body().isSuccessful()) {
                    data.clear();
                    data.addAll(response.body().data);
                    rvNews.getAdapter().notifyDataSetChanged();
                }
                //else{
                //Toast.makeText(NewsActivity.this, response.body().error_message, Toast.LENGTH_SHORT).show();
                //}
            }

            @Override
            public void onFailure(Call<APIResponse<ArrayList<News>>> call, Throwable t) {
                dialog.dismiss();
                Log.e("response error", t.getMessage());
                Toast.makeText(NewsActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initRecycleView() {
        NewsAdapter ar = new NewsAdapter(data, rvNews, NewsActivity.this);
        rvNews.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvNews.setAdapter(ar);
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                this.finish();
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }
}
