package app.ayo.golf;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimatedStateListDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.session.MediaSession;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.google.android.gms.maps.MapView;

import java.util.ArrayList;
import java.util.List;

import app.ayo.golf.entitytiket.hoteldetailent.AllPhoto;
import app.ayo.golf.entitytiket.hoteldetailent.Photo;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.service.ApiServicesTiket;
import app.ayo.golf.retrofit.service.InitRetrofitTiket;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DetailHotel extends AppCompatActivity {

    private static final String TOKEN = "71cf2d3dec294394e267fbb0bf28916f4198f8d6";
    private ViewPager vp_bannerHotelDetail;
    private MapView mapView;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_detail_hotel);
        ButterKnife.bind(this);

        Util.setCustomActionBar(this)
                .setTitle("Detail Hotel")
                .setTransparent(true)
                .enableElevation(true)
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                })
                .setRightButton(null, null);

        vp_bannerHotelDetail = (ViewPager) findViewById(R.id.vp_detailHotel);
        vp_bannerHotelDetail.setPageMargin((int) Util.convertDpToPixel(5f, DetailHotel.this));
        vp_bannerHotelDetail.setPageMarginDrawable(android.R.color.white);
        mapView = (MapView) findViewById(R.id.mvhotel);
        mapView.onCreate(savedInstanceState);

        //ApiServicesTiket tiket = InitRetrofitTiket.getServiceTiket();
        /**
         * https://api-sandbox.tiket.com/alron-hotel?startdate=2018-03-11&enddate=2018-03-12&night=1&room=1&adult=2&child=0&uid=business%3A4108&token=71cf2d3dec294394e267fbb0bf28916f4198f8d6&output=json
         */

        Call<AllPhoto> call = InitRetrofitTiket.getServiceTiket(ApiServicesTiket.class)
                .detailPhoto("2018-03-11", "2018-03-12", "1", "1", "2", "0", TOKEN, "json");
        call.enqueue(new Callback<AllPhoto>() {
            @Override
            public void onResponse(Call<AllPhoto> call, Response<AllPhoto> response) {
                if (response.isSuccessful()) {
                    List<Photo> data = response.body().getPhoto();
                }
            }

            @Override
            public void onFailure(Call<AllPhoto> call, Throwable t) {

            }
        });
    }
}
