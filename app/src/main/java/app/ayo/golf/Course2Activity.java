package app.ayo.golf;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.MapView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import app.ayo.golf.adapter.CourseListAdapter;
import app.ayo.golf.entityweather.weather1.Example;
import app.ayo.golf.entityweather.weather1.Weather;
import app.ayo.golf.entityweather.weather2.Example2;
import app.ayo.golf.helper.SessionManager;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.pojo.APIResponse;
import app.ayo.golf.retrofit.pojo.CourseItem;
import app.ayo.golf.retrofit.pojo.CourseItemWeather;
import app.ayo.golf.retrofit.pojo.Golfer;
import app.ayo.golf.retrofit.service.ApiServices;
import app.ayo.golf.retrofit.service.ApiServicesWeather;
import app.ayo.golf.retrofit.service.InitRetrofit;
import app.ayo.golf.retrofit.service.InitRetrofitWeather;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Course2Activity extends AppCompatActivity {

    final int REQUEST_LOGIN = 201;
    private static final String q = "Jakarta";
    private static final String API_KEY = "2fc56d498a3b4d87ba298c232e65e6b0";
    private static final String UNITS = "metric";
    @BindView(R.id.rvcourse2)
    RecyclerView rvcourse2;
    CourseItem courseItem;
    private ArrayList<Example2> weathersList = new ArrayList<>();
    ArrayList<CourseItem> courseList = new ArrayList<>();
    int selectedCourse = 0;
    Double latitude, longitude;
    Golfer mGolfer;
    private MapView mapView;
    CourseItem getCourseItem;
    Example2 example2List;
    CourseItemWeather courseItemWeather;
    CourseListAdapter courseListAdapter = new CourseListAdapter(courseList, rvcourse2, weathersList, Course2Activity.this);

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course2);
        ButterKnife.bind(this);

        // init actionbar
        Util.setCustomActionBar(this)
                .setTitle("Golf Courses")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                })
                .setRightButton(null, null);

//        courseItem = getIntent().getParcelableExtra("weather");
//        latitude = courseItem.lat;
//        longitude = courseItem.lng;

        //get konfigurasi dari retrofit
        ApiServices api = InitRetrofit.getInstanceRetrofit();
        //get request
        Call<APIResponse<ArrayList<CourseItem>>> call = api.get_course_list();
        //get response
        call.enqueue(new Callback<APIResponse<ArrayList<CourseItem>>>() {
            @Override
            public void onResponse(Call<APIResponse<ArrayList<CourseItem>>> call, Response<APIResponse<ArrayList<CourseItem>>> response) {
                if (response.isSuccessful()) {

                    if (response.body().isSuccessful()) {
                        loadWeather(q, API_KEY, UNITS);
                        courseList.addAll(response.body().data);
                        CourseListAdapter ar = new CourseListAdapter(courseList, rvcourse2, weathersList, Course2Activity.this);
                        courseListAdapter.setListener(adapterListener);
                        rvcourse2.setAdapter(courseListAdapter);
                        courseListAdapter.updateList2(courseList);
                        courseListAdapter.notifyDataSetChanged();
                        rvcourse2.setLayoutManager(new LinearLayoutManager(Course2Activity.this));
                    } else {
                        // DO what ever if response fail
                    }
                }
            }

            @Override
            public void onFailure(Call<APIResponse<ArrayList<CourseItem>>> call, Throwable t) {
                Log.e("course response", t.getMessage());
            }
        });

        //http://api.openweathermap.org/data/2.5/forecast?q=Jakarta&APPID=2fc56d498a3b4d87ba298c232e65e6b0&units=metric
//        latitude = -6.989085;
//        longitude = 107.647601;
    }

    private void loadWeather(String q, String apiKey, String units) {
        ApiServicesWeather weather = InitRetrofitWeather.getServicesWeather();
        Call<Example2> exampleCall2 = weather.getDetailWeather(q, apiKey, units);
        exampleCall2.enqueue(new Callback<Example2>() {
            @Override
            public void onResponse(Call<Example2> call, Response<Example2> response) {
                if (response.isSuccessful()) {
//                    Toast.makeText(Course2Activity.this, "Success", Toast.LENGTH_LONG).show();
                    example2List = response.body();
                    weathersList.addAll(Collections.singleton(example2List));
                    courseListAdapter.updateList(weathersList);
                    courseListAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(Course2Activity.this, "Gagal", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Example2> call, Throwable t) {

            }
        });
    }

//    private void loadWeather(Double latitude, Double longitude, String API_KEY) {
//        ApiServicesWeather weather = InitRetrofitWeather.getServicesWeather();
//        Call<Example> call2 = weather.getAllWeather(latitude, longitude, API_KEY);
//        call2.enqueue(new Callback<Example>() {
//            @Override
//            public void onResponse(Call<Example> call, Response<Example> response) {
//                if (response.isSuccessful()) {
//                    Toast.makeText(Course2Activity.this, "Success", Toast.LENGTH_LONG).show();
////                    List<Weather> exampleList = response.body().getWeather();
//                    weathersList.addAll(response.body().getWeather());
////                    courseListAdapter.setListener(adapterListener);
////                    rvcourse2.setAdapter(courseListAdapter);
////                    rvcourse2.setLayoutManager(new LinearLayoutManager(Course2Activity.this));
////                    courseListAdapter.updateList(exampleList);
////                    courseListAdapter.notifyDataSetChanged();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Example> call, Throwable t) {
//
//            }
//        });
//        call2.enqueue(new Callback<APIResponse<Weather>>() {
//            @Override
//            public void onResponse(Call<APIResponse<Weather>> call, Response<APIResponse<Weather>> response) {
//                if (response.isSuccessful()){
//                   List<Weather> weathers = (List<Weather>) response.body().data;
//                   weathersList.clear();
//                   weathersList.addAll(weathers);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<APIResponse<Weather>> call, Throwable t) {
//
//            }
//        });
//        call2.enqueue(new Callback<APIResponse<Weather_>>() {
//            @Override
//            public void onResponse(Call<APIResponse<Weather_>> call, Response<APIResponse<Weather_>> response) {
//                if (response.isSuccessful()&&response.body().isSuccessful()){
//                    Weather_ weather1 = response.body().data;
//                    weathers.addAll((Collection<? extends Weather_>) response.body().data);
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<APIResponse<Weather_>> call, Throwable t) {
//
//            }
//        });
//        call2.enqueue(new Callback<Weather_>() {
//            @Override
//            public void onResponse(Call<Weather_> call, Response<Weather_> response) {
//                if (response.isSuccessful() && response.errorBody().) {
//                    response.body();
//
//                    Toast.makeText(Course2Activity.this, "Succes", Toast.LENGTH_SHORT).show();
//                    weathers.addAll(weathers);
//
//                    CourseListAdapter listAdapter = new CourseListAdapter(courseList, rvcourse2, weathers, Course2Activity.this);
//                    listAdapter.setListener(adapterListener);
//                    rvcourse2.setAdapter(listAdapter);
//                    rvcourse2.setLayoutManager(new LinearLayoutManager(Course2Activity.this));
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Weather_> call, Throwable t) {
//                Toast.makeText(Course2Activity.this, "Gagal" + t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });
//        call2.enqueue(new Callback<Weather>() {
//            @Override
//            public void onResponse(Call<Weather> call, Response<Weather> response) {
//                if (response.isSuccessful() && response.body().equals(response)) {
//                    Toast.makeText(Course2Activity.this, "Success", Toast.LENGTH_SHORT).show();
//                    assert weathers.addAll(weathers);
//                    CourseListAdapter listAdapter = new CourseListAdapter(courseList, rvcourse2, weathers, Course2Activity.this);
//                    listAdapter.setListener(adapterListener);
//                    rvcourse2.setAdapter(listAdapter);
//                    rvcourse2.setLayoutManager(new LinearLayoutManager(Course2Activity.this));
////                    CourseListAdapter courseListAdapter = new CourseListAdapter(courseList, rvcourse2, Course2Activity.this);
////                    courseListAdapter.setListener(adapterListener);
////                    rvcourse2.setAdapter(courseListAdapter);
////                    rvcourse2.setLayoutManager(new LinearLayoutManager(Course2Activity.this));
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<Weather> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "Gagal" + t, Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

    private CourseListAdapter.CourseListAdapterListener adapterListener = new CourseListAdapter.CourseListAdapterListener() {
        @Override
        public void onBook(int courseIndex) {
            selectedCourse = courseIndex;
            if (SessionManager.getProfile(getApplicationContext()) != null) {
                CourseItem item = courseList.get(courseIndex);
                Intent i = new Intent(Course2Activity.this, CoFormActivity.class);
                i.putExtra("course", item);
                startActivity(i);
            } else {
                Intent i = new Intent(Course2Activity.this, LoginActivity.class);
                startActivityForResult(i, REQUEST_LOGIN);
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_LOGIN && resultCode == RESULT_OK) {
            // login successful
            Intent intent = new Intent(Course2Activity.this, CoFormActivity.class);
            intent.putExtra("course", this.courseList.get(selectedCourse));
            startActivity(intent);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
