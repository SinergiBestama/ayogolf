package app.ayo.golf;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import java.util.ArrayList;

import app.ayo.golf.adapter.BannerAdapter;
import app.ayo.golf.adapter.EntAdapter;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.pojo.APIResponse;
import app.ayo.golf.retrofit.pojo.Banner;
import app.ayo.golf.retrofit.pojo.Entertainment;
import app.ayo.golf.retrofit.service.ApiServices;
import app.ayo.golf.retrofit.service.InitRetrofit;
import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EntActivity extends AppCompatActivity {

    @BindView(R.id.vpbanner)
    ViewPager vpbanner;
    @BindView(R.id.circleindicator)
    CircleIndicator circleindicator;
    @BindView(R.id.rvent)
    RecyclerView rvent;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_ent);
        ButterKnife.bind(this);

        // init actionbar
        Util.setCustomActionBar(this)
                .setTitle("Entertainment")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                })
                .setRightButton(null, null);

        getBanner();
        getAds();
    }

    private void getAds() {
        ApiServices api = InitRetrofit.getInstanceRetrofit();
        Call<APIResponse<ArrayList<Entertainment>>> call = api.get_ent();
        call.enqueue(new Callback<APIResponse<ArrayList<Entertainment>>>() {
            @Override
            public void onResponse(Call<APIResponse<ArrayList<Entertainment>>> call, Response<APIResponse<ArrayList<Entertainment>>> response) {
                Log.d("Entertainment", response.message());
                if (response.isSuccessful()) {
                    ArrayList<Entertainment> data = new ArrayList<>();
                    data.addAll(response.body().data);
                    EntAdapter ar = new EntAdapter(EntActivity.this, data, rvent);
                    rvent.setAdapter(ar);
                    rvent.setLayoutManager(new LinearLayoutManager(EntActivity.this));
                }
            }

            @Override
            public void onFailure(Call<APIResponse<ArrayList<Entertainment>>> call, Throwable t) {
                Log.e("Error Ent", t.getLocalizedMessage());
            }
        });
    }

    private void getBanner() {
        //get konfigurasi dari retrofit
        ApiServices api = InitRetrofit.getInstanceRetrofit();

        //get request
        Call<APIResponse<ArrayList<Banner>>> call = api.get_banner();

        call.enqueue(new Callback<APIResponse<ArrayList<Banner>>>() {
            @Override
            public void onResponse(Call<APIResponse<ArrayList<Banner>>> call, Response<APIResponse<ArrayList<Banner>>> response) {
                Log.d("MainActivity", "Success");
                ArrayList<Banner> data = response.body().data;
                BannerAdapter mAdapter = new BannerAdapter(data, EntActivity.this);
                vpbanner.setAdapter(mAdapter);
                circleindicator.setViewPager(vpbanner);
                mAdapter.registerDataSetObserver(circleindicator.getDataSetObserver());
            }

            @Override
            public void onFailure(Call<APIResponse<ArrayList<Banner>>> call, Throwable t) {
                Log.d("MainActivity", t.getMessage());
            }
        });
    }


}
