package app.ayo.golf;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import app.ayo.golf.entitytiket.searchairport.Airport;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.service.InitRetrofit;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FlightActivity extends AppCompatActivity implements NumberPicker.OnValueChangeListener {

    private static TextView kotaAsalFlight, kotaTujuanFlight, setTanggalBerangkat, setTanggalPulang,
            jmlPenumpang, kelasPenerbangan;
    private static final int Date_id = 0;
    private static final int Time_id = 1;
    private Switch mSwitch;
    LinearLayout mLayoutHide;
    Calendar calendar;
    DatePickerDialog datePickerDialog;
    int Year, Month, Day, Day1;
    private static final int SECOND_ACTIVITY_REQUEST_CODE = 26;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_flight);
        ButterKnife.bind(this);

        Util.setCustomActionBar(this)
                .setTitle("Flight")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                })
                .setRightButton(null, null);

        final Switch mSwitch = (Switch) findViewById(R.id.switch_flight);
        final LinearLayout mLayoutHide = (LinearLayout) findViewById(R.id.group_layout_hide);
        kotaAsalFlight = (TextView) findViewById(R.id.tv_kota_asal);
        kotaTujuanFlight = (TextView) findViewById(R.id.tv_kota_tujuan);
        setTanggalBerangkat = (TextView) findViewById(R.id.tv_tanggal_berangkat_flight);
        setTanggalPulang = (TextView) findViewById(R.id.tg_tanggal_pulang);
        jmlPenumpang = (TextView) findViewById(R.id.tv_jmlPenumpang);
        kelasPenerbangan = (TextView) findViewById(R.id.tv_kelasPenerbangan);

        String asalAirport = getIntent().getStringExtra("airportKeberangkatan");
        kotaAsalFlight.setText(asalAirport);

        String airportTujuan = getIntent().getStringExtra("airportTujuan");
        kotaTujuanFlight.setText(airportTujuan);
    }

    public static int FROM_CITY = 1001;
    public static int DEST_CITY = 1002;

    public void kotaAsalFlight(final View view) {

        Intent toSearchActivity = new Intent(this, SearchAirport.class);
        startActivityForResult(toSearchActivity, FROM_CITY);

        // setup the alert builder
//        final AlertDialog.Builder builder = new AlertDialog.Builder(FlightActivity.this);
//        builder.setTitle("Pilih Kota Asal");
//
//// add a list
//        final String[] asalkota = {"Jakarta", "Medan", "Palembang", "Bandung", "Yogyakarta", "Palu", "Makassar", "Padang", "Surabaya", "Palangkaraya", "Solo", "Denpasar", "Madura"};
//        builder.setItems(asalkota, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                switch (which) {
//                    case 0: // horse
//                    case 1: // cow
//                    case 2: // camel
//                    case 3: // sheep
//                    case 4: // goat
//                    case 5:
//                    case 6:
//                    case 7:
//                    case 8:
//                    case 9:
//                    case 10:
//                    case 11:
//                    case 12:
//
//                        kotaAsalFlight.setText(asalkota[which].toString());
//                }
//            }
//        });
//
//// create and show the alert dialog
//        AlertDialog dialog = builder.create();
//        dialog.show();
    }

    public void switchTextView(View view) {

        mSwitch = (Switch) findViewById(R.id.switch_flight);
        mLayoutHide = (LinearLayout) findViewById(R.id.group_layout_hide);

        if (mSwitch.isChecked()) {
            mLayoutHide.setVisibility(View.VISIBLE);
        } else {
            mLayoutHide.setVisibility(View.GONE);
        }
    }

    public void kotaTujuanFlight(View view) {

        Intent toSearchActivity = new Intent(this, SearchAirport.class);
        startActivityForResult(toSearchActivity, DEST_CITY);

//        AlertDialog.Builder builder = new AlertDialog.Builder(FlightActivity.this);
//        builder.setTitle("Pilih Kota Tujuan");
//
//        final String[] kotatujuan = {"Jakarta", "Medan", "Palembang", "Bandung", "Yogyakarta", "Palu",
//                "Makassar", "Padang", "Surabaya", "Palangkaraya", "Solo", "Denpasar", "Madura"};
//        builder.setItems(kotatujuan, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                switch (i) {
//                    case 0:
//                    case 1:
//                    case 2:
//                    case 3:
//                    case 4:
//                    case 5:
//                    case 6:
//                    case 7:
//                    case 8:
//                    case 9:
//                    case 10:
//                    case 11:
//                    case 12:
//
//                        kotaTujuanFlight.setText(kotatujuan[i].toString());
//                }
//            }
//        });
//        AlertDialog dialog = builder.create();
//        dialog.show();
    }

    public void tanggalBerangkatFlight(View view) {

        // Get Current Date
        calendar = Calendar.getInstance();
        Year = calendar.get(Calendar.YEAR);
        Month = calendar.get(Calendar.MONTH);
        Day = calendar.get(Calendar.DAY_OF_MONTH);
        Day1 = calendar.get(Calendar.DAY_OF_WEEK);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        setTanggalBerangkat.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                    }
                }, Year, Month, Day);

        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    public void setTanggalPulang(View view) {
        // Get Current Date
        calendar = Calendar.getInstance();
        Year = calendar.get(Calendar.YEAR);
        Month = calendar.get(Calendar.MONTH);
        Day = calendar.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        setTanggalPulang.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                    }
                }, Year, Month, Day);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    public void jmlPenumpang(View view) {

        final Dialog pDialog = new Dialog(FlightActivity.this);
        pDialog.setTitle((Html.fromHtml("<b>" + " Jumlah Penumpang" + "</b>")));
        pDialog.setContentView(R.layout.number_picker_penumpang);
        Button nPenumpang = (Button) pDialog.findViewById(R.id.btn_npPenumpang);
        NumberPicker npDewasa = (NumberPicker) pDialog.findViewById(R.id.numberPickerDewasa);
        final NumberPicker npAnak = (NumberPicker) pDialog.findViewById(R.id.numberPickerAnak);
        final NumberPicker npBayi = (NumberPicker) pDialog.findViewById(R.id.numberPickerBayi);

        //EditText numberPickerChild = (EditText) numberPicker.getChildAt(0);
        final NumberPicker np1 = (NumberPicker) pDialog.findViewById(R.id.numberPickerDewasa);
        final NumberPicker np2 = (NumberPicker) pDialog.findViewById(R.id.numberPickerAnak);
        final NumberPicker np3 = (NumberPicker) pDialog.findViewById(R.id.numberPickerBayi);

        // -------------- np1 ---------------------------------- //
        np1.setMaxValue(20);
        np1.setMinValue(1);
        np1.setWrapSelectorWheel(false);
        np1.setOnValueChangedListener(this);
        //npTamu.setFocusable(true);
        np1.setDescendantFocusability(npDewasa.FOCUS_BLOCK_DESCENDANTS);
        //tamuDialog.getWindow().setLayout(500, 900);
        //numberPickerChild.setInputType(InputType.TYPE_NULL);

        // -------------- np2 ---------------------------------- //
        np2.setMaxValue(10);
        np2.setMinValue(0);
        np2.setWrapSelectorWheel(false);
        np2.setOnValueChangedListener(this);
        //npTamu.setFocusable(true);
        np2.setDescendantFocusability(npAnak.FOCUS_BLOCK_DESCENDANTS);
        //tamuDialog.getWindow().setLayout(500, 900);
        //numberPickerChild.setInputType(InputType.TYPE_NULL);

        // ------------------ np3 ------------------------------ //
        np3.setMaxValue(10);
        np3.setMinValue(0);
        np3.setWrapSelectorWheel(false);
        np3.setOnValueChangedListener(this);
        //npTamu.setFocusable(true);
        np3.setDescendantFocusability(npBayi.FOCUS_BLOCK_DESCENDANTS);
        //tamuDialog.getWindow().setLayout(500, 900);
        //numberPickerChild.setInputType(InputType.TYPE_NULL);

        nPenumpang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (np2.getValue() == 0 && np3.getValue() == 0) {
                    jmlPenumpang.setText(String.valueOf(np1.getValue()) + " Dewasa"); //set Value to TextView
                    pDialog.dismiss();
                } else if (np2.getValue() == 0) {
                    jmlPenumpang.setText(String.valueOf(np1.getValue()) + " Dewasa" + ", " + String.valueOf(np3.getValue()) + " Bayi"); //set Value to TextView
                    pDialog.dismiss();
                } else if (np3.getValue() == 0) {
                    jmlPenumpang.setText(String.valueOf(np1.getValue()) + " Dewasa" + ", " + String.valueOf(np2.getValue()) + " Anak"); //set Value to TextView
                    pDialog.dismiss();
                } else {
                    jmlPenumpang.setText(String.valueOf(np1.getValue()) + " Dewasa" + ", " + String.valueOf(np2.getValue()) + " Anak" + ", " + String.valueOf(np3.getValue()) + " Bayi"); //set Value to TextView
                    pDialog.dismiss();
                }

            }
        });
        pDialog.show();

       /* AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Penumpang");

        final List<String> lables = new ArrayList<>();
        lables.add("1");
        lables.add("2");
        lables.add("3");
        lables.add("4");
        lables.add("5");
        lables.add("6");
        lables.add("7");
        lables.add("8");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, lables);
        builder.setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(FlightActivity.this, "You Have Selected " + lables.get(which), Toast.LENGTH_SHORT).show();

                //Set ke TextView setelah dipilih
                jmlPenumpang.setText(lables.get(which).toString());
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show(); */
    }

    @Override
    public void onValueChange(NumberPicker numberPicker, int i, int i1) {

    }

    public void kelasPenerbangan(View view) {

       /* final Dialog kpDialog = new Dialog(FlightActivity.this);
        kpDialog.setTitle("Kelas Penerbangan");
        kpDialog.setContentView(R.layout.number_picker_tamu);
        Button nkp = (Button) kpDialog.findViewById(R.id.btn_kelasPenerbangan);
        NumberPicker numberPicker = (NumberPicker) kpDialog.findViewById(R.id.numberPickerKP);
        //EditText numberPickerChild = (EditText) numberPicker.getChildAt(0);

        final NumberPicker npkp = (NumberPicker) kpDialog.findViewById(R.id.numberPickerKP);
        final String[] arrayString = new String[]{"Ekonomi", "Bisnis", "Kelas Satu"};
        npkp.setDisplayedValues(arrayString);
        npkp.setMaxValue(2);
        npkp.setMinValue(0);
        npkp.setWrapSelectorWheel(false);
        npkp.setOnValueChangedListener(this);
        npkp.setDisplayedValues(arrayString);
        //npTamu.setFocusable(true);
        npkp.setDescendantFocusability(numberPicker.FOCUS_BLOCK_DESCENDANTS);
        //tamuDialog.getWindow().setLayout(500, 900);
        //numberPickerChild.setInputType(InputType.TYPE_NULL);
        npkp.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int value) {
                return arrayString[value];
            }
        });

        nkp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kelasPenerbangan.setText(String.valueOf(npkp.getValue())); //set Value to TextView
                kpDialog.dismiss();
            }
        });
        kpDialog.show(); */

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((Html.fromHtml("<u><b>" + "Kelas Penerbangan" + "</b></u>")));
        //builder.getWindow().setLayout(500, 900);

        final List<String> lables = new ArrayList<>();
        lables.add("Ekonomi");
        lables.add("Bisnis");
        lables.add("Kelas Satu");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, lables);
        builder.setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Toast.makeText(FlightActivity.this, "You Have Selected " + lables.get(which), Toast.LENGTH_SHORT).show();

                //Menyimpan nilai pada textview saat kita memilih salah satu dari list builder
                kelasPenerbangan.setText(lables.get(which).toString());
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Airport airport = new Gson().fromJson(data.getData().toString(), Airport.class);

        if (resultCode == RESULT_OK) {
            if (requestCode == FROM_CITY) {
                kotaAsalFlight.setText(airport.getAirportName());
            } else if (requestCode == DEST_CITY) {
                kotaTujuanFlight.setText(airport.getAirportName());
            }
        }
    }

    public void searchFlight(View view) {
        Intent toSearchFlight = new Intent(this, SearchFlight.class);
        startActivity(toSearchFlight);
        //Toast.makeText(FlightActivity.this, "Under Construction", Toast.LENGTH_SHORT).show();
    }

}
