package app.ayo.golf;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import app.ayo.golf.helper.Constant;
import app.ayo.golf.helper.GPSTracker;
import app.ayo.golf.retrofit.pojo.Course;
import app.ayo.golf.retrofit.pojo.ResponseServer;
import app.ayo.golf.retrofit.service.ApiServices;
import app.ayo.golf.retrofit.service.InitRetrofit;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CourseLocatorActivity extends FragmentActivity implements OnMapReadyCallback {

    @BindView(R.id.spinnerloc)
    Spinner spinnerloc;
    @BindView(R.id.txtspinner)
    TextView txtspinner;
    private GoogleMap mMap;
    MarkerOptions markerOptions = new MarkerOptions();
    CameraPosition cameraPosition;
    LatLng center, latLng;
    String title;
    GPSTracker gpsTracker;
    Double latitude, longitude;
    //GoogleApiClient apiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_course_locator);

        gpsTracker = new GPSTracker(getApplicationContext());
        gpsTracker.getLocation();
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();
        ButterKnife.bind(this);

        //String[] city = ;
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        /*if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }*/
        //mMap.setMyLocationEnabled(true);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    Constant.PERMISSIONS_REQUEST_GPS
            );

        }
        /*else {
            if (gpsTracker.canGetLocation()){
                gpsTracker.getLocation();
                latitude = gpsTracker.getLatitude();
                longitude = gpsTracker.getLongitude();
                // Add a marker in Sydney and move the camera
                LatLng sydney = new LatLng(latitude,longitude);
                mMap.addMarker(new MarkerOptions().position(sydney).title("My Location").icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_myloc)));
                //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
            }
            else {
                gpsTracker.showSettingGps();
            }

        }*/
        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(latitude,longitude);
        mMap.addMarker(new MarkerOptions().position(sydney).title("My Location").icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_myloc)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        //menampilkan setting gmaps zoom
        mMap.getUiSettings().setZoomControlsEnabled(true);

        //set type maps
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17.0f));
        //GPSTracker.checkPermission(this);
        //mMap.setMyLocationEnabled(true);
        getMarkers();
    }

    private void addMarker(LatLng latlng, final String title) {
        markerOptions.position(latlng);
        markerOptions.title(title);
        mMap.addMarker(markerOptions).setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_courseloc));

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Toast.makeText(getApplicationContext(), marker.getTitle(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getMarkers() {
        ApiServices api = InitRetrofit.getInstanceRetrofit();
        Call<ResponseServer> call = api.get_course();
        call.enqueue(new Callback<ResponseServer>() {
            @Override
            public void onResponse(Call<ResponseServer> call, Response<ResponseServer> response) {
                Log.d("response getmarker", response.message());
                ArrayList<Course> data = response.body().getCourse();

                for (int i = 0; i < data.size(); i++) {
                    title = data.get(i).getCourse_name();
                    latLng = new LatLng(Double.parseDouble(data.get(i).getLat()), Double.parseDouble(data.get(i).getLng()));
                    addMarker(latLng, title);
                }


            }

            @Override
            public void onFailure(Call<ResponseServer> call, Throwable t) {
                Log.d("Response", t.getMessage());
            }
        });
    }
}
