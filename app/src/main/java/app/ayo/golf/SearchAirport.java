package app.ayo.golf;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewStub;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import app.ayo.golf.adapter.SearchAirportAdapter;
import app.ayo.golf.adapter.SearchAirportTujuanAdapter;
import app.ayo.golf.entitytiket.searchairport.Airport;
import app.ayo.golf.entitytiket.searchairport.AirportItem;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.service.InitRetrofitTiket;
import app.ayo.golf.retrofit.service.flight_api;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SearchAirport extends AppCompatActivity implements SearchAirportTujuanAdapter.FlightListener{

    private static final String TOKEN = "71cf2d3dec294394e267fbb0bf28916f4198f8d6";
    private RecyclerView rvSearchAirport;
    private EditText etSearchAirport;
    private SearchAirportAdapter searchAirportAdapter;
    List<Airport> list = new ArrayList<>();
    private ProgressDialog progressDialog;
    ArrayAdapter<String> adapter;
    private ArrayList<String> array_sort = new ArrayList<String>();
    int textLenght = 0;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_airport);
        ButterKnife.bind(this);

        Util.setCustomActionBar(this)
                .setTitle("Search Airport")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                })
                .setRightButton(null, null);

        rvSearchAirport = (RecyclerView) findViewById(R.id.rv_searchAirport);
        etSearchAirport = (EditText) findViewById(R.id.et_searchAirport);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvSearchAirport.setLayoutManager(layoutManager);
        rvSearchAirport.setHasFixedSize(true);

        searchAirportAdapter = new SearchAirportAdapter(list, this);
        rvSearchAirport.setAdapter(searchAirportAdapter);


        etSearchAirport.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //adapter.getFilter().filter(charSequence);
                tesSearch(charSequence.toString());
//                SearchAirport.this.searchAirportAdapter.getFilter().filter(charSequence);
//                searchAirportAdapter.getFilter().filter(charSequence.toString());
            }

            @Override

            public void afterTextChanged(Editable editable) {
//                airportSearch(editable.toString());

            }
        });
        tesSearch("");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait....");
        progressDialog.setCancelable(true);
        progressDialog.show();
    }

    private void tesSearch(String s) {

        Call<AirportItem> call = (Call<AirportItem>) InitRetrofitTiket.getServiceTiket(flight_api.class)
                .searchAirport(TOKEN, "json", s);

        call.enqueue(new Callback<AirportItem>() {
            @Override
            public void onResponse(Call<AirportItem> call, Response<AirportItem> response) {
                if (response.isSuccessful()) ;
                List<Airport> airportList = response.body().getAllAirport().getAirport();

                searchAirportAdapter.updateList(airportList);
                searchAirportAdapter.notifyDataSetChanged();
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<AirportItem> call, Throwable t) {

            }
        });

//        Call<AirportItem<Airport>> call = InitRetrofitTiket.getServiceTiket(flight_api.class)
//                .searchAirport(TOKEN, "json", string);
//        call.enqueue(new Callback<SingleResult<Airport>>() {
//            @Override
//            public void onResponse(Call<SingleResult<Airport>> call, Response<SingleResult<Airport>> response) {
//                if (response.isSuccessful()) {
//                    AllAirport.class.cast(response);
//                    Toast.makeText(getApplicationContext(), "Ini Kalo OK : "+ response.body(), Toast.LENGTH_LONG).show();
//
//
////                    searchAirportAdapter.updateList(airports);
////                    searchAirportAdapter.notifyDataSetChanged();
//
//                    progressDialog.dismiss();
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<SingleResult<Airport>> call, Throwable t) {
//
//            }
//        });

    }

    @Override
    public void flightSelected(Airport airport) {
        Intent intent = new Intent();
        intent.setData(Uri.parse(new Gson().toJson(airport)));
        setResult(RESULT_OK, intent);
        finish();
    }
}
