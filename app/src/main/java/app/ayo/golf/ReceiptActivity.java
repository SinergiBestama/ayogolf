//package app.ayo.golf;
//
//import android.annotation.SuppressLint;
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Typeface;
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.text.TextUtils;
//import android.view.LayoutInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.bumptech.glide.Glide;
//import com.google.gson.Gson;
//import com.google.gson.JsonArray;
//import com.google.gson.reflect.TypeToken;
//
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.Locale;
//
//import app.ayo.golf.helper.SessionManager;
//import app.ayo.golf.helper.Util;
//import app.ayo.golf.retrofit.pojo.APIResponse;
//import app.ayo.golf.retrofit.pojo.BookingCourseResult;
//import app.ayo.golf.retrofit.pojo.BookingCourseTransaction;
//import app.ayo.golf.retrofit.pojo.Golfer;
//import app.ayo.golf.retrofit.service.ApiServices;
//import app.ayo.golf.retrofit.service.InitRetrofit;
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//import de.hdodenhof.circleimageview.CircleImageView;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
//
//public class ReceiptActivity extends AppCompatActivity {
//
//    @BindView(R.id.txtcourse)
//    TextView txtcourse;
//    @BindView(R.id.txtbookdate)
//    TextView txtbookdate;
//    @BindView(R.id.txtflight)
//    TextView txtflight;
//    @BindView(R.id.txtcaddy)
//    TextView txtcaddy;
//    @BindView(R.id.iv_caddy_pic)
//    CircleImageView ivCaddyPic;
//    /*@BindView(R.id.txtprice)
//    TextView txtprice;*/
//    @BindView(R.id.txttotal)
//    TextView txttotal;
//    /*@BindView(R.id.txtpym)
//    TextView txtpym;
//    @BindView(R.id.spinnerpayment)
//    Spinner spinnerpayment;*/
//    @BindView(R.id.txtplayer)
//    TextView txtplayer;
//    @BindView(R.id.btnconfirm)
//    View btnconfirm;
//    @BindView(R.id.rv_flight_detail)
//    RecyclerView rvFlightDetail;
//    /*@BindView(R.id.txtpayment)
//    TextView txtpayment;
//
//
//    String[] payment = {"--Choose Payment--", "On Course Payment", "Bank Transferred"};*/
//
//    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
//    BookingCourseTransaction transaction;
//
//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        setContentView(R.layout.activity_receipt);
//        ButterKnife.bind(this);
//
//        // init actionbar
//        Util.setCustomActionBar(this)
//                .setTitle("Confirmation")
//                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        finish();
//                    }
//                })
//                .setRightButton(null, null);
//
//        transaction = getIntent().getParcelableExtra("transaction");
//
//        txtcourse.setText(transaction.course.course_name);
//        txtbookdate.setText(Util.dateToString(new Date(transaction.booking_date)));
//        txtflight.setText(transaction.flights.size() + " Flight(s)");
//        if (transaction.caddy != null) {
//            txtcaddy.setText(!TextUtils.isEmpty(transaction.caddy.caddy_name) ?
//                    transaction.caddy.caddy_name : "No caddy");
//            Glide.with(this)
//                    .load(transaction.caddy.caddy_pic)
//                    .into(ivCaddyPic);
//        }
//
//        //txtprice.setText(price);
//        setTotalPlayer();
//        setTotalPrice();
//
//        /*ArrayAdapter adapterpym = new ArrayAdapter(this, android.R.layout.simple_spinner_item, payment);
//        spinnerpayment.setAdapter(adapterpym);
//
//        spinnerpayment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                txtpayment.setText(payment[position]);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });*/
//
//        rvFlightDetail.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
//        rvFlightDetail.setAdapter(new FlightDetailAdapter());
//    }
//
//    void setTotalPlayer() {
//        int totalPlayer = 0;
//        for (BookingCourseTransaction.Flight fl : transaction.flights) {
//            totalPlayer += fl.player;
//        }
//        txtplayer.setText(totalPlayer + " Player(s)");
//    }
//
//    void setTotalPrice() {
//        long totalPrice = 0;
//
//        for (BookingCourseTransaction.Flight fl : transaction.flights) {
//            long prc = fl.player * fl.timeType.price;
//            totalPrice = totalPrice + prc;
//        }
//
//        NumberFormat format = NumberFormat.getCurrencyInstance();
//        format.setCurrency(Currency.getInstance("IDR"));
//        format.setMinimumFractionDigits(0);
//        txttotal.setText(Util.formatToCurrency(totalPrice));
//    }
//
//    private void bookingRequest() {
//        Golfer golfer = SessionManager.getProfile(this);
//
//        String golfer_id = golfer.golfer_id;
//        String course_id = transaction.course.course_id;
//        String booked_date = sdf.format(new Date(transaction.booking_date));
//        String caddy = (transaction.caddy != null) ? transaction.caddy.caddy_id : null;
//        String flights;
//        String payment = "2"; // 2 = transfer
//
//        // convert transaction flights into request flights (json array)
//        ArrayList<BookingCourseTransaction.FlightRequest> flightsReq = new ArrayList<>();
//        for (BookingCourseTransaction.Flight fl : transaction.flights) {
//            flightsReq.add(new BookingCourseTransaction.FlightRequest(
//                    String.valueOf(fl.flight_number),
//                    fl.timeType.tee_time,
//                    String.valueOf(fl.player)));
//        }
//        JsonArray flightsJson = (JsonArray) new Gson().toJsonTree(
//                flightsReq,
//                new TypeToken<ArrayList<BookingCourseTransaction.FlightRequest>>() {
//                }.getType());
//        flights = flightsJson.toString();
//
//        // send data to server
//        ApiServices api = InitRetrofit.getInstanceRetrofit();
//        Call<APIResponse<BookingCourseResult>> call = api.booking(golfer_id, course_id, booked_date, caddy, flights, payment);
//        call.enqueue(new Callback<APIResponse<BookingCourseResult>>() {
//            @Override
//            public void onResponse(Call<APIResponse<BookingCourseResult>> call, Response<APIResponse<BookingCourseResult>> response) {
//                if (response.isSuccessful()) {
//                    BookingCourseResult result = response.body().data;
//
//                    Intent i = new Intent(ReceiptActivity.this, PaymentActivity.class);
//                    i.putExtra("booking_result", result);
//                    startActivity(i);
//                } else {
//                    Toast.makeText(ReceiptActivity.this, "Booking failed.", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<APIResponse<BookingCourseResult>> call, Throwable t) {
//                Toast.makeText(ReceiptActivity.this, "Booking failed.", Toast.LENGTH_SHORT).show();
//            }
//        });
//    }
//
//    @OnClick(R.id.btnconfirm)
//    public void onViewClicked() {
//        bookingRequest();
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                this.finish();
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }
//
//    public class FlightDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
//
//        @Override
//        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_flight_detail, parent, false);
//            return new FlightItemHolder(view);
//        }
//
//        @SuppressLint("SetTextI18n")
//        @Override
//        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//            if (holder instanceof FlightItemHolder) {
//                FlightItemHolder itemHolder = (FlightItemHolder) holder;
//
//                BookingCourseTransaction.Flight flight = transaction.flights.get(position);
//                itemHolder.tvFlightNumber.setText("Flight " + flight.flight_number);
//                itemHolder.tvFlightTeeTime.setText(flight.timeType.tee_time + "");
//                itemHolder.tvFlightTeeTime.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
//                itemHolder.tvFlightPrice.setText(Util.formatToCurrency(flight.timeType.price));
//                itemHolder.tvFlightPlayer.setText(flight.player + " Player(s)");
//                long totalPrice = flight.player * flight.timeType.price;
//                itemHolder.tvFlightTotal.setText(Util.formatToCurrency(totalPrice));
//            }
//        }
//
//        @Override
//        public int getItemCount() {
//            return transaction.flights.size();
//        }
//
//        public class FlightItemHolder extends RecyclerView.ViewHolder {
//            @BindView(R.id.tv_flight_number)
//            TextView tvFlightNumber;
//            @BindView(R.id.tv_flight_teetime)
//            TextView tvFlightTeeTime;
//            @BindView(R.id.tv_flight_price)
//            TextView tvFlightPrice;
//            @BindView(R.id.tv_flight_player)
//            TextView tvFlightPlayer;
//            @BindView(R.id.tv_flight_total)
//            TextView tvFlightTotal;
//
//            public FlightItemHolder(View itemView) {
//                super(itemView);
//                ButterKnife.bind(this, itemView);
//            }
//        }
//    }
//}