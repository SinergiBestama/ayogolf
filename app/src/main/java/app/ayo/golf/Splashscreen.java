package app.ayo.golf;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.widget.ProgressBar;


public class Splashscreen extends AppCompatActivity {
    protected static final int TIMER_RUNTIME = 5000;
    protected boolean mbActive;
    //int progress=0;
    ProgressBar simpleProgressBar;
    Handler mHandler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splashscreen);
        simpleProgressBar = (ProgressBar) findViewById(R.id.progressBar2);

        final Thread timerThread = new Thread(){
            @Override
            public void run(){
                mbActive=true;
                try {
                    int waited = 0;
                    while (mbActive && (waited<TIMER_RUNTIME)){
                        sleep(50);
                        if(mbActive){
                            waited+=200;
                            updateProgress(waited);
                        }
                    }
                } catch (InterruptedException e){
                    //case error
                } finally {
                    onContinue();
                }
            }
        };
        timerThread.start();
        /*final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
            }
        }, 3000L); //3000 L = 3 detik*/
        //setProgressValue();
        /*startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();*/
    }

    private void updateProgress(final int timePassed) {
        if(null!= simpleProgressBar){
            final int progress = simpleProgressBar.getMax() * timePassed / TIMER_RUNTIME;
            simpleProgressBar.setProgress(progress);
        }
    }

    private void onContinue() {
        startActivity(new Intent(this,MainActivity.class));
    }


    /*private void setProgressValue() {

        // set the progress
        //simpleProgressBar.setProgress(progress);
        // thread is used to change the progress value
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (progress<100) {
                    try {
                        Thread.sleep(100);
                    } catch (Exception e) {

                    }
                    ++progress;
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            simpleProgressBar.setProgress(progress);
                            /*startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            finish();
                        }
                    });
                }
                while (progress==100){
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    finish();
                }
            }
        });
        thread.start();
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
        if (progress==100){
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }
    }*/
}
