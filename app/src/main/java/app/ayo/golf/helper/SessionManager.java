package app.ayo.golf.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import app.ayo.golf.retrofit.pojo.Golfer;

/**
 * Created by imastudio on 2/9/16.
 */
public class SessionManager {
//    private static final String KEY_TOKEN = "tokenLogin";
//    private static final String KEY_LOGIN = "isLogin";
//    SharedPreferences pref;
//    SharedPreferences.Editor editor;
//
//    int PRIVATE_MODE = 0;
//    Context c;
//
//    //0 agar cuma bsa dibaca hp itu sendiri
//    String PREF_NAME = "OjekOnlinePref";
//
//    //konstruktor
//    public SessionManager(Context c) {
//        this.c = c;
//        pref = c.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
//        editor = pref.edit();
//    }
//
//    //membuat session login
//    public void createLoginSession(String token) {
//        editor.putString(KEY_TOKEN, token);
//        editor.putBoolean(KEY_LOGIN, true);
//        editor.commit();
//        //commit digunakan untuk menyimpan perubahan
//    }
//
//    //mendapatkan token
//    public String getToken() {
//        return pref.getString(KEY_TOKEN, "");
//    }
//
//    //cek login
//    public boolean isLogin() {
//        return pref.getBoolean(KEY_LOGIN, false);
//    }
//
//    //logout user
//    public void logout() {
//        editor.clear();
//        editor.commit();
//    }
//
//    public String getNama() {
//        return pref.getString("nama", "");
//    }
//
//    public void setNama(String nama) {
//        editor.putString("nama", nama);
//        editor.commit();
//    }
//
//    public String getEmail() {
//        return pref.getString("email", "");
//    }
//
//    public void setEmail(String email) {
//        editor.putString("email", email);
//        editor.commit();
//    }
//
//    public String getPhone() {
//        return pref.getString("phone", "");
//    }
//
//    public void setPhone(String phone) {
//        editor.putString("phone", phone);
//        editor.commit();
//    }
//
//    public void setIduser(String id_user) {
//        editor.putString("id_user", id_user);
//        editor.commit();
//    }
//
//    public String getIdUser() {
//        return pref.getString("id_user", "");
//    }

    private static String PREF_NAME = "AyoGolfPref";
    private static String PREF_GOLFER = "own";
    private static Gson gson = new Gson();

    public static Golfer saveProfile(Context context, Golfer newProfile){
        Golfer updated = getProfile(context);

        if(updated == null)
            updated = newProfile;
        else {
            if (newProfile.email != null)
                updated.email = newProfile.email;

            if (newProfile.app_id != null)
                updated.app_id = newProfile.app_id;

            if (newProfile.golfer_name != null)
                updated.golfer_name = newProfile.golfer_name;

            if (newProfile.golfer_phone != null)
                updated.golfer_phone = newProfile.golfer_phone;

            if (newProfile.created_date != null)
                updated.created_date = newProfile.created_date;

            if (newProfile.birth_date != null)
                updated.birth_date = newProfile.birth_date;

            if (newProfile.gender != null)
                updated.gender = newProfile.gender;

            if (newProfile.golfer_pic != null)
                updated.golfer_pic = newProfile.golfer_pic;

            if (newProfile.handicap_index != null)
                updated.handicap_index = newProfile.handicap_index;
        }

        String json = gson.toJson(updated, Golfer.class);
        SharedPreferences pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        pref.edit().putString(PREF_GOLFER, json).commit();
        return updated;
    }

    public static Golfer getProfile(Context context){
        SharedPreferences pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        String json = pref.getString(PREF_GOLFER, null);
        if(json != null)
            return gson.fromJson(json, Golfer.class);
        else
            return null;
    }

    public static void clear(Context context){
        SharedPreferences pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        pref.edit().putString(PREF_GOLFER, null).commit();
    }

}
