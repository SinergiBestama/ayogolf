package app.ayo.golf.helper;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Hendi on 02/06/17.
 */

public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

    final public static int GRID_SPACING_ROW = 1;
    final public static int GRID_SPACING_COLUMN = 2;
    final public static int GRID_SPACING_FULL = 3; //row and column

    private int spanCount;
    private int spacing;
    private boolean includeEdge;
    private int headerNum;
    private boolean useRowSpace, useColumnSpace;

    public GridSpacingItemDecoration(int orientation, int spanCount, int spacing, boolean includeEdge, int headerNum) {
        this.spanCount = spanCount;
        this.spacing = spacing;
        this.includeEdge = includeEdge;
        this.headerNum = headerNum;
        this.useRowSpace = orientation == GRID_SPACING_ROW || orientation == GRID_SPACING_FULL;
        this.useColumnSpace = orientation == GRID_SPACING_COLUMN || orientation == GRID_SPACING_FULL;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view) - headerNum; // item position

        if (position >= 0) {
            int column = position % spanCount; // item column

            if (includeEdge) {
                if(useColumnSpace){
                    outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                    outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)
                }

                if (position < spanCount) { // top edge
                    if(useRowSpace)
                        outRect.top = spacing;
                }
                if(useRowSpace)
                    outRect.bottom = spacing; // item bottom
            } else {
                if(useColumnSpace)
                    outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                if(position!=state.getItemCount()-1) //not last item
                    if(useColumnSpace)
                        outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    if(useRowSpace)
                        outRect.top = (int) (spacing + (spacing/Math.ceil((double)state.getItemCount()/spanCount))); // item top
                }
            }
        } else {
            outRect.left = 0;
            outRect.right = 0;
            outRect.top = 0;
            outRect.bottom = 0;
        }
    }
}

