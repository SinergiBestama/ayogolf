package app.ayo.golf.helper;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AlertDialog;


/**
 * Created by hendi on 8/25/16.
 */
public class PermissionUtil {

    public static boolean hashPermission(Activity activity, String[] permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && activity != null && permissions != null) {
            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(activity, permission) != PermissionChecker.PERMISSION_GRANTED)
                    return false;
            }
        }
        return true;
    }

    public static void requestPermission(final Activity activity, final String[] permissions, final int request) {

        boolean needExplanation = false;
        for (String perm : permissions) {
            needExplanation = ActivityCompat.shouldShowRequestPermissionRationale(activity, perm);
            if (needExplanation)
                break;
        }

        if (needExplanation) {
            new AlertDialog.Builder(activity)
                    .setMessage("This application need some features")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(activity, permissions, request);
                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .create()
                    .show();
        } else
            ActivityCompat.requestPermissions(activity, permissions, request);
    }
}
