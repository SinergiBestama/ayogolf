package app.ayo.golf.helper;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;

import app.ayo.golf.R;

/**
 * Created by admin on 9/30/17.
 */

public class Util {

    public static String convertUnixTimeUtcToDateString(long unixTimeUtc, String dateFormat) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(unixTimeUtc * 1000); // multiply by 1000 to convert in miliseconds
        SimpleDateFormat simpleDataFormat = new SimpleDateFormat(dateFormat); // date format: dd/MM/yyyy hh:mm:ss
        return simpleDataFormat.format(calendar.getTime());
    }

    public static int getDrawableResourceIdByName(Context context, String name) {
        return context.getResources().getIdentifier(name, "drawable", context.getPackageName());
    }
    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    @SuppressLint("NewApi")
    public static int[] getScreenSize(Context context) {
        int[] screenSize = new int[2];
        screenSize[0] = context.getResources().getDisplayMetrics().widthPixels;
        screenSize[1] = context.getResources().getDisplayMetrics().heightPixels;
        return screenSize;
    }

    /* Haries, 11/7/17 */
    public static void showConfirmDialog(Context context, String title, String message, DialogInterface.OnClickListener onConfirmClick) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", onConfirmClick)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        dialog.show();
    }

    /* Haries, 11/6/17 */
    public static void showDatePicker(Context context, String initialDate, DatePickerDialog.OnDateSetListener onDateSetListener) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(stringToDate(initialDate));
        int mYear = calendar.get(Calendar.YEAR);
        int mMonth = calendar.get(Calendar.MONTH);
        int mDay = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dateDialog = new DatePickerDialog(
                context, R.style.PickerTheme, onDateSetListener, mYear, mMonth, mDay);
        dateDialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
        dateDialog.show();
    }

    /* Haries, 11/6/17 */
    public static Date stringToDate(String string) {
        Date date = new Date("1980/01/01");
        if (!TextUtils.isEmpty(string)) try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            date = format.parse(string);
        } catch (Exception ignored) {
        }

        return date;
    }

    /* Haries, 11/6/17 */
    public static String dateToString(Date date) {
        String result = null;
        if (date != null) try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            result = format.format(date);
        } catch (Exception ignored) {
        }

        return result;
    }

    //Bams Format
    public static String dateToAge(Date date) {
        String result = null;
        if (date != null) try {
            SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy", Locale.US);
            result = format.format(date);
        } catch (Exception e) {
        }
        return result;
    }


    public static String formatDate(String strDate, String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.US);
        Date date = stringToDate(strDate);
        return formatter.format(date);
    }

    /* Haries, 11/13/17 */
    public static String formatToCurrency(Long number) {
        NumberFormat format = NumberFormat.getCurrencyInstance();
        format.setCurrency(Currency.getInstance("IDR"));
        format.setMinimumFractionDigits(0);
        return format.format(number);
    }

    /* Haries, 11/7/17 */
    public static CustomActionBar setCustomActionBar(AppCompatActivity activity) {
        activity.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        activity.getSupportActionBar().setDisplayShowCustomEnabled(true);
        activity.getSupportActionBar().setCustomView(R.layout.custom_action_bar);
        View view = activity.getSupportActionBar().getCustomView();
        Toolbar parent = (Toolbar) view.getParent();
        parent.setPadding(0, 0, 0, 0);//for tab otherwise give space in tab
        parent.setContentInsetsAbsolute(0, 0);
        return new CustomActionBar(activity.getSupportActionBar());
    }

    public static class CustomActionBar {
        ImageView mIvLeft;
        ImageView mIvRight;
        TextView mTvTitle;
        View mBtnOverflow;
        ActionBar mActionBar;
        float mElevation;
        Context mContext;

        public CustomActionBar(ActionBar actionBar) {
            mActionBar = actionBar;
            mElevation = mActionBar.getElevation();
            mContext = mActionBar.getCustomView().getContext();
            mIvLeft = (ImageView) mActionBar.getCustomView().findViewById(R.id.btn_left);
            mIvRight = (ImageView) mActionBar.getCustomView().findViewById(R.id.btn_right);
            mTvTitle = (TextView) mActionBar.getCustomView().findViewById(R.id.tv_title);
            mBtnOverflow = mActionBar.getCustomView().findViewById(R.id.btn_overflow);
            setMenus(null, null);
        }

        public CustomActionBar setTitle(String title) {
            if (mTvTitle != null) mTvTitle.setText(title);
            return this;
        }

        public CustomActionBar setTitle(int resID) throws Resources.NotFoundException {
            if (mTvTitle != null) mTvTitle.setText(
                    mContext.getResources().getString(resID)
            );
            return this;
        }

        public CustomActionBar setLeftButton(Integer resID, View.OnClickListener onClickListener) {
            if (mIvLeft != null) {
                mIvLeft.setVisibility((resID == null || onClickListener == null) ?
                        View.GONE : View.VISIBLE);

                if (resID != null) mIvLeft.setImageResource(resID);
                mIvLeft.setOnClickListener(onClickListener);
            }
            return this;
        }

        public CustomActionBar setRightButton(Integer resID, View.OnClickListener onClickListener) {
            if (mIvRight != null) {
                mIvRight.setVisibility((resID == null || onClickListener == null) ?
                        View.GONE : View.VISIBLE);

                if (resID != null) mIvRight.setImageResource(resID);
                mIvRight.setOnClickListener(onClickListener);
            }
            return this;
        }

        public CustomActionBar setMenus(Integer menuResID, PopupMenu.OnMenuItemClickListener onMenuClickListener) {
            if (mBtnOverflow != null) {
                mBtnOverflow.setVisibility((menuResID == null || onMenuClickListener == null) ?
                        View.GONE : View.VISIBLE);

                final PopupMenu popupMenu = new PopupMenu(mContext, mBtnOverflow);
                if (menuResID != null)
                    popupMenu.getMenuInflater().inflate(menuResID, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(onMenuClickListener);
                mBtnOverflow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupMenu.show();
                    }
                });
            }
            return this;
        }

        public CustomActionBar enableElevation(boolean enable) {
            if (mActionBar != null) mActionBar.setElevation(enable ? mElevation : 0);
            return this;
        }


        public CustomActionBar setTransparent(boolean enable) {
            if (mActionBar != null) {
                enableElevation(!enable);
                if (enable) {
                    mActionBar.getCustomView().setBackgroundResource(R.drawable.bg_gradation_dark_descend);
                    mActionBar.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    mActionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                } else {
                    ColorDrawable colorPrimary = new ColorDrawable(ContextCompat.getColor(mContext, R.color.colorPrimary));
                    mActionBar.getCustomView().setBackground(colorPrimary);
                    mActionBar.setBackgroundDrawable(colorPrimary);
                    mActionBar.setStackedBackgroundDrawable(colorPrimary);
                }
            }
            return this;
        }
    }
}
