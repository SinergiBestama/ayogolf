package app.ayo.golf.helper;

import android.net.Uri;

import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by hendi on 6/8/17.
 */

public class YoutubeHelper {

    /**
     * https://stackoverflow.com/questions/24048308/how-to-get-the-video-id-from-a-youtube-url-with-regex-in-java
     *
     * Haries, 9/8/17 - new regex
     * https://stackoverflow.com/questions/35435555/extract-youtube-id-with-or-without-regex/35436389
     */
    public static String getYTId(String youtubeUrl) {
        String vId = null;
//        String regEx = "^https?://.*(?:youtu.be/|v/|u/\\w/|embed/|watch?v=)([^#&?]*).*$";
        /* Haries, 9/8/17 *///new regex
        String regEx = "^(?:(?:https?:\\/\\/)?(?:www\\.)?)?(youtube(?:-nocookie)?\\.com|youtu\\.be)\\/.*?(?:embed|e|v|watch\\?.*?v=)?\\/?([a-z0-9]+)";
        Pattern pattern = Pattern.compile(
                regEx, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(youtubeUrl);
        if (matcher.find()){
            vId = matcher.group(2);
        }
        return vId;
    }

    public static String getHQThumbnail(String vID) {
        return String.format("http://img.youtube.com/vi/%s/hqdefault.jpg", vID);
    }

    public static String videoInfoUrl(String vID) {
        return "http://www.youtube.com/get_video_info?&video_id=" + vID;
    }

    public static String getDashUrl(String data) throws UnsupportedEncodingException {
        String url = "http://abc.com?" + data;
        return Uri.parse(url).getQueryParameter("dashmpd");
    }

    /* Haries, 8/2/17 */
    public static String getFullUrl(String videoID){
        return "http://www.youtube.com/watch?v=" + videoID;
    }
}

