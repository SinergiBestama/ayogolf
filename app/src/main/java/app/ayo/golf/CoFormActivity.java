package app.ayo.golf;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import app.ayo.golf.adapter.FlightButtonAdapter;
import app.ayo.golf.adapter.GolferAdapter;
import app.ayo.golf.adapter.MySpinnerAdapter;
import app.ayo.golf.adapter.TeetimeAdapter;
import app.ayo.golf.helper.SessionManager;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.pojo.APIResponse;
import app.ayo.golf.retrofit.pojo.BookingCourseDetails;
import app.ayo.golf.retrofit.pojo.BookingCourseResult;
import app.ayo.golf.retrofit.pojo.BookingCourseTransaction;
import app.ayo.golf.retrofit.pojo.DataBookingDetail;
import app.ayo.golf.retrofit.pojo.Golfer;
import app.ayo.golf.retrofit.pojo.TeeTime;
import app.ayo.golf.retrofit.service.ApiServices;
import app.ayo.golf.retrofit.service.InitRetrofit;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class CoFormActivity extends AppCompatActivity {
    @BindView(R.id.buttongolfer)
    Button buttongolfer;
    @BindView(R.id.tv_date_result)
    TextView tvDateResult;
    @BindView(R.id.iv_caddy_pic)
    ImageView ivCaddyPic;
    @BindView(R.id.tv_caddy_name)
    TextView tvCaddyName;
    @BindView(R.id.tv_total_price)
    TextView tvTotalPrice;
    @BindView(R.id.tv_flight_number)
    TextView tvFlightNumber;
    @BindView(R.id.rv_flight_button)
    public RecyclerView rvFlightButton;
    @BindView(R.id.vp_flights)
    public ViewPager vpFlights;
    public BookingCourseTransaction transaction = new BookingCourseTransaction();
    public static Calendar newDate = Calendar.getInstance();
    DatePickerDialog datePickerDialog;
    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    ArrayList<TeeTime> ttAndPrice = new ArrayList<>();
    List<String> player = Arrays.asList("1", "2", "3", "4");
    List<String> sessionTime = Arrays.asList("AM", "PM");

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_co_form);
        ButterKnife.bind(this);

        transaction.course = getIntent().getParcelableExtra("course");

        // init actionbar
        Util.setCustomActionBar(this)
                .setTitle("Booking Form")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                })
                .setRightButton(null, null);

        // load tee time and prices
        loadTeeTime();
    }

    private void loadTeeTime() {
        ApiServices api = InitRetrofit.getInstanceRetrofit();
        Call<APIResponse<ArrayList<TeeTime>>> call = api.getTeeTimeAndPrice(transaction.course.course_id);
        call.enqueue(new Callback<APIResponse<ArrayList<TeeTime>>>() {
            @Override
            public void onResponse(Call<APIResponse<ArrayList<TeeTime>>> call, Response<APIResponse<ArrayList<TeeTime>>> response) {
                if (response.isSuccessful()) {

                    if (response.body().isSuccessful()) {
                        ttAndPrice = response.body().data;
//                        transaction.flights.get(0).timeType = ttAndPrice.get(0);

                        // init flight view
                        initFlightButton();
                        initFlightPager();
                    } else {
                        // DO what ever if response fail
                    }

                } else {
                    Toast.makeText(CoFormActivity.this, response.errorBody().toString(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<APIResponse<ArrayList<TeeTime>>> call, Throwable t) {
            }
        });
    }

    public void setCurrentFlight(int pos) {
        for (int i = 0; i < transaction.flights.size(); i++) {
            transaction.flights.get(i).showing = false;
        }
        transaction.flights.get(pos).showing = true;
        rvFlightButton.getAdapter().notifyDataSetChanged();
        vpFlights.setCurrentItem(pos);
    }

    public void removeFlightAt(int pos) {
        for (int i = 0; i < transaction.flights.size(); i++) {
            transaction.flights.get(i).showing = false;
        }
        transaction.flights.get(pos - 1).showing = true;
        transaction.flights.remove(pos);
        rvFlightButton.getAdapter().notifyDataSetChanged();
        vpFlights.getAdapter().notifyDataSetChanged();
        vpFlights.setCurrentItem(pos - 1);
    }

    private void initFlightButton() {
        FlightButtonAdapter ar = new FlightButtonAdapter(transaction.flights, rvFlightButton, CoFormActivity.this);
        rvFlightButton.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvFlightButton.setAdapter(ar);

        // set flight number label
        tvFlightNumber.setText(transaction.flights.size() + " Flight(s)");
    }

    private void initFlightPager() {
        FlightPagerAdapter adapter = new FlightPagerAdapter(getSupportFragmentManager());
        vpFlights.setAdapter(adapter);
        vpFlights.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                setCurrentFlight(position);
            }

            @Override
            public void onPageSelected(int position) {
                setCurrentFlight(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
//                setCurrentFlight(state);
            }
        });
    }

    public void setTotalPrice() {
        long totalPrice = 0;
        for (BookingCourseTransaction.Flight flight : transaction.flights) {
            int player = flight.player;
//            long price = player * flight.timeType.price;
//            totalPrice += price;
        }

//        NumberFormat format= NumberFormat.getCurrencyInstance();
//        format.setCurrency(Currency.getInstance("IDR"));
//        format.setMinimumFractionDigits(0);
        tvTotalPrice.setText(Util.formatToCurrency(totalPrice));
    }

    private void showDateDialog() {
        String today = Util.dateToString(Calendar.getInstance().getTime());
        Util.showDatePicker(this, today, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                /**
                 * Method ini dipanggil saat kita selesai memilih tanggal di DatePicker
                 */

                /**
                 * Set Calendar untuk menampung tanggal yang dipilih
                 */

                String day = (String) DateFormat.format("EEEE", newDate);
                newDate.set(year, monthOfYear, dayOfMonth);

                /**
                 * Update TextView dengan tanggal yang kita pilih
                 */
                tvDateResult.setText(Util.dateToString(newDate.getTime()));

                // update transaction date
                transaction.booking_date = newDate.getTimeInMillis();
            }
        });
    }

    private boolean validateForm() {
        if (transaction.booking_date == 0 || tvCaddyName == null || ivCaddyPic == null) {
            Toast.makeText(this, "Please select your booking date", Toast.LENGTH_SHORT).show();
            return false;
        }

        for (int i = 0; i < transaction.flights.size(); i++) {
            BookingCourseTransaction.Flight flight = transaction.flights.get(i);
            if (flight.timeType == null) {
                Toast.makeText(this, "Please select your tee time in flight " + flight.flight_number, Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        return true;
    }

    @OnClick({R.id.buttongolfer, R.id.btn_select_date, R.id.btn_select_caddy, R.id.btn_book_now, R.id.btn_add_flight})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_add_flight:
                BookingCourseTransaction.Flight lastFlight = transaction.flights.get(transaction.flights.size() - 1);
                if (lastFlight.player == 4) {
                    BookingCourseTransaction.Flight nextFlight = new BookingCourseTransaction.Flight();
                    nextFlight.player = 1;
                    nextFlight.flight_number = lastFlight.flight_number + 1;
//                    nextFlight.timeType = ttAndPrice.get(0);
                    transaction.flights.add(nextFlight);
                    // notify flight button
                    rvFlightButton.getAdapter().notifyDataSetChanged();
                    // notify flight page
                    vpFlights.getAdapter().notifyDataSetChanged();
                    vpFlights.setCurrentItem(transaction.flights.size() - 1);
                    // set flight number label
                    tvFlightNumber.setText(transaction.flights.size() + " Flight(s)");
                } else {
                    Toast.makeText(this, "Fill all quota player (4 players) in previous flight", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_book_now:
                if (validateForm()) {
                    insertDataToServer();
//                    Intent intent = new Intent(CoFormActivity.this, ReceiptActivity2.class);
//                    intent.putExtra("transaction", transaction);
//                    loadBookingResult("", "", "", "", "", "");
//                    startActivity(intent);
                }
                break;
            case R.id.btn_select_date:
                showDateDialog();
                break;
            case R.id.buttongolfer:
                Toast.makeText(this, "Under construction", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_select_caddy:
                Intent caddyIntent = new Intent(this, CaddyActivity.class);
                startActivityForResult(caddyIntent, 111);
                break;
        }
    }

    private void insertDataToServer() {
        Golfer golfer = SessionManager.getProfile(this);

        String golfer_id = golfer.golfer_id.toString();
        String course_id = transaction.course.course_id;
        String booked_date = dateFormatter.format(new Date(transaction.booking_date)).toString();
        String caddy = (transaction.caddy != null) ? transaction.caddy.caddy_id : null;
        String flights;
        String payment = "2";

        // convert transaction flights into request flights (json array)
        ArrayList<BookingCourseTransaction.FlightRequest> flightsReq = new ArrayList<>();
        for (BookingCourseTransaction.Flight fl : transaction.flights) {
            flightsReq.add(new BookingCourseTransaction.FlightRequest(
                    String.valueOf(fl.flight_number),
                    fl.timeType,
                    String.valueOf(fl.player)));
        }
        JsonArray flightsJson = (JsonArray) new Gson().toJsonTree(
                flightsReq,
                new TypeToken<ArrayList<BookingCourseTransaction.FlightRequest>>() {
                }.getType());
        flights = flightsJson.toString();

        loadBookingResult(golfer_id, course_id, booked_date, caddy, flights, payment);
    }

    private void loadBookingResult(final String golfer_id, final String course_id, final String booked_date, final String caddy, final String flights, final String payment) {
        ApiServices api = InitRetrofit.getInstanceRetrofit();
        Call<APIResponse<DataBookingDetail>> call = api.bookedDetail(golfer_id, course_id, booked_date, caddy, flights, payment);
        call.enqueue(new Callback<APIResponse<DataBookingDetail>>() {
            @Override
            public void onResponse(Call<APIResponse<DataBookingDetail>> call, Response<APIResponse<DataBookingDetail>> response) {
                if (response.isSuccessful() && response.body().isSuccessful()) {
                    DataBookingDetail booking = response.body().data;
                    Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(CoFormActivity.this, ReceiptActivity2.class);
//                    intent.putExtra("transaction", transaction);
//                    loadBookingResult("", "", "", "", "", "");
                    intent.putExtra("booking_detail", booking);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<APIResponse<DataBookingDetail>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Gagal" + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void actionDialogGolfer(ArrayList<Golfer> data2) {

        final Dialog dialog = new Dialog(CoFormActivity.this);
        //set layout custom
        dialog.setContentView(R.layout.activity_golfer);
        final RecyclerView rvgolfer = (RecyclerView) dialog.findViewById(R.id.rvgolfer);
        final Button btnchoosegolfer = (Button) dialog.findViewById(R.id.btnchoosegolfer);

        GolferAdapter ar = new GolferAdapter(data2, rvgolfer, CoFormActivity.this);
        rvgolfer.setAdapter(ar);
        rvgolfer.setLayoutManager(new LinearLayoutManager(CoFormActivity.this));

        dialog.show();

        btnchoosegolfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == 111) { // 111 == request code for CaddyActivity
            transaction.caddy = data.getParcelableExtra("caddy");
            tvCaddyName.setText(transaction.caddy.caddy_name);
            Glide.with(this)
                    .load(transaction.caddy.caddy_pic)
                    .into(ivCaddyPic);
        }
    }

    private class FlightPagerAdapter extends FragmentStatePagerAdapter {

        public FlightPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            BookingCourseTransaction.Flight flight = transaction.flights.get(position);
            FlightFragment frg = new FlightFragment();

            Bundle b = new Bundle();
            b.putParcelable("flight", flight);
            b.putInt("position", position);
            frg.setArguments(b);
            return frg;
        }

        @Override
        public int getCount() {
            return transaction.flights.size();
        }
    }

    public static class FlightFragment extends Fragment {
        BookingCourseTransaction.Flight flight;
        int fragmentPos;
        CoFormActivity act;
        TextView tvPrice, tvDaysTeetime, tvNormalPrice, tvAyoGolfPrice, tvPublishRatePrice;

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            act = (CoFormActivity) getActivity();
            flight = getArguments().getParcelable("flight");
            fragmentPos = getArguments().getInt("position");
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_flight_page, null);
        }

        @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);

            final Spinner spnTeeTime = (Spinner) view.findViewById(R.id.spn_tee_time);
            final Spinner spnPlayer = (Spinner) view.findViewById(R.id.spn_player_number);
            tvPrice = (TextView) view.findViewById(R.id.tv_flight_price);
            tvNormalPrice = view.findViewById(R.id.tv_flight_normal_price);
            tvNormalPrice.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            tvDaysTeetime = view.findViewById(R.id.tv_day_teetime);

            //------------------------------For To View----------------------------//
            tvAyoGolfPrice = view.findViewById(R.id.tv_ayoGolfPrice);
            tvAyoGolfPrice.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            tvPublishRatePrice = view.findViewById(R.id.tv_publishRatePrice);
            tvPublishRatePrice.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

//            setFlightPrice();

//            spnTeeTime.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//                    builder.setTitle((Html.fromHtml("<u><b>" + "Select Session Time" + "</b></u>")));
//                    //builder.getWindow().setLayout(500, 900);
//                    final List<String> lables = new ArrayList<>();
//                    lables.add("AM");
//                    lables.add("PM");
//
//                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, lables);
//                    builder.setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
////                            Toast.makeText(getContext(), "You Have Selected " + lables.get(which), Toast.LENGTH_SHORT).show();
//                            //Menyimpan nilai pada textview saat kita memilih salah satu dari list builder
//                           spnTeeTime.setText(lables.get(which).toString());
//                            ((CoFormActivity) getActivity()).transaction.flights.get(fragmentPos).timeType = flight.timeType;
////                            setFlightPrice();
//                        }
//                    });
//                    AlertDialog dialog = builder.create();
//                    dialog.show();
//                }
//            });

            MySpinnerAdapter<String> teeTimeAdapter = new MySpinnerAdapter<>(act, act.sessionTime);
            spnTeeTime.setAdapter(teeTimeAdapter);
            spnTeeTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    flight.timeType = act.sessionTime.get(i);

                    // update transaction data
                    ((CoFormActivity) getActivity()).transaction.flights.get(fragmentPos).timeType = flight.timeType;

//                    setFlightPrice();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

//            spnPlayer.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    final Dialog tamuDialog = new Dialog(getContext());
//                    tamuDialog.setTitle("Jumlah Tamu");
//                    tamuDialog.setContentView(R.layout.number_picker_tamu);
//                    Button nTamu = (Button) tamuDialog.findViewById(R.id.btn_ntamu);
//                    NumberPicker numberPicker = (NumberPicker) tamuDialog.findViewById(R.id.numberPickerTamu);
//                    //EditText numberPickerChild = (EditText) numberPicker.getChildAt(0);
//                    final NumberPicker npTamu = (NumberPicker) tamuDialog.findViewById(R.id.numberPickerTamu);
//                    npTamu.setMaxValue(4);
//                    npTamu.setMinValue(1);
//                    npTamu.setWrapSelectorWheel(false);
//                    npTamu.setOnValueChangedListener(NumberPicker::scrollBy);
//                    //npTamu.setFocusable(true);
//                    npTamu.setDescendantFocusability(numberPicker.FOCUS_BLOCK_DESCENDANTS);
//                    //tamuDialog.getWindow().setLayout(500, 900);
//                    //numberPickerChild.setInputType(InputType.TYPE_NULL);
//
//                    nTamu.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            spnPlayer.setText(String.valueOf(npTamu.getValue()) + " Tamu"); //set Value to TextView
//                            flight.player = 0;
//                            ((CoFormActivity) getActivity()).transaction.flights.get(fragmentPos).player = flight.player;
//                            tamuDialog.dismiss();
//                        }
//                    });
//                    tamuDialog.show();
//                }
//            });
            MySpinnerAdapter<String> playerAdapter = new MySpinnerAdapter<>(act, act.player);
            spnPlayer.setAdapter(playerAdapter);
            spnPlayer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        flight.player = Integer.parseInt(act.player.get(position));
                    } catch (NumberFormatException e) {
                        flight.player = 1;
                    }

                    // update transaction data
                    ((CoFormActivity) getActivity()).transaction.flights.get(fragmentPos).player = flight.player;

//                    setFlightPrice();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }

//        private void setFlightPrice() {
//            int player = flight.player;
//            long price = flight.timeType.apps_price;
//            long flightPrice = player * price;
//            boolean Days = flight.timeType.price_id == "12";
//
////            NumberFormat format= NumberFormat.getCurrencyInstance();
////            format.setCurrency(Currency.getInstance("IDR"));
////            format.setMinimumFractionDigits(0);
//
//            //Diubah Sementara Ke Price Dulu
//            tvNormalPrice.setText(Util.formatToCurrency(flight.timeType.price));
//            tvPrice.setText(Util.formatToCurrency(price));
//            tvPrice.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
//            tvDaysTeetime.setText(String.valueOf(Days));
//            act.setTotalPrice();
//        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}