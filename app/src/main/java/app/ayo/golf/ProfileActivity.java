package app.ayo.golf;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.util.Calendar;

import app.ayo.golf.abstract_class.BaseActivity;
import app.ayo.golf.helper.ProgressRequestBody;
import app.ayo.golf.helper.SessionManager;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.pojo.APIResponse;
import app.ayo.golf.retrofit.pojo.Golfer;
import app.ayo.golf.retrofit.service.ApiServices;
import app.ayo.golf.retrofit.service.InitRetrofit;
import app.ayo.golf.util.ImageSelector;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends BaseActivity {

    @BindView(R.id.imgprofile)
    CircleImageView imgprofile;
    @BindView(R.id.txtname)
    EditText txtname;
    @BindView(R.id.txtage)
    EditText txtage;
    @BindView(R.id.spinnergender)
    Spinner spinnergender;
    @BindView(R.id.txthandicap)
    EditText txthandicap;
    @BindView(R.id.txtemail)
    EditText txtemail;
    @BindView(R.id.btnsignout)
    View btnsignout;
    @BindView(R.id.progress_layout)
    View progressLayout;
    @BindView(R.id.tv_progress)
    TextView tvProgress;
    @BindView(R.id.tv_phone)
    EditText tvPhone;

    Golfer mGolferProfile;
    boolean isUpdating;
    String[] genderArr = {"Not Specified", "Male", "Female"};

    ImageSelector mImageSelector;
    File proPicFile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        mImageSelector = ImageSelector.create(this, new ImageSelector.ImageSelectorDelegate() {
            @Override
            public void onImageSelected(Uri uri) {
                CropImage.activity(uri).setAspectRatio(1,1)
                        .start(ProfileActivity.this);
            }
        });

        // init actionbar
//        getSupportActionBar().setTitle("Profile");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Util.setCustomActionBar(this)
                .setTitle("Profile")
                .setLeftButton(R.drawable.button_close, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!isUpdating) finish();
                    }
                })
                .setRightButton(R.drawable.button_check_mark, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!isUpdating) updateProfile();
                    }
                });

        // init gender spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_dropdown_item, genderArr);
        spinnergender.setAdapter(adapter);

        mGolferProfile = SessionManager.getProfile(this);
        setProfileInfo();

        loadProfile();
        showProgress(false);

        txtage.setKeyListener(null);
        txtage.setInputType(InputType.TYPE_NULL);
        txtage.setFocusable(false);
    }

    private void setProfileInfo() {
        txtname.setText(mGolferProfile.getGolfer_name());
        txtemail.setText(mGolferProfile.getEmail());
        txtage.setText(mGolferProfile.birth_date);
        txthandicap.setText(mGolferProfile.handicap_index);
        tvPhone.setText(mGolferProfile.golfer_phone);
        spinnergender.setSelection(
                TextUtils.equals(mGolferProfile.gender, "Male") ? 1 :
                        TextUtils.equals(mGolferProfile.gender, "Female") ? 2 : 0
        );

        RequestOptions opt = new RequestOptions().placeholder(R.drawable.default_picture);
        Glide.with(this)
                .load(mGolferProfile.golfer_pic)
                .apply(opt)
                .into(imgprofile);
    }

    void loadProfile() {
        ApiServices api = InitRetrofit.getInstanceRetrofit();
        Call<APIResponse<Golfer>> call = api.profile(mGolferProfile.golfer_id);
        call.enqueue(new Callback<APIResponse<Golfer>>() {
            @Override
            public void onResponse(Call<APIResponse<Golfer>> call, Response<APIResponse<Golfer>> response) {
                if (response.isSuccessful() && response.body().isSuccessful()) {
                    mGolferProfile = response.body().data;
                    SessionManager.saveProfile(ProfileActivity.this, mGolferProfile);

                    if(isViewActive)
                        setProfileInfo();
                }
            }

            @Override
            public void onFailure(Call<APIResponse<Golfer>> call, Throwable t) {

            }
        });
    }

    void showProgress(boolean show) {
        if (show)
            progressLayout.setVisibility(View.VISIBLE);
        else
            progressLayout.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (!isUpdating)
                    this.finish();
                return true;
            case R.id.item_save:
                if (!isUpdating)
                    updateProfile();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (!isUpdating)
            super.onBackPressed();
    }

    @OnClick({R.id.btnsignout, R.id.btn_change_picture, R.id.txtage})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnsignout:
                Util.showConfirmDialog(this,
                        "Signing out", "Do you want to continue?", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                SessionManager.clear(getApplicationContext());
                                finish();
                            }
                        });
                break;
            case R.id.btn_change_picture:
                new AlertDialog.Builder(this)
                        .setTitle("Select image from")
                        .setItems(new String[]{"Camera", "Gallery"},
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        if(i == 0)// camera
                                            mImageSelector.captureImage();
                                        else // gallery
                                            mImageSelector.openGallery();
                                    }
                                })
                        .show();
                break;
            case R.id.txtage:
                Util.showDatePicker(this, txtage.getText().toString(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        Calendar cal = Calendar.getInstance();
                        cal.set(Calendar.YEAR, i);
                        cal.set(Calendar.MONTH, i1);
                        cal.set(Calendar.DAY_OF_MONTH, i2);

                        txtage.setText(Util.dateToString(cal.getTime()));
                    }
                });
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        mImageSelector.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK){
                Uri resultUri = result.getUri();
                proPicFile = new File(resultUri.getPath());
                Glide.with(this)
                        .load(proPicFile)
                        .apply(RequestOptions.circleCropTransform())
                        .into(imgprofile);
            }
        }else
            mImageSelector.onActivityResult(requestCode, resultCode, data);
    }

    private void updateProfile() {
        showProgress(true);
        isUpdating = true;

        String email = txtemail.getText().toString();
        String name = txtname.getText().toString();
        String birth_date = txtage.getText().toString();
        String phone = tvPhone.getText().toString();
        String handicap_index = txthandicap.getText().toString();
        String gender = spinnergender.getSelectedItemPosition() > 0 ?
                genderArr[spinnergender.getSelectedItemPosition()] : "";

        // ini progress listener
        ProgressRequestBody.ProgressListener progressListener = new ProgressRequestBody.ProgressListener() {
            @Override
            public void transferred(final int num, long transferred, long totalSize) {
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            tvProgress.setText(num + "%");
                        } catch (NullPointerException ne) {
                            ne.printStackTrace();
                        }
                    }
                });
            }
        };

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part _file = null;
        if (proPicFile != null && proPicFile.exists()) {
            // init request body
            ProgressRequestBody requestFileBody = new ProgressRequestBody(proPicFile, "multipart/form-data", progressListener);
            _file = MultipartBody.Part.createFormData("pic", proPicFile.getName(), requestFileBody);
        }

        // set request body
        RequestBody _type = RequestBody.create(MediaType.parse("text/plain"), "edit");
        RequestBody _golfer_id = RequestBody.create(MediaType.parse("text/plain"), mGolferProfile.golfer_id);
        RequestBody _name = RequestBody.create(MediaType.parse("text/plain"), name);
        RequestBody _phone = RequestBody.create(MediaType.parse("text/plain"), phone);
        RequestBody _email = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody _gender = RequestBody.create(MediaType.parse("text/plain"), gender);
        RequestBody _handicap_index = RequestBody.create(MediaType.parse("text/plain"), handicap_index);
        RequestBody _birth_date = RequestBody.create(MediaType.parse("text/plain"), birth_date);

        ApiServices api = InitRetrofit.getInstanceRetrofit(false);
        Call<APIResponse<Golfer>> call = api.edit_profile(_type, _golfer_id, _name, _phone,
                _gender, _handicap_index, _birth_date, _email, _file);
        call.enqueue(new Callback<APIResponse<Golfer>>() {
            @Override
            public void onResponse(Call<APIResponse<Golfer>> call, Response<APIResponse<Golfer>> response) {
                showProgress(false);
                isUpdating = false;

                if (response.isSuccessful() && response.body().isSuccessful()) {
                    Toast.makeText(ProfileActivity.this, "Update Success!", Toast.LENGTH_SHORT).show();
                    loadProfile();
                } else {
                    Toast.makeText(ProfileActivity.this, "Update Failed!", Toast.LENGTH_SHORT).show();
                    Log.e("upload error", response.body().error_message);
                }
            }

            @Override
            public void onFailure(Call<APIResponse<Golfer>> call, Throwable t) {
                showProgress(false);
                isUpdating = false;
                Toast.makeText(ProfileActivity.this, "Update Failed!", Toast.LENGTH_SHORT).show();
                Log.e("error upload", t.getLocalizedMessage());
            }
        });
    }
}
