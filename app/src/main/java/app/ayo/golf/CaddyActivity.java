package app.ayo.golf;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import app.ayo.golf.adapter.CaddyAdapter;
import app.ayo.golf.helper.GridSpacingItemDecoration;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.pojo.APIResponse;
import app.ayo.golf.retrofit.pojo.Caddy;
import app.ayo.golf.retrofit.service.ApiServices;
import app.ayo.golf.retrofit.service.InitRetrofit;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CaddyActivity extends AppCompatActivity {

    @BindView(R.id.rv_caddy) RecyclerView rvCaddy;

    ArrayList<Caddy> data = new ArrayList<>();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caddy);
        ButterKnife.bind(this);

        // init actionbar
        Util.setCustomActionBar(this)
                .setTitle("Select Caddy")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                })
                .setRightButton(null, null);

        // init recycle view
        initRecycleView();

        //untuk menunggu server response, supaya user tdk bingung saat proses berjalan
        final ProgressDialog dialog = new ProgressDialog(CaddyActivity.this);
        //set message dialog
        dialog.setMessage("loading");
        //supaya gabisa di cancel
        dialog.setCancelable(false);
        //tampilkan
        dialog.show();

        //get konfigurasi dari retrofit
        ApiServices api = InitRetrofit.getInstanceRetrofit();

        //get request
        Call<APIResponse<ArrayList<Caddy>>> call = api.get_caddy();

        //get response
        call.enqueue(new Callback<APIResponse<ArrayList<Caddy>>>() {
            @Override
            public void onResponse(Call<APIResponse<ArrayList<Caddy>>> call, Response<APIResponse<ArrayList<Caddy>>> response) {
                dialog.dismiss();

                if(response.isSuccessful() && response.body().isSuccessful()){
                    data.clear();
                    data.addAll(response.body().data);
                    rvCaddy.getAdapter().notifyDataSetChanged();
                }
                else{
                    Toast.makeText(CaddyActivity.this, response.body().error_message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<APIResponse<ArrayList<Caddy>>> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(CaddyActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initRecycleView(){
        final int column = Util.getScreenSize(this)[0] <= Util.convertDpToPixel(720, this)  ? 2 : 3;
        rvCaddy.setLayoutManager(new GridLayoutManager(this, column, LinearLayoutManager.VERTICAL, false));
        int spacingInPixels = (int) Util.convertDpToPixel(10f, this);
        rvCaddy.addItemDecoration(new GridSpacingItemDecoration(GridSpacingItemDecoration.GRID_SPACING_FULL, column, spacingInPixels, true, 0));

        CaddyAdapter ar = new CaddyAdapter(data, rvCaddy, CaddyActivity.this);
        rvCaddy.setAdapter(ar);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
