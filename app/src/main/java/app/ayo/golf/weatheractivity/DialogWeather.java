package app.ayo.golf.weatheractivity;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

import app.ayo.golf.R;
import app.ayo.golf.entityweather.weather2.Example2;
import app.ayo.golf.entityweather.weather2.List;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.service.ApiServicesWeather;
import app.ayo.golf.retrofit.service.InitRetrofitWeather;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.facebook.FacebookSdk.sdkInitialize;

public class DialogWeather extends android.app.DialogFragment {

    public static final int WIDTH = 60;
    public static final int HEIGHT = 80;
    public static final String DATE_FORMAT_WITHOUT_TIME = "dd/MM";
    private static final String ICON_PREFIX = "img_";
    private static final String API_KEY = "2fc56d498a3b4d87ba298c232e65e6b0";
    private static final String UNITS = "metric";
    //    final String q = "jakarta";
    PopupWindow myPopupWindow;
    Dialog myDialog;
    DialogWeather dialogWeather;
    private TextView txtCloseWeather, txtCityCountry,
            txtCurrentDate, tvDescription, tvWindResult, tvTemperatureResult;
    private ImageView weatherIcon;
    private RecyclerView mRecyclerViewWeather;
    DialogWeatherAdapter dialogWeatherAdapter;
    private java.util.List<Example2> weathers = new ArrayList<>();
    private java.util.List<List> lists = new ArrayList<>();
    Example2 example2List;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_dialog_weather, container, false);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        tvDescription = rootView.findViewById(R.id.tv_description);
        tvWindResult = rootView.findViewById(R.id.wind_result);
        tvTemperatureResult = rootView.findViewById(R.id.temperature_result);
        weatherIcon = rootView.findViewById(R.id.weather_icon);
        txtCityCountry = rootView.findViewById(R.id.city_country_weather);
        txtCityCountry.setTypeface(Typeface.DEFAULT_BOLD);
        txtCurrentDate = rootView.findViewById(R.id.current_date_weather);
        txtCloseWeather = rootView.findViewById(R.id.txt_close_weather);
        txtCloseWeather.setTypeface(Typeface.DEFAULT_BOLD);
        Fragment fragment = this;
        txtCloseWeather.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getActivity(), "Button Close", Toast.LENGTH_LONG).show();
                getActivity().getFragmentManager().beginTransaction().remove(fragment).commit();
            }
        });
//        DisplayMetrics dm = new DisplayMetrics();
//        DialogWeather.this.getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
//
//        int width = dm.widthPixels;
//        int height = dm.heightPixels;
//        DialogWeather.this.getActivity().getWindow().setLayout((int)(width*.95),(int)(height*.4));

        mRecyclerViewWeather = rootView.findViewById(R.id.weather_daily_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerViewWeather.setLayoutManager(layoutManager);
        mRecyclerViewWeather.setHasFixedSize(true);
        dialogWeatherAdapter = new DialogWeatherAdapter(lists);
        mRecyclerViewWeather.setAdapter(dialogWeatherAdapter);

        String q = getArguments().getString("message");
        loadWeathers(q, API_KEY, UNITS);

        return rootView;
    }

    private void loadWeathers(String q, String apiKey, String units) {
        ApiServicesWeather weather = InitRetrofitWeather.getServicesWeather();
        Call<Example2> exampleCall = weather.getDetailWeather(q, apiKey, units);
        exampleCall.enqueue(new Callback<Example2>() {
            @Override
            public void onResponse(Call<Example2> call, Response<Example2> response) {
                if (response.isSuccessful()) {
//                    Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_LONG).show();
                    example2List = response.body();
                    weathers.addAll(Collections.singleton(example2List));
                    loadSetView();
                    dialogWeatherAdapter.updateList(example2List.getList());
                    dialogWeatherAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<Example2> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Gagal " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void loadSetView() {
        txtCityCountry.setText(weathers.get(getId()).getCity().getName());
        txtCurrentDate.setText(Util.formatDate(example2List.getList().get(getId()).getDtTxt(), "EEE MMM dd, yyyy"));
        weatherIcon.setImageResource(Util.getDrawableResourceIdByName(getApplicationContext(), ICON_PREFIX + weathers.get(getId()).getList().get(getId()).getWeather().get(getId()).getIcon()));
        tvDescription.setText(weathers.get(getId()).getList().get(getId()).getWeather().get(getId()).getDescription());
        tvWindResult.setText("Wind : " + example2List.getList().get(getId()).getWind().getSpeed() + " m/s");
//        double mTemperature = (double) weathers.get(getId()).getList().get(getId()).getMain().getTemp();
        tvTemperatureResult.setText(String.valueOf(weathers.get(getId()).getList().get(getId()).getMain().getTemp() + " °C"));
    }

    @Override
    public void onResume() {
        super.onResume();
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        int [] screenSize = Util.getScreenSize(getActivity());
        params.width = (int) (0.8 * screenSize[0]);
        params.height = (int) (0.8 * screenSize[1]);
        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);
    }

    //=================================== ADAPTER =========================================================
    public class DialogWeatherAdapter extends RecyclerView.Adapter<DialogWeatherAdapter.DialogViewHolder> {
        java.util.List<List> data;
        Context context;
        Activity c;

        public DialogWeatherAdapter(java.util.List<List> data, Context context, Activity c) {
            this.data = data;
            this.context = context;
            this.c = c;
        }

        public DialogWeatherAdapter(java.util.List<List> weathers) {
            this.data = weathers;
        }

        @Override
        public DialogViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.item_weathers, parent, false);
            return new DialogViewHolder(view);
        }

        @Override
        public void onBindViewHolder(DialogViewHolder holder, int position) {
//            if (holder instanceof DialogViewHolder) {
            final DialogViewHolder viewHolder = (DialogViewHolder) holder;
            viewHolder.tvCurrentDateItem.setText(Util.convertUnixTimeUtcToDateString(data.get(position).getDt(), "dd/MM"));
            viewHolder.tvCurrentTimeItem.setText(Util.convertUnixTimeUtcToDateString(data.get(position).getDt(), "HH:mm"));
            viewHolder.ivIconItem.setImageResource(Util.getDrawableResourceIdByName(getApplicationContext(), ICON_PREFIX + data.get(position).getWeather().get(0).getIcon()));
            viewHolder.tvDescriptionItem.setText(String.valueOf(data.get(position).getMain().getTemp() + " °C"));
            viewHolder.tvDescriptionItem.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            viewHolder.tvTemperatureItem.setText(data.get(position).getWind().getSpeed() + " m/s");
        }

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public void updateList(java.util.List<List> weathers) {
            this.data = weathers;
        }

        public class DialogViewHolder extends RecyclerView.ViewHolder {
            TextView tvCurrentDateItem, tvCurrentTimeItem, tvDescriptionItem, tvTemperatureItem;
            ImageView ivIconItem;

            public DialogViewHolder(View itemView) {
                super(itemView);
                tvCurrentDateItem = itemView.findViewById(R.id.current_date_weather_item);
                tvCurrentTimeItem = itemView.findViewById(R.id.current_time_weather_item);
                tvDescriptionItem = itemView.findViewById(R.id.description_item);
                tvTemperatureItem = itemView.findViewById(R.id.temperature_item);
                ivIconItem = itemView.findViewById(R.id.icon_weather_item);
            }
        }
    }
}