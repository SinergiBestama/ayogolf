package app.ayo.golf;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import app.ayo.golf.adapter.SearchFlightAdapter;
import app.ayo.golf.entitytiket.searchflight.ResultFlight;
import app.ayo.golf.entitytiket.searchflight.ResultSearchFlight;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.service.InitRetrofitTiket;
import app.ayo.golf.retrofit.service.search;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SearchFlight extends AppCompatActivity {

    private RecyclerView rvSearchFlight;
    private SearchFlightAdapter searchFlightAdapter;
    private List<ResultFlight> list = new ArrayList<>();
    private static final String TOKEN = "71cf2d3dec294394e267fbb0bf28916f4198f8d6";

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_flight);
        ButterKnife.bind(this);

        Util.setCustomActionBar(this)
                .setTitle("Search Flight")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                })
                .setRightButton(null, null);

        rvSearchFlight = (RecyclerView) findViewById(R.id.rv_searchFlight);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvSearchFlight.setLayoutManager(layoutManager);
        rvSearchFlight.setHasFixedSize(true);

        searchFlightAdapter = new SearchFlightAdapter(list);
        rvSearchFlight.setAdapter(searchFlightAdapter);

        loadFlightInfo();
    }

    private void loadFlightInfo() {

        Call<ResultFlight> call = InitRetrofitTiket.getServiceTiket(search.class)
                .resultFlight("CGK", "DPS", "2018-03-19", "2018-03-20", "1", "0", "0", TOKEN, "3", "json");

        call.enqueue(new Callback<ResultFlight>() {

                         @Override
                         public void onResponse(Call<ResultFlight> call, Response<ResultFlight> response) {
                             Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_SHORT).show();

                             if (response.isSuccessful()) {
                                 List<ResultFlight> searchFlights = (List<ResultFlight>) response.body();

                                 list.clear();
                                 list.addAll(searchFlights);
                                 searchFlightAdapter.notifyDataSetChanged();
                                 Toast.makeText(getApplicationContext(), "ok", Toast.LENGTH_SHORT).show();
                             }
                         }

                         @Override
                         public void onFailure(Call<ResultFlight> call, Throwable t) {
                             Toast.makeText(SearchFlight.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                         }
                     });
//            @Override
//            public void onResponse(Call<ResultSearchFlight> call, Response<ResultSearchFlight> response) {
//                Log.e("CULOBOYO", "Response recieved !");
//                if (response.isSuccessful()) {
//                    List<ResultFlight> resultFlights = (List<ResultFlight>) response.body();
//                    searchFlightAdapter.updateList(resultFlights);
//                    searchFlightAdapter.notifyDataSetChanged();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResultSearchFlight> call, Throwable t) {
//
//            }
//        });
//            @Override
//            public void onResponse(Call<ResultFlight> call, Response<ResultFlight> response) {
//                if (response.isSuccessful()) {
//                    List<ResultFlight> flightList = (List<ResultFlight>) response.body();
//                    //Toast.makeText(getApplicationContext(), "Data : " + response.body(), Toast.LENGTH_LONG).show();
//
//                    searchFlightAdapter.updateList(flightList);
//                    searchFlightAdapter.notifyDataSetChanged();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResultFlight> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "This Error : " + t.getMessage(), Toast.LENGTH_LONG).show();
//            }
//        });
    }
}
