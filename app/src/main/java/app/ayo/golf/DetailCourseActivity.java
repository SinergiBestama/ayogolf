package app.ayo.golf;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import app.ayo.golf.adapter.FacilitiesAdapter;
import app.ayo.golf.adapter.GalleriesAdapter;
import app.ayo.golf.helper.GPSTracker;
import app.ayo.golf.helper.SessionManager;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.pojo.CourseItem;
import app.ayo.golf.retrofit.pojo.Golfer;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DetailCourseActivity extends AppCompatActivity {

    final int REQUEST_LOGIN = 241;

    @BindView(R.id.btnbook)
    Button btnbook;
    @BindView(R.id.vpbanner)
    ViewPager vpbanner;
    @BindView(R.id.circleindicator)
    CircleIndicator circleindicator;
    @BindView(R.id.rvamenities)
    RecyclerView rvamenities;
    @BindView(R.id.txttotalholes)
    TextView txttotalholes;
    @BindView(R.id.txtpar)
    TextView txtpar;
    @BindView(R.id.txtlength)
    TextView txtlength;
    @BindView(R.id.txtopentime)
    TextView txtopentime;
    @BindView(R.id.txtclosetime)
    TextView txtclosetime;
    //@BindView(R.id.txtclosedate)
    //TextView txtclosedate;
    @BindView(R.id.txtdesigner)
    TextView txtdesigner;
    @BindView(R.id.txtestin)
    TextView txtestin;
    //@BindView(R.id.mvcourse)
    //MapView mvcourse;
    @BindView(R.id.txtcoursename)
    TextView txtcoursename;
    @BindView(R.id.txtaddress)
    TextView txtaddress;
    //@BindView(R.id.txtabout)
    //TextView txtabout;
    //MapView mapView;
    //private GoogleMap mMap;
    //MarkerOptions markerOptions = new MarkerOptions();
    CameraPosition cameraPosition;
    LatLng center, latLng;
    String title;
    GPSTracker gpsTracker;
    Double latitude, longitude;
    //GoogleMap map;
    CourseItem item;
    private GoogleMap mMap;
    private MapView mapView;
    private Marker marker;
    Golfer mOwn;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_course);
        ButterKnife.bind(this);

        // init actionbar
        Util.setCustomActionBar(this)
                .setTitle(null)
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                })
                .setRightButton(null, null)
                .setTransparent(true);

        // init session manager
        mOwn = SessionManager.getProfile(this);

        mapView = (MapView) findViewById(R.id.mvcourse);
        mapView.onCreate(savedInstanceState);
        // get extras
        item = getIntent().getParcelableExtra("course");

        latitude = item.lat;
        longitude = item.lng;
        txtcoursename.setText(item.course_name);
        txtaddress.setText(item.course_address);
        txtclosetime.setText(item.close_time);
        txtdesigner.setText(item.designer);
        txttotalholes.setText(item.holes);
        txtlength.setText(item.length);
        txtestin.setText(item.est_in);
        txtopentime.setText(item.open_time);
        txtpar.setText(item.par);

        getBanner();
        getFacilities();
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                setUpMap(googleMap);
            }
        });
    }

    private void setUpMap(GoogleMap map) {
        mMap = map;

        LatLng sydney = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_courseloc)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 14.0f));
        //menampilkan setting gmaps zoom
        mMap.getUiSettings().setZoomControlsEnabled(true);

    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    private void getFacilities() {
        ArrayList<CourseItem.Amenities> data = item.amenities;
        FacilitiesAdapter ar = new FacilitiesAdapter(data, DetailCourseActivity.this, rvamenities);
        rvamenities.setAdapter(ar);
        //rvamenities.setLayoutManager(new LinearLayoutManager(DetailCourseActivity.this));
        LinearLayoutManager layout = new LinearLayoutManager(DetailCourseActivity.this);
        layout.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvamenities.setLayoutManager(layout);

    }

    private void getBanner() {
        ArrayList<CourseItem.Gallery> gambar = item.galleries;
        GalleriesAdapter mAdapter = new GalleriesAdapter(gambar, DetailCourseActivity.this);
        vpbanner.setAdapter(mAdapter);
        circleindicator.setViewPager(vpbanner);
        mAdapter.registerDataSetObserver(circleindicator.getDataSetObserver());
        /*BannerAdapter mAdapter = new BannerAdapter(datagambar, DetailCourseActivity.this);
        vpbanner.setAdapter(mAdapter);
        circleindicator.setViewPager(vpbanner);
        mAdapter.registerDataSetObserver(circleindicator.getDataSetObserver());*/
    }

    @OnClick(R.id.btnbook)
    public void onViewClicked() {
        if (mOwn != null) {
            openBookingForm();
        } else {
            Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivityForResult(loginIntent, REQUEST_LOGIN);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_LOGIN && resultCode == RESULT_OK) {
            // login success
            openBookingForm();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    void openBookingForm() {
        if (item.course_id.equals("1") || item.course_id.equals("2")) {
            Intent formIntent = new Intent(getApplicationContext(), CoFormActivity.class);
            formIntent.putExtra("course", item);
            startActivity(formIntent);
        }
    }

    /*@Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    Constant.PERMISSIONS_REQUEST_GPS
            );

        }*/
        /*else {
            if (gpsTracker.canGetLocation()){
                gpsTracker.getLocation();
                latitude = gpsTracker.getLatitude();
                longitude = gpsTracker.getLongitude();
                // Add a marker in Sydney and move the camera
                LatLng sydney = new LatLng(latitude,longitude);
                mMap.addMarker(new MarkerOptions().position(sydney).title("My Location").icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_myloc)));
                //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
            }
            else {
                gpsTracker.showSettingGps();
            }

        }*/
    // Add a marker in Sydney and move the camera
        /*LatLng sydney = new LatLng(latitude,longitude);
        mMap.addMarker(new MarkerOptions().position(sydney).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_locator)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        //menampilkan setting gmaps zoom
        //mMap.getUiSettings().setZoomControlsEnabled(true);

        //set type maps
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17.0f));
        //GPSTracker.checkPermission(this);
        //mMap.setMyLocationEnabled(true);
    }*/
}
