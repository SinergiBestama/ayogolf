package app.ayo.golf;

import android.accounts.Account;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import java.io.IOException;

import app.ayo.golf.helper.SessionManager;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.pojo.APIResponse;
import app.ayo.golf.retrofit.pojo.Golfer;
import app.ayo.golf.retrofit.service.ApiServices;
import app.ayo.golf.retrofit.service.InitRetrofit;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    @BindView(R.id.txtemail)
    TextInputEditText txtemail;
    @BindView(R.id.txtpassword)
    TextInputEditText txtpassword;
    @BindView(R.id.btnlogin)
    Button btnlogin;
    @BindView(R.id.txtforgotpass)
    TextView txtforgotpass;
    @BindView(R.id.btnfacebook)
    LoginButton btnfacebook;
    @BindView(R.id.txtcreateacc)
    TextView txtcreateacc;
    @BindView(R.id.btngoogle)
    SignInButton btngoogle;
    @BindView(R.id.sign_out_and_disconnect)
    Button signOutAndDisconnect;
    @BindView(R.id.tilemail)
    TextInputLayout tilemail;
    @BindView(R.id.tilpassword)
    TextInputLayout tilpassword;

    private GoogleApiClient mGoogleApiClient;
    private static final String TAG = "SignInActivity";
    private static final int RC_SIGN_IN = 9001;
    CallbackManager callbackManager;
    private ProgressDialog mProgressDialog;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        //init actionbar
        Util.setCustomActionBar(this)
                .setTitle("Login")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                })
                .setRightButton(null, null);

        initLoginFB();
        initLoginGoogle();
    }

    protected void setGooglePlusButtonText(SignInButton signInButton, String buttonText) {
        // Find the TextView that is inside of the SignInButton and set its text
        for (int i = 0; i < signInButton.getChildCount(); i++) {
            View v = signInButton.getChildAt(i);

            if (v instanceof TextView) {
                TextView tv = (TextView) v;
                tv.setText(buttonText);
                return;
            }
        }
    }

    private void initLoginGoogle() {
        setGooglePlusButtonText(btngoogle, "Continue with Google ");

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void loginGoogle() {
        mGoogleApiClient.clearDefaultAccountAndReconnect();
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void logoutGoogle() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        updateUI(false);
                        // [END_EXCLUDE]
                    }
                });
    }

//    private void revokeAccess() {
//        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
//                new ResultCallback<Status>() {
//                    @Override
//                    public void onResult(Status status) {
//                        // [START_EXCLUDE]
//                        updateUI(false);
//                        // [END_EXCLUDE]
//                    }
//                });
//    }

    @Override
    protected void onStart() {
        super.onStart();

        /*OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            showProgressDialog();
            opr.setResultCallback(  new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideProgressDialog();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("loading");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }


    private void initLoginFB() {
        callbackManager = CallbackManager.Factory.create();
        btnfacebook.setReadPermissions("email", "public_profile");
        btnfacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                String accessToken = loginResult.getAccessToken().getToken();
                oauthToken(accessToken, "fb");
            }

            @Override
            public void onCancel() {
                Toast.makeText(LoginActivity.this, "Login canceled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(LoginActivity.this, "Login failed", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        // ResultFlight returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            //mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
            updateUI(true);

            getAccessToken(acct.getAccount());
        } else {
            // Signed out, show unauthenticated UI.
            updateUI(false);
        }
    }

    private void getAccessToken(final Account account) {
        showProgressDialog();

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String scopes = "oauth2:profile email";
                try {
                    return GoogleAuthUtil.getToken(LoginActivity.this, account, scopes);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (GoogleAuthException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                hideProgressDialog();

                if (s != null) {
                    oauthToken(s, "google");
                } else {
                    Toast.makeText(LoginActivity.this, "Google access token failed", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }.execute();
    }

    private void oauthToken(String token, String channel) {
        ApiServices api = InitRetrofit.getInstanceRetrofit();
        Call<APIResponse<Golfer>> call = api.signup(null, null, null, token, channel);
        call.enqueue(new Callback<APIResponse<Golfer>>() {
            @Override
            public void onResponse(Call<APIResponse<Golfer>> call, Response<APIResponse<Golfer>> response) {
                if (response.isSuccessful() && (response.body().isSuccessful() || response.body().code.equals("201"))) {
                    Golfer golfer = response.body().data;
                    SessionManager.saveProfile(getApplicationContext(), golfer);
                    setResult(RESULT_OK);
                    finish();
                } else {
                    Toast.makeText(LoginActivity.this, "Login error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<APIResponse<Golfer>> call, Throwable t) {
                Toast.makeText(LoginActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateUI(boolean signedIn) {
        if (signedIn) {
            findViewById(R.id.btngoogle).setVisibility(View.GONE);
            findViewById(R.id.sign_out_and_disconnect).setVisibility(View.VISIBLE);
        } else {
            //mStatusTextView.setText(R.string.signed_out);

            findViewById(R.id.btngoogle).setVisibility(View.VISIBLE);
            findViewById(R.id.sign_out_and_disconnect).setVisibility(View.GONE);
        }
    }

    private void loginEmail() {
        String email = txtemail.getText().toString();
        String pass = txtpassword.getText().toString();

        if (txtemail.getText().toString().trim().length() == 0) {
            tilemail.setError("Email is required!");
        } else if (txtpassword.getText().toString().trim().length() == 0) {
            tilpassword.setError("Password is required!");
        }

        ApiServices api = InitRetrofit.getInstanceRetrofit();
        Call<APIResponse<Golfer>> call = api.signin(email, pass);
        call.enqueue(new Callback<APIResponse<Golfer>>() {
            @Override
            public void onResponse(Call<APIResponse<Golfer>> call, Response<APIResponse<Golfer>> response) {
                if (response.isSuccessful() && response.body().isSuccessful()) {
                    Toast.makeText(LoginActivity.this, "Login Success", Toast.LENGTH_SHORT).show();

                    Golfer golfer = response.body().data;
                    SessionManager.saveProfile(getApplicationContext(), golfer);

                    setResult(RESULT_OK);
                    finish();
                } else {
                    Toast.makeText(LoginActivity.this, "Login error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<APIResponse<Golfer>> call, Throwable t) {
                Log.e("Failed Signin response", t.getMessage());
                Toast.makeText(LoginActivity.this, "Login Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @OnClick({R.id.btnlogin, R.id.txtforgotpass, R.id.btnfacebook, R.id.txtcreateacc, R.id.btngoogle, R.id.sign_out_and_disconnect})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnlogin:
                loginEmail();
                break;
            case R.id.txtforgotpass:
                startActivity(new Intent(this, ForgotPasswordActivity.class));
                break;
            case R.id.btnfacebook:
                break;
            case R.id.txtcreateacc:
                startActivity(new Intent(LoginActivity.this, SignupActivity.class));
                finish();
                break;
            case R.id.btngoogle:
                loginGoogle();
                break;
            case R.id.sign_out_and_disconnect:
                logoutGoogle();
                break;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    public void onClose(View view) {
        finish();
    }
}
