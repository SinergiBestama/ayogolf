package app.ayo.golf;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.icu.util.Output;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import app.ayo.golf.adapter.ResultHotelAdapter;
import app.ayo.golf.entitytiket.findhotel.SingleResult;
import app.ayo.golf.entitytiket.hotelresult.Example;
import app.ayo.golf.entitytiket.hotelresult.Result;
import app.ayo.golf.entitytiket.hotelresult.Results;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.service.InitRetrofit;
import app.ayo.golf.retrofit.service.InitRetrofitTiket;
import app.ayo.golf.retrofit.service.search;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HotelSearch extends AppCompatActivity {

    private static final String TOKEN = "71cf2d3dec294394e267fbb0bf28916f4198f8d6";
    private static final String output = "json";
    private RecyclerView rvHotelResult;
    private ResultHotelAdapter resultHotelAdapter;
    List<Result> resultList = new ArrayList<>();
    private ProgressDialog dialog;
    private static final int LONG_DELAY = 3500; // 3.5 seconds
    private static final int SHORT_DELAY = 2000; // 2 seconds

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_search);
        ButterKnife.bind(this);

        Util.setCustomActionBar(this)
                .setTitle("Search Hotel")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                })
                .setRightButton(null, null);

        rvHotelResult = (RecyclerView) findViewById(R.id.rv_hotelSearch);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvHotelResult.setLayoutManager(layoutManager);
        rvHotelResult.setHasFixedSize(true);

        resultHotelAdapter = new ResultHotelAdapter(resultList);
        rvHotelResult.setAdapter(resultHotelAdapter);

        dialog = new ProgressDialog(this);
        dialog.setMessage("Please Wait....");
        dialog.setCancelable(true);
        dialog.show();

//        Runnable progressRunnable = new Runnable() {
//            @Override
//            public void run() {
//                dialog.cancel();
//                for (int i = 0; i < 4; i++) {
//                    Toast.makeText(getApplicationContext(), "Something Wrong", Toast.LENGTH_LONG).show();
//                }
//            }
//        };
//        Handler pdCanceller = new Handler();
//        pdCanceller.postDelayed(progressRunnable, 6000);

//        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialogInterface) {
//                rvHotelResult.setVisibility(View.GONE);
//            }
//        });

        String q = getIntent().getStringExtra("HotelTujuan");
        String startdate = getIntent().getStringExtra("tanggalCheckin");
        String enddate = getIntent().getStringExtra("tanggalCheckout");
        String room = getIntent().getStringExtra("kamarHotel");
        String adult = getIntent().getStringExtra("tamuHotel");

        loadHotelResult(q, startdate, "", enddate, room, adult, "");
    }

    private void loadHotelResult(final String q, final String startdate, final String night, final String enddate, final String room, final String adult, final String child) {

        Call<SingleResult<Result>> call = InitRetrofitTiket.getServiceTiket(search.class)
                .resultHotel(q, startdate, "", enddate, room, adult, "0", TOKEN, "json");
        call.enqueue(new Callback<SingleResult<Result>>() {
            @Override
            public void onResponse(Call<SingleResult<Result>> call, Response<SingleResult<Result>> response) {
                if (response.isSuccessful()) ;
                List<Result> resultListHotel = (List<Result>) response.body().getResults().getResult();

                resultHotelAdapter.updateList(resultListHotel);
                resultHotelAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<SingleResult<Result>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "This : " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
