package app.ayo.golf;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import app.ayo.golf.helper.SessionManager;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.pojo.APIResponse;
import app.ayo.golf.retrofit.pojo.Golfer;
import app.ayo.golf.retrofit.service.ApiServices;
import app.ayo.golf.retrofit.service.InitRetrofit;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignupActivity extends AppCompatActivity {
    @BindView(R.id.txtname)
    TextInputEditText txtname;
    @BindView(R.id.txtemail)
    TextInputEditText txtemail;
    @BindView(R.id.txtpassword)
    TextInputEditText txtpassword;
    @BindView(R.id.txtrepeatpass)
    TextInputEditText txtrepeatpass;
    @BindView(R.id.btnsignup)
    Button btnsignup;
    @BindView(R.id.txtlogin)
    TextView txtlogin;
    @BindView(R.id.tilname)
    TextInputLayout tilname;
    @BindView(R.id.tilemail)
    TextInputLayout tilemail;
    @BindView(R.id.tilpassword)
    TextInputLayout tilpassword;
    @BindView(R.id.tilrepeatpass)
    TextInputLayout tilrepeatpass;
    @BindView(R.id.tilphonenumber)
    TextInputLayout tilPhoneNumber;
    @BindView(R.id.txtphonenumber)
    TextInputEditText txtPhoneNumber;
    //================================== Constant ============================================//
    private static final String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    //========================================================================================//
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        //init actionbar
        Util.setCustomActionBar(this)
                .setTitle("Sign Up")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                })
                .setRightButton(null, null);
    }

    @OnClick({R.id.btnsignup, R.id.txtlogin})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnsignup:
                String nama = txtname.getText().toString().trim();
                String email = txtemail.getText().toString().trim();
                String phoneNumber = txtPhoneNumber.getText().toString().trim();
                String repass = txtrepeatpass.getText().toString().trim();
                String pass = txtpassword.getText().toString().trim();
                String channel = "email";
                /*if (txtname.getText().toString().trim().length() == 0) {
                    tilname.setError("Name is required!");
                } else*/
                if (txtemail.getText().toString().trim().length() == 0 || !email.matches(EMAIL_STRING)) {
                    tilemail.setError("Email is required!");
                } else if (txtPhoneNumber.getText().toString().trim().length() == 0 || phoneNumber.equals("")) {
                    tilPhoneNumber.setError("Phone Number is required!");
                } else if (txtpassword.getText().toString().trim().length() == 0) {
                    tilpassword.setError("Password is required!");
                } else if (!pass.equals(repass)) {
//                    Toast.makeText(this, "Password didn't match", Toast.LENGTH_SHORT).show();
                    tilrepeatpass.setError("Password didn't match");
                } else {
                    //Intent intent = new Intent(SignupActivity.this,LoginActivity.class);

                    ApiServices api = InitRetrofit.getInstanceRetrofit();
                    Call<APIResponse<Golfer>> call = api.signup(nama, email, pass, null, channel);
                    call.enqueue(new Callback<APIResponse<Golfer>>() {
                        @Override
                        public void onResponse(Call<APIResponse<Golfer>> call, Response<APIResponse<Golfer>> response) {
                            if (response.body() != null) {
                                if (response.isSuccessful() && response.body().isSuccessful()) {

                                    Golfer golfer = response.body().data;
                                    SessionManager.saveProfile(SignupActivity.this, golfer);
                                    setResult(RESULT_OK);
                                    finish();
                                    Intent toLogin = new Intent(SignupActivity.this, LoginActivity.class);
                                    startActivity(toLogin);
                                    finish();
                                    Toast.makeText(SignupActivity.this, "Register successful", Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    Toast.makeText(SignupActivity.this, response.body().error_message, Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<APIResponse<Golfer>> call, Throwable t) {
                            Toast.makeText(SignupActivity.this, "Register successful", Toast.LENGTH_SHORT).show();
                            Intent toLogin = new Intent(SignupActivity.this, LoginActivity.class);
                            startActivity(toLogin);
                            finish();
                        }
                    });
                }
                break;
            case R.id.txtlogin:
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                break;
        }
    }

    public void onClose(View view) {
        finish();
    }
}
