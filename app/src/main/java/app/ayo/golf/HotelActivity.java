package app.ayo.golf;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.xml.datatype.Duration;

import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.pojo.Hotel;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HotelActivity extends AppCompatActivity implements NumberPicker.OnValueChangeListener {

    Calendar calendar1, calendar2;
    DatePickerDialog datePickerDialog;
    int Year, Month, Day, Day1, Day2;
    public TextView mHotelTujuan, mTglCheckIn, mTglCheckOut, mTamuHotel, mKamarHotel, mDurasiHotel;
    private LinearLayout linearHotelTujuan;
    NumberPicker nTamu, nKamar;
    String date1, date2;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_hotel);
        ButterKnife.bind(this);

        Util.setCustomActionBar(this)
                .setTitle("Hotel")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                })
                .setRightButton(null, null);

        mHotelTujuan = (TextView) findViewById(R.id.tv_cari_hotel);
        linearHotelTujuan = (LinearLayout) findViewById(R.id.ll_hotel_tujuan);
        mTglCheckIn = (TextView) findViewById(R.id.tv_tglcheckin);
        mTglCheckOut = (TextView) findViewById(R.id.tv_tglcheckout);
        mTamuHotel = (TextView) findViewById(R.id.tv_jmlTamuHotel);
        mKamarHotel = (TextView) findViewById(R.id.tv_kamarHotel);
        // mDurasiHotel = (TextView) findViewById(R.id.tv_durasiHotel);

        String titleresult = getIntent().getStringExtra("Location");
        mHotelTujuan.setText(titleresult);


//    public static long calendarDaysBetween(Calendar startCal, Calendar endCal) {
//
//        // Create copies so we don't update the original calendars.
//
//        Calendar start = Calendar.getInstance();
//        start.setTimeZone(startCal.getTimeZone());
//        start.setTimeInMillis(startCal.getTimeInMillis());
//
//        Calendar end = Calendar.getInstance();
//        end.setTimeZone(endCal.getTimeZone());
//        end.setTimeInMillis(endCal.getTimeInMillis());
//
//        // Set the copies to be at midnight, but keep the day information.
//
//        start.set(Calendar.HOUR_OF_DAY, 0);
//        start.set(Calendar.MINUTE, 0);
//        start.set(Calendar.SECOND, 0);
//        start.set(Calendar.MILLISECOND, 0);
//
//        end.set(Calendar.HOUR_OF_DAY, 0);
//        end.set(Calendar.MINUTE, 0);
//        end.set(Calendar.SECOND, 0);
//        end.set(Calendar.MILLISECOND, 0);
//
//        // At this point, each calendar is set to midnight on
//        // their respective days. Now use TimeUnit.MILLISECONDS to
//        // compute the number of full days between the two of them.
//
//        return TimeUnit.MILLISECONDS.toDays(
//                Math.abs(end.getTimeInMillis() - start.getTimeInMillis()));
//    }

        //----------------------------------------------------------------------------------

//        long millis1 = calendar1.getTimeInMillis();
//        long millis2 = calendar2.getTimeInMillis();
//
//        // Calculate difference in milliseconds
//        long diff = millis2 - millis1;
//
//        // Calculate difference in seconds
//        long diffSeconds = diff / 1000;
//
//        // Calculate difference in minutes
//        long diffMinutes = diff / (60 * 1000);
//
//        // Calculate difference in hours
//        long diffHours = diff / (60 * 60 * 1000);
//
//        // Calculate difference in days
//        int diffDays = (int) (millis2 - millis1 / (24 * 60 * 60 * 1000));
//        mDurasiHotel.setText(getNight());

        //----------------------------------------------------------------------------------

        //mDurasiHotel.setText(((calendar2.getTimeInMillis()-calendar1.getTimeInMillis())/(24 * 60 * 60 * 1000)));
//        mDurasiHotel.setText((int) (calendar2.getTimeInMillis()-calendar1.getTimeInMillis()/(24*60*60*1000)));

//        Date d2 = new Date();
//        Date d1 = new Date(1384831803875l);
//
//        long diff = d2.getTime() - d1.getTime();
//        long diffSeconds = diff / 1000 % 60;
//        long diffMinutes = diff / (60 * 1000) % 60;
//        long diffHours = diff / (60 * 60 * 1000);
//        int diffInDays = (int) diff / (1000 * 60 * 60 * 24);

        //long diff = Math.abs()

    }

    public void hotelTujuan(View view) {
        Intent intent = new Intent(HotelActivity.this, HotelLocation.class);
        startActivity(intent);
        finish();
    }

    public void hotelCheckIn(View view) {

        // Get Current Date
        calendar1 = Calendar.getInstance();
        Year = calendar1.get(Calendar.YEAR);
        Month = calendar1.get(Calendar.MONTH);
        Day1 = calendar1.get(Calendar.DAY_OF_MONTH);
        Day = calendar1.get(Calendar.DAY_OF_WEEK);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        date1 = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                        mTglCheckIn.setText(date1);
                    }
                }, Year, Month, Day);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    public void tanggalCheckout(View view) {

        // Get Current Date
        calendar2 = Calendar.getInstance();
        Year = calendar2.get(Calendar.YEAR);
        Month = calendar2.get(Calendar.MONTH);
        Day2 = calendar2.get(Calendar.DAY_OF_MONTH);
        Day1 = calendar2.get(Calendar.DAY_OF_WEEK);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        date2 = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                        mTglCheckOut.setText(date2);
                    }
                }, Year, Month, Day);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    public void calculateDate() {
        long diff = calendar2.getTimeInMillis() - calendar1.getTimeInMillis();
        float dayCount = (float) diff / (24 * 60 * 60 * 1000);
    }


    public void tamuHotel(View view) {

        final Dialog tamuDialog = new Dialog(HotelActivity.this);
        tamuDialog.setTitle("Jumlah Tamu");
        tamuDialog.setContentView(R.layout.number_picker_tamu);
        Button nTamu = (Button) tamuDialog.findViewById(R.id.btn_ntamu);
        NumberPicker numberPicker = (NumberPicker) tamuDialog.findViewById(R.id.numberPickerTamu);
        //EditText numberPickerChild = (EditText) numberPicker.getChildAt(0);
        final NumberPicker npTamu = (NumberPicker) tamuDialog.findViewById(R.id.numberPickerTamu);
        npTamu.setMaxValue(20);
        npTamu.setMinValue(1);
        npTamu.setWrapSelectorWheel(false);
        npTamu.setOnValueChangedListener(this);
        //npTamu.setFocusable(true);
        npTamu.setDescendantFocusability(numberPicker.FOCUS_BLOCK_DESCENDANTS);
        //tamuDialog.getWindow().setLayout(500, 900);
        //numberPickerChild.setInputType(InputType.TYPE_NULL);

        nTamu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTamuHotel.setText(String.valueOf(npTamu.getValue()) + " Tamu"); //set Value to TextView
                tamuDialog.dismiss();
            }
        });
        tamuDialog.show();
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        Log.i("value is", "" + newVal);
    }

    public void filterHarga(View view) {

        final Dialog tamuDialog = new Dialog(HotelActivity.this);
        tamuDialog.setTitle("Jumlah Kamar");
        tamuDialog.setContentView(R.layout.number_picker_tamu);
        Button nTamu = (Button) tamuDialog.findViewById(R.id.btn_ntamu);
        NumberPicker numberPicker = (NumberPicker) tamuDialog.findViewById(R.id.numberPickerTamu);
        //EditText numberPickerChild = (EditText) numberPicker.getChildAt(0);
        final NumberPicker npTamu = (NumberPicker) tamuDialog.findViewById(R.id.numberPickerTamu);
        npTamu.setMaxValue(20);
        npTamu.setMinValue(1);
        npTamu.setWrapSelectorWheel(false);
        npTamu.setOnValueChangedListener(this);
        //npTamu.setFocusable(true);
        npTamu.setDescendantFocusability(numberPicker.FOCUS_BLOCK_DESCENDANTS);
        //tamuDialog.getWindow().setLayout(500, 900);
        //numberPickerChild.setInputType(InputType.TYPE_NULL);

        nTamu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mKamarHotel.setText(String.valueOf(npTamu.getValue()) + " Kamar"); //set Value to TextView
                tamuDialog.dismiss();
            }
        });
        tamuDialog.show();
    }

    public void searchHotel(View view) {
        Intent intentResultHotel = new Intent(this, HotelSearch.class);
        intentResultHotel.putExtra("HotelTujuan", mHotelTujuan.getText().toString());
        intentResultHotel.putExtra("tanggalCheckin", mTglCheckIn.getText().toString());
        intentResultHotel.putExtra("tanggalCheckout", mTglCheckOut.getText().toString());
        intentResultHotel.putExtra("tamuHotel", mTamuHotel.getText().toString());
        intentResultHotel.putExtra("kamarHotel", mKamarHotel.getText().toString());
        view.getContext().startActivity(intentResultHotel);
        //Toast.makeText(HotelActivity.this, "Under Construction", Toast.LENGTH_SHORT).show();
    }
}
