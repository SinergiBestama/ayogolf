package app.ayo.golf;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import app.ayo.golf.helper.PermissionUtil;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.pojo.BookingCourseResult;
import app.ayo.golf.retrofit.pojo.DataBookingDetail;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Activity_Payment2 extends AppCompatActivity {

    @BindView(R.id.txttotal2)
    TextView txttotal2;
    private TextView mConfirmItalic2;

    private Button btnUploadPic2;
    private String picLoad2;
    private static final int UPLOAD_PICT = 222;
    private static final int REQUEST_PERMISSION = 252;
    BookingCourseResult bookingResult;
    DataBookingDetail dataBookingDetail;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment2);
        ButterKnife.bind(this);
        Util.setCustomActionBar(this)
                .setTitle("Payment")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                })
                .setRightButton(null, null);

        mConfirmItalic2 = (TextView) findViewById(R.id.tv_confirmItalic2);
        btnUploadPic2 = (Button) findViewById(R.id.btn_upload_file2);
        mConfirmItalic2.setTypeface(null, Typeface.ITALIC);

        // get extras
        dataBookingDetail = getIntent().getParcelableExtra("booking_result2");

        // set total price
        setTotalPrice2();

        btnUploadPic2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentUploadFile2 = new Intent(Activity_Payment2.this, UploadFileTransact.class);
                startActivity(intentUploadFile2);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION) {
            if (PermissionUtil.hashPermission(this, permissions)) {
                picLoad();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    private void picLoad() {
        //Do Action
    }

    void setTotalPrice2() {
        long totalPrice2 = Long.parseLong(dataBookingDetail.getTotal());
//        NumberFormat format = NumberFormat.getCurrencyInstance();
//        format.setCurrency(Currency.getInstance("IDR"));
//        format.setMinimumFractionDigits(0);
        txttotal2.setText(Util.formatToCurrency(totalPrice2));
    }

    @OnClick({R.id.btnconfirm2})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnconfirm2:
                Intent intent = new Intent(this, BookActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
