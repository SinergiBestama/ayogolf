package app.ayo.golf.retrofit.service;

import app.ayo.golf.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by arifina on 7/25/2017.
 */

public class InitRetrofit {
    private static boolean ENABLE_LOGGING = BuildConfig.DEBUG;

    /*Gson gson = new GsonBuilder()
            .setLenient()
            .create();*/

    //init library Retrofit di project
    public static Retrofit getInitRetrofit(){
        return getInitRetrofit(true);
    }

    //api service membutuhkan init retrofit juga, sehingga hrs dipanggil classnya
    //supaya bisa dipanggil di class lain, tambahkan static
    public static ApiServices getInstanceRetrofit(){
        return getInitRetrofit().create(ApiServices.class);
    }

    public static ApiServices getInstanceRetrofit(boolean forceDebug){
        return getInitRetrofit(forceDebug).create(ApiServices.class);
    }

    public static Retrofit getInitRetrofit(boolean forceDebug){
        return  new Retrofit.Builder()
                .baseUrl("http://117.54.3.24:11080/ayogolf/index.php/api/")
                .client(generateClient(forceDebug))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private static OkHttpClient generateClient(boolean forceDebug){
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();

        if(ENABLE_LOGGING && forceDebug) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            clientBuilder.addInterceptor(loggingInterceptor);
        }
        return clientBuilder.build();
    }
}
