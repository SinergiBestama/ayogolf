package app.ayo.golf.retrofit.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by admin on 24/04/2018.
 */

public class BookingCourseDetails implements Parcelable {

    public String booking_code;
    public String golfer_id;
    public String course_name;
    public String booked_date;
    public String caddy;
    public int flightCount;
    public String player;
    public String total;
    public ArrayList<DetailFlight> detailFlight;

    public BookingCourseDetails(){}


    protected BookingCourseDetails(Parcel in) {
        booking_code = in.readString();
        golfer_id = in.readString();
        course_name = in.readString();
        booked_date = in.readString();
        caddy = in.readString();
        flightCount = in.readInt();
        player = in.readString();
        total = in.readString();
        if (in.readByte() == 0x01) {
            detailFlight = new ArrayList<>();
            in.readList(detailFlight, DetailFlight.class.getClassLoader());
        } else {
            detailFlight = null;
        }
    }

    public static final Creator<BookingCourseDetails> CREATOR = new Creator<BookingCourseDetails>() {
        @Override
        public BookingCourseDetails createFromParcel(Parcel in) {
            return new BookingCourseDetails(in);
        }

        @Override
        public BookingCourseDetails[] newArray(int size) {
            return new BookingCourseDetails[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(booking_code);
        dest.writeString(golfer_id);
        dest.writeString(course_name);
        dest.writeString(booked_date);
        dest.writeString(caddy);
        dest.writeInt(flightCount);
        dest.writeString(player);
        dest.writeString(total);
        if (detailFlight == null) {
            dest.writeByte((byte)(0x00));
        } else {
            dest.writeByte((byte)(0x01));
            dest.writeList(detailFlight);
        }
    }

    //-------- INNER CLASS --------//
    public static class DetailFlight implements Parcelable {

        public String flight_id;
        public String trx_id;
        public String flight_booked;
        public String time_type;
        public String player;
        public String price;
        public String total;
        public String created_date;
        public String flightnumber;

        public DetailFlight() {
        }

        protected DetailFlight(Parcel in) {
            flight_id = in.readString();
            trx_id = in.readString();
            flight_booked = in.readString();
            time_type = in.readString();
            player = in.readString();
            price = in.readString();
            total = in.readString();
            created_date = in.readString();
            flightnumber = in.readString();
        }

        public static final Creator<DetailFlight> CREATOR = new Creator<DetailFlight>() {
            @Override
            public DetailFlight createFromParcel(Parcel in) {
                return new DetailFlight(in);
            }

            @Override
            public DetailFlight[] newArray(int size) {
                return new DetailFlight[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(flight_id);
            dest.writeString(trx_id);
            dest.writeString(flight_booked);
            dest.writeString(time_type);
            dest.writeString(player);
            dest.writeString(price);
            dest.writeString(total);
            dest.writeString(created_date);
            dest.writeString(flightnumber);


        }
    }
}
