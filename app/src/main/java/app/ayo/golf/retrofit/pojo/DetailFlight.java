package app.ayo.golf.retrofit.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 24/04/2018.
 */

public class DetailFlight implements Parcelable {

    @SerializedName("flight_id")
    @Expose
    private String flightId;
    @SerializedName("trx_id")
    @Expose
    private String trxId;
    @SerializedName("flight_booked")
    @Expose
    private String flightBooked;
    @SerializedName("tee_time")
    @Expose
    private Object teeTime;
    @SerializedName("time_type")
    @Expose
    private String timeType;
    @SerializedName("player")
    @Expose
    private String player;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("flightnumber")
    @Expose
    private String flightnumber;

    public DetailFlight(String flightId, String trxId, String flightBooked, Object teeTime, String timeType, String player, String price, String total, String createdDate, String flightnumber) {
        this.flightId = flightId;
        this.trxId = trxId;
        this.flightBooked = flightBooked;
        this.teeTime = teeTime;
        this.timeType = timeType;
        this.player = player;
        this.price = price;
        this.total = total;
        this.createdDate = createdDate;
        this.flightnumber = flightnumber;
    }

    public String getFlightId() {
        return flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getFlightBooked() {
        return flightBooked;
    }

    public void setFlightBooked(String flightBooked) {
        this.flightBooked = flightBooked;
    }

    public Object getTeeTime() {
        return teeTime;
    }

    public void setTeeTime(Object teeTime) {
        this.teeTime = teeTime;
    }

    public String getTimeType() {
        return timeType;
    }

    public void setTimeType(String timeType) {
        this.timeType = timeType;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getFlightnumber() {
        return flightnumber;
    }

    public void setFlightnumber(String flightnumber) {
        this.flightnumber = flightnumber;
    }

    @Override
    public String toString() {
        return "DetailFlight{" +
                "flightId='" + flightId + '\'' +
                ", trxId='" + trxId + '\'' +
                ", flightBooked='" + flightBooked + '\'' +
                ", teeTime=" + teeTime +
                ", timeType='" + timeType + '\'' +
                ", player='" + player + '\'' +
                ", price='" + price + '\'' +
                ", total='" + total + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", flightnumber='" + flightnumber + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.flightId);
        dest.writeString(this.trxId);
        dest.writeString(this.flightBooked);
        dest.writeParcelable((Parcelable) this.teeTime, flags);
        dest.writeString(this.timeType);
        dest.writeString(this.player);
        dest.writeString(this.price);
        dest.writeString(this.total);
        dest.writeString(this.createdDate);
        dest.writeString(this.flightnumber);
    }

    public DetailFlight(Parcel in) {
        this.flightId = in.readString();
        this.trxId = in.readString();
        this.flightBooked = in.readString();
        this.teeTime = in.readParcelable(Object.class.getClassLoader());
        this.timeType = in.readString();
        this.player = in.readString();
        this.price = in.readString();
        this.total = in.readString();
        this.createdDate = in.readString();
        this.flightnumber = in.readString();
    }

    public static final Creator<DetailFlight> CREATOR = new Creator<DetailFlight>() {
        @Override
        public DetailFlight createFromParcel(Parcel source) {
            return new DetailFlight(source);
        }

        @Override
        public DetailFlight[] newArray(int size) {
            return new DetailFlight[size];
        }
    };
}
