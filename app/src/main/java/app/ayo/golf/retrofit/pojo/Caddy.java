package app.ayo.golf.retrofit.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by arifina on 7/26/2017.
 */

public class Caddy implements Parcelable {
    public String caddy_id;
    public String caddy_name;
    public String gender;
    public String caddy_course;
    public String caddy_pic;

    protected Caddy(Parcel in) {
        caddy_id = in.readString();
        caddy_name = in.readString();
        gender = in.readString();
        caddy_course = in.readString();
        caddy_pic = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(caddy_id);
        dest.writeString(caddy_name);
        dest.writeString(gender);
        dest.writeString(caddy_course);
        dest.writeString(caddy_pic);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Caddy> CREATOR = new Parcelable.Creator<Caddy>() {
        @Override
        public Caddy createFromParcel(Parcel in) {
            return new Caddy(in);
        }

        @Override
        public Caddy[] newArray(int size) {
            return new Caddy[size];
        }
    };
}
