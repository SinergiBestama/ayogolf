package app.ayo.golf.retrofit.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by arifina on 10/17/2017.
 */

public class Coach implements Parcelable {
    public String coach_name;
    public String coach_title;
    public String coach_image;
    public String coach_desc;

    protected Coach(Parcel in) {
        coach_name = in.readString();
        coach_title = in.readString();
        coach_image = in.readString();
        coach_desc = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(coach_name);
        dest.writeString(coach_title);
        dest.writeString(coach_image);
        dest.writeString(coach_desc);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Coach> CREATOR = new Parcelable.Creator<Coach>() {
        @Override
        public Coach createFromParcel(Parcel in) {
            return new Coach(in);
        }

        @Override
        public Coach[] newArray(int size) {
            return new Coach[size];
        }
    };
}