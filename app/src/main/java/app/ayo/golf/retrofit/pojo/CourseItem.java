package app.ayo.golf.retrofit.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Hendi on 9/26/17.
 */
public class CourseItem implements Parcelable {
    public String icon;
    public String course_id;
    public String course_name;
    public String course_image;
    public String course_address;
    public String course_city;
    public double lat;
    public double lng;
    public String holes;
    public String par;
    public String length;
    public String open_time;
    public String close_time;
    public String est_in;
    public String designer;
    public ArrayList<Gallery> galleries;
    public ArrayList<Amenities> amenities;

    public static class Amenities implements Parcelable {
        public String name;
        public String facility_image;

        protected Amenities(Parcel in) {
            name = in.readString();
            facility_image = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(name);
            dest.writeString(facility_image);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Amenities> CREATOR = new Parcelable.Creator<Amenities>() {
            @Override
            public Amenities createFromParcel(Parcel in) {
                return new Amenities(in);
            }

            @Override
            public Amenities[] newArray(int size) {
                return new Amenities[size];
            }
        };
    }

    public static class Gallery implements Parcelable {
        public String galery_id;
        public String course_id;
        public String course_image;
        public String created_date;

        protected Gallery(Parcel in) {
            galery_id = in.readString();
            course_id = in.readString();
            course_image = in.readString();
            created_date = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(galery_id);
            dest.writeString(course_id);
            dest.writeString(course_image);
            dest.writeString(created_date);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Gallery> CREATOR = new Parcelable.Creator<Gallery>() {
            @Override
            public Gallery createFromParcel(Parcel in) {
                return new Gallery(in);
            }

            @Override
            public Gallery[] newArray(int size) {
                return new Gallery[size];
            }
        };
    }

    public CourseItem(ArrayList<CourseItem> data) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.icon);
        dest.writeString(this.course_id);
        dest.writeString(this.course_name);
        dest.writeString(this.course_image);
        dest.writeString(this.course_address);
        dest.writeString(this.course_city);
        dest.writeDouble(this.lat);
        dest.writeDouble(this.lng);
        dest.writeString(this.holes);
        dest.writeString(this.par);
        dest.writeString(this.length);
        dest.writeString(this.open_time);
        dest.writeString(this.close_time);
        dest.writeString(this.est_in);
        dest.writeString(this.designer);
        dest.writeTypedList(this.galleries);
        dest.writeTypedList(this.amenities);
    }

    public CourseItem(Parcel in) {
        this.icon = in.readString();
        this.course_id = in.readString();
        this.course_name = in.readString();
        this.course_image = in.readString();
        this.course_address = in.readString();
        this.course_city = in.readString();
        this.lat = in.readDouble();
        this.lng = in.readDouble();
        this.holes = in.readString();
        this.par = in.readString();
        this.length = in.readString();
        this.open_time = in.readString();
        this.close_time = in.readString();
        this.est_in = in.readString();
        this.designer = in.readString();
        this.galleries = in.createTypedArrayList(Gallery.CREATOR);
        this.amenities = in.createTypedArrayList(Amenities.CREATOR);
    }

    public static final Creator<CourseItem> CREATOR = new Creator<CourseItem>() {
        @Override
        public CourseItem createFromParcel(Parcel source) {
            return new CourseItem(source);
        }

        @Override
        public CourseItem[] newArray(int size) {
            return new CourseItem[size];
        }
    };
}
