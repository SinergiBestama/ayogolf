package app.ayo.golf.retrofit.service;

import app.ayo.golf.entityweather.weather1.Example;
import app.ayo.golf.entityweather.weather2.Example2;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by admin on 07/06/2018.
 */
//https://api.openweathermap.org/data/2.5/weather?lat=-6.2228491&lon=106.7942765&appid=2fc56d498a3b4d87ba298c232e65e6b0
public interface ApiServicesWeather {
    @GET("weather?lat=-6.2228491&lon=106.7942765")
    Call<Example> getAllWeather(@Query("lat") Double lat,
                                @Query("lon") Double lon,
                                @Query("appid") String appid);


    //http://api.openweathermap.org/data/2.5/forecast?q=Jakarta&APPID=2fc56d498a3b4d87ba298c232e65e6b0&units=metric
    @GET("forecast?")
    Call<Example2> getDetailWeather(@Query("q") String q,
                                    @Query("appid") String appid,
                                    @Query("units") String units);
}
