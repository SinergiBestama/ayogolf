package app.ayo.golf.retrofit.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Hendi on 10/4/17.
 */

public class TeeTime implements Parcelable {
    public String tee_time;
    public String time_type;
    public long price;
    public long apps_price;
    public String day;
    public String price_id;

    public TeeTime(Parcel in) {
        tee_time = in.readString();
        time_type = in.readString();
        price = in.readLong();
        apps_price = in.readLong();
        day = in.readString();
        price_id = in.readString();
    }

    public TeeTime(String tee_time, String time_type, long price, long apps_price, String day, String price_id) {
        this.tee_time = tee_time;
        this.time_type = time_type;
        this.price = price;
        this.apps_price = apps_price;
        this.day = day;
        this.price_id = price_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(tee_time);
        dest.writeString(time_type);
        dest.writeLong(price);
        dest.writeLong(apps_price);
        dest.writeString(day);
        dest.writeString(price_id);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<TeeTime> CREATOR = new Parcelable.Creator<TeeTime>() {
        @Override
        public TeeTime createFromParcel(Parcel in) {
            return new TeeTime(in);
        }

        @Override
        public TeeTime[] newArray(int size) {
            return new TeeTime[size];
        }
    };
}
