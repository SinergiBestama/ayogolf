package app.ayo.golf.retrofit.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by arifina on 10/17/2017.
 */

public class Tutorial implements Parcelable {
    public String id;
    public String title;
    public String source;
    public String video;
    public String source_url;

    protected Tutorial(Parcel in) {
        id = in.readString();
        title = in.readString();
        source = in.readString();
        video = in.readString();
        source_url = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(source);
        dest.writeString(video);
        dest.writeString(source_url);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Tutorial> CREATOR = new Parcelable.Creator<Tutorial>() {
        @Override
        public Tutorial createFromParcel(Parcel in) {
            return new Tutorial(in);
        }

        @Override
        public Tutorial[] newArray(int size) {
            return new Tutorial[size];
        }
    };
}
