package app.ayo.golf.retrofit.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Haries on 15/11/17.
 */

public class MarketItem implements Parcelable {
    public String item_id;
    public String item_title;
    public int item_pic;
    public long item_price;

    public MarketItem(String item_id, String item_title, int item_pic, long item_price) {
        this.item_id = item_id;
        this.item_title = item_title;
        this.item_pic = item_pic;
        this.item_price = item_price;
    }

    protected MarketItem(Parcel in) {
        item_id = in.readString();
        item_title = in.readString();
        item_pic = in.readInt();
        item_price = in.readLong();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(item_id);
        dest.writeString(item_title);
        dest.writeInt(item_pic);
        dest.writeLong(item_price);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Caddy> CREATOR = new Parcelable.Creator<Caddy>() {
        @Override
        public Caddy createFromParcel(Parcel in) {
            return new Caddy(in);
        }

        @Override
        public Caddy[] newArray(int size) {
            return new Caddy[size];
        }
    };
}
