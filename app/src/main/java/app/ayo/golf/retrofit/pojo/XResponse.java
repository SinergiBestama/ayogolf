package app.ayo.golf.retrofit.pojo;

import com.google.gson.annotations.SerializedName;

import android.text.TextUtils;

/**
 * Created by Hendi on 9/26/17.
 */

public abstract class XResponse {
    @SerializedName("code")
    public String code;
    public String error_message;

    public boolean isSuccessful(){
        boolean isSuccess = false;
        if(!TextUtils.isEmpty(code)) {
            isSuccess = Integer.valueOf(code) >= 200 &&
                        Integer.valueOf(code) < 300;
        }
        return isSuccess;
    }
}
