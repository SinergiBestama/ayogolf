package app.ayo.golf.retrofit.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Hendi on 10/1/17.
 */

public class News implements Parcelable {
    public String news_id;
    public String news_title;
    public String news_content;
    public String publish_date;
    public String created_date;
    public String news_image;
    public String source_url;

    //public long news_time;

    protected News(Parcel in) {
        news_id = in.readString();
        news_title = in.readString();
        news_content = in.readString();
        news_image = in.readString();
        source_url = in.readString();
        created_date = in.readString();
        publish_date = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(news_id);
        dest.writeString(news_title);
        dest.writeString(news_content);
        dest.writeString(news_image);
        dest.writeString(source_url);
        dest.writeString(created_date);
        dest.writeString(publish_date);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<News> CREATOR = new Parcelable.Creator<News>() {
        @Override
        public News createFromParcel(Parcel in) {
            return new News(in);
        }

        @Override
        public News[] newArray(int size) {
            return new News[size];
        }
    };
}
