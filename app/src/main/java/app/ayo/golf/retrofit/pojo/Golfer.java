package app.ayo.golf.retrofit.pojo;

/**
 * Created by arifina on 7/27/2017.
 */

public class Golfer {
    public String golfer_id;
    public String app_id;
    public String golfer_name;
    public String golfer_phone;
    public String created_date;
    public String birth_date;
    public String gender;
    public String email;
    public String password;
    public String flag;
    public String golfer_pic;
    public String token;
    public String channel;
    public String handicap_index;

    public String getGolfer_name(){
        return golfer_name==null?"":golfer_name;
    }

    public String getChannel(){
        return channel==null?"":channel;
    }

    public String getEmail(){
        return email==null?"":email;
    }

    public String getPassword() {
        return password==null?"":password;
    }

    public String getGolfer_phone() {
        return golfer_phone==null?"":golfer_phone;
    }

    public String getCreated_date() {
        return created_date==null?"":created_date;
    }

    public String getHandicap_index() {
        return handicap_index==null?"":handicap_index;
    }
}
