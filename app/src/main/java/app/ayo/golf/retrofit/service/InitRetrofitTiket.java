package app.ayo.golf.retrofit.service;

import java.util.concurrent.TimeUnit;

import app.ayo.golf.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by admin on 18/02/2018.
 */

public class InitRetrofitTiket {

    /**
     * Service API for tiket.com
     */

    private static boolean ENABLE_LOGGING = BuildConfig.DEBUG;
    private static String BASE_URL = "http://api-sandbox.tiket.com/";

    public static <T> T getServiceTiket(Class<T> service){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL+service.getSimpleName() + "/")
                .client(generateClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(service);
    }

    private static OkHttpClient generateClient(){
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        clientBuilder.connectTimeout(60*1000, TimeUnit.MILLISECONDS)
                .writeTimeout(60*1000, TimeUnit.MILLISECONDS)
                .readTimeout(60*1000, TimeUnit.MILLISECONDS);

        if(ENABLE_LOGGING) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            clientBuilder.addInterceptor(loggingInterceptor);
        }
        return clientBuilder.build();
    }
}

//    public static final String BASE_URL_TIKET = "http://117.54.4.124:12180/mvicall/index.php/api/";
//    //public static final String BASE_URL_TIKET = "https://api-sandbox.tiket.com/";
//
//    public static Retrofit getTiketClient() {
//        return new Retrofit.Builder()
//                .baseUrl(BASE_URL_TIKET)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//    }
//
//    public static ApiServicesTiket getApiServicesTiket() {
//        return getTiketClient().create(ApiServicesTiket.class);
//    }

