package app.ayo.golf.retrofit.pojo;

/**
 * Created by arifina on 7/25/2017.
 */

public class Course {
    String course_id;
    String course_name;
    String address;
    String city;
    String course_image;
    String lng;
    String lat;

    public String getCourse_id() {
        return course_id;
    }

    public String getCourse_name() {
        return course_name;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getCourse_image() {
        return course_image;
    }

    public String getLng() {
        return lng;
    }

    public String getLat() {
        return lat;
    }
}
