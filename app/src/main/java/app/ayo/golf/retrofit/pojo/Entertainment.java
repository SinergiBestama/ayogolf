package app.ayo.golf.retrofit.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by arifina on 10/18/2017.
 */

public class Entertainment implements Parcelable {
    @SerializedName("banner_image")
    public String banner_img;
    public String banner_url;
    public String publish_date;
    public String expired_date;

    protected Entertainment(Parcel in) {
        banner_img = in.readString();
        banner_url = in.readString();
        publish_date = in.readString();
        expired_date = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(banner_img);
        dest.writeString(banner_url);
        dest.writeString(publish_date);
        dest.writeString(expired_date);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Entertainment> CREATOR = new Parcelable.Creator<Entertainment>() {
        @Override
        public Entertainment createFromParcel(Parcel in) {
            return new Entertainment(in);
        }

        @Override
        public Entertainment[] newArray(int size) {
            return new Entertainment[size];
        }
    };
}