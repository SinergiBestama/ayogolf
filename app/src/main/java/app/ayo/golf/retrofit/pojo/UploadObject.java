package app.ayo.golf.retrofit.pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 19/02/2018.
 */

public class UploadObject {

    /*
    private String success;

    public UploadObject(String success){
        this.success = success;
    }
    public String getSuccess(){
        return success;
    } */

    @Override
    public String toString() {
        return "UploadObject{" +
                "success=" + success +
                ", message='" + message + '\'' +
                '}';
    }

    @SerializedName("success")
    boolean success;
    @SerializedName("message")
    String message;

    public String getMessage() {
        return message;
    }

    public boolean isSuccess() {
        return success;
    }
}
