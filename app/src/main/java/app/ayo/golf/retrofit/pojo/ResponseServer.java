package app.ayo.golf.retrofit.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by admin on 7/25/2017.
 */

public class ResponseServer {
    ArrayList<Course> course = new ArrayList<>();

    ArrayList<Caddy> caddy = new ArrayList<>();

    ArrayList<Golfer> golfer = new ArrayList<>();

    @SerializedName("coaching_clinic")
    ArrayList<CoachingClinic> coachingClinic = new ArrayList<>();

    ArrayList<CourseCity> course_city = new ArrayList<>();

    public ArrayList<Course> getCourse() {
        return course;
    }

    public ArrayList<Caddy> getCaddy() {
        return caddy;
    }

    public ArrayList<Golfer> getGolfer() {
        return golfer;
    }

    public ArrayList<CoachingClinic> getCoachingClinic() {
        return coachingClinic;
    }

    public ArrayList<CourseCity> getCourseCities() {
        return course_city;
    }
}