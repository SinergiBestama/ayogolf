package app.ayo.golf.retrofit.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Bam on 10/29/17.
 */

public class BookingCourseResult implements Parcelable {
    public String booking_code;
    public String golfer_id;
    public String course_name;
    public String booked_date;
    public int flightCount;
    public int player;
    public String total;
    public ArrayList<DetailFlight> detailFlight;
    public String status;
    public String status_message;
    public String payment_status;
    public String currentTime;
    public String created_date;
    public String time_type;
    public String caddy;
    public String trx_id;

    public BookingCourseResult() {
    }

    // INNER CLASS
    public static class DetailFlight implements Parcelable {
        public String flight_id;
        public String trx_id;
        public String flight_booked;
        public String time_type;
        public String player;
        public String price;
        public String total;
        public String created_date;
        public String flightnumber;

        public DetailFlight() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.flight_id);
            dest.writeString(this.trx_id);
            dest.writeString(this.flight_booked);
            dest.writeString(this.time_type);
            dest.writeString(this.player);
            dest.writeString(this.price);
            dest.writeString(this.total);
            dest.writeString(this.created_date);
            dest.writeString(this.flightnumber);
        }

        protected DetailFlight(Parcel in) {
            this.flight_id = in.readString();
            this.trx_id = in.readString();
            this.flight_booked = in.readString();
            this.time_type = in.readString();
            this.player = in.readString();
            this.price = in.readString();
            this.total = in.readString();
            this.created_date = in.readString();
            this.flightnumber = in.readString();
        }

        public static final Creator<DetailFlight> CREATOR = new Creator<DetailFlight>() {
            @Override
            public DetailFlight createFromParcel(Parcel source) {
                return new DetailFlight(source);
            }

            @Override
            public DetailFlight[] newArray(int size) {
                return new DetailFlight[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.booking_code);
        dest.writeString(this.golfer_id);
        dest.writeString(this.course_name);
        dest.writeString(this.booked_date);
        dest.writeInt(this.flightCount);
        dest.writeInt(this.player);
        dest.writeString(this.total);
        dest.writeTypedList(this.detailFlight);
        dest.writeString(this.status);
        dest.writeString(this.status_message);
        dest.writeString(this.payment_status);
        dest.writeString(this.currentTime);
        dest.writeString(this.created_date);
        dest.writeString(this.time_type);
        dest.writeString(this.caddy);
        dest.writeString(this.trx_id);
    }

    protected BookingCourseResult(Parcel in) {
        this.booking_code = in.readString();
        this.golfer_id = in.readString();
        this.course_name = in.readString();
        this.booked_date = in.readString();
        this.flightCount = in.readInt();
        this.player = in.readInt();
        this.total = in.readString();
        this.detailFlight = in.createTypedArrayList(DetailFlight.CREATOR);
        this.status = in.readString();
        this.status_message = in.readString();
        this.payment_status = in.readString();
        this.currentTime = in.readString();
        this.created_date = in.readString();
        this.time_type = in.readString();
        this.caddy = in.readString();
        this.trx_id = in.readString();
    }

    public static final Creator<BookingCourseResult> CREATOR = new Creator<BookingCourseResult>() {
        @Override
        public BookingCourseResult createFromParcel(Parcel source) {
            return new BookingCourseResult(source);
        }

        @Override
        public BookingCourseResult[] newArray(int size) {
            return new BookingCourseResult[size];
        }
    };
}
