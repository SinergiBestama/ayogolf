package app.ayo.golf.retrofit.service;

import java.util.List;

import app.ayo.golf.entitytiket.findhotel.HotelFind;
import app.ayo.golf.entitytiket.findhotel.SingleResult;
import app.ayo.golf.entitytiket.hotelresult.Result;
import app.ayo.golf.entitytiket.searchflight.ResultFlight;
import app.ayo.golf.entitytiket.searchflight.ResultSearchFlight;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by admin on 20/02/2018.
 */

public interface search {

    //https://api-sandbox.tiket.com/search/autocomplete/hotel?q=mah&token=90d2fad44172390b11527557e6250e50&output=json
    @GET("autocomplete/hotel")
    Call<SingleResult<HotelFind>> findHotel(@Query("token") String token, @Query("output") String output, @Query("q") String string);

    //https://api-sandbox.tiket.com/search/hotel?q=jakarta&startdate=2018-03-01&night=&enddate=2018-03-04&room=1&adult=1&child=0&token=71cf2d3dec294394e267fbb0bf28916f4198f8d6&output=json
    @GET("hotel")
    Call<SingleResult<Result>> resultHotel(
            @Query("q") String q,
            @Query("startdate") String startdate,
            @Query("night") String night,
            @Query("enddate") String enddate,
            @Query("room") String room,
            @Query("adult") String adult,
            @Query("child") String child,
            @Query("token") String token,
            @Query("output") String output);

    /**
     * http://api-sandbox.tiket.com/search/flight?d=CGK&a=DPS&date=2018-03-15&ret_date=2018-03-16&adult=1&child=0&infant=0&token=71cf2d3dec294394e267fbb0bf28916f4198f8d6&v=3&output=json
     */

    @GET("flight")
    Call<ResultFlight> resultFlight (
            @Query("d") String d,
            @Query("a") String a,
            @Query("date") String date,
            @Query("ret_date") String ret_date,
            @Query("adult") String adult,
            @Query("child") String child,
            @Query("infant") String infant,
            @Query("token") String token,
            @Query("v") String v,
            @Query("output") String output
    );
    //ini ada allflight mana ?
}
