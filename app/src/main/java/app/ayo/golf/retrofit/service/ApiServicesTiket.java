package app.ayo.golf.retrofit.service;

import java.util.List;

import app.ayo.golf.entitytiket.findhotel.Response;
import app.ayo.golf.entitytiket.findhotel.TicketCurrency;
import app.ayo.golf.entitytiket.hoteldetailent.AllPhoto;
import app.ayo.golf.retrofit.pojo.APIResponseTiket;
import app.ayo.golf.retrofit.pojo.Hotel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by admin on 18/02/2018.
 */

public interface ApiServicesTiket {

    @POST("videoMusic")
    Call<APIResponseTiket<List<Hotel>>> getVideoMusic();

    @GET("/flight_api/all_airport?")
    Call<APIResponseTiket<List<Hotel>>> getValue(@Query("token") String token);

    /**
     * http://api-sandbox.tiket.com/general_api/listCurrency?token=c992866a6ffb08e59a86fc6a050ca7c7bdec6c2f
     */
    @GET("listCurrency")
    Call<Response<TicketCurrency>> getListCurrency(@Query("token") String token, @Query("output") String output);

    /**
     * https://api-sandbox.tiket.com/alron-hotel?startdate=2018-03-11&enddate=2018-03-12&night=1&room=1&adult=2&child=0&uid=business%3A4108&token=71cf2d3dec294394e267fbb0bf28916f4198f8d6&output=json
     */
    @GET("alron-hotel")
    Call<AllPhoto> detailPhoto
    (@Query("startdate") String startdate,
     @Query("enddate") String enddate,
     @Query("night") String night,
     @Query("room") String room,
     @Query("adult") String adult,
     @Query("child") String child,
     @Query("token") String token,
     @Query("output") String output);
}
