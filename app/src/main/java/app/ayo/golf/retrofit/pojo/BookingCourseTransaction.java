package app.ayo.golf.retrofit.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Hendi on 10/2/17.
 */

public class BookingCourseTransaction implements Parcelable {
    public Caddy caddy;
    public long booking_date;
    public ArrayList<Flight> flights = new ArrayList<>();
    public CourseItem course;

    public BookingCourseTransaction() {

        // set initial flight
        Flight initialFlight = new Flight();
        initialFlight.flight_number = 1;
        initialFlight.player = 1;
        initialFlight.showing = true;
        flights.add(initialFlight);
    }

    protected BookingCourseTransaction(Parcel in) {
        caddy = (Caddy) in.readValue(Caddy.class.getClassLoader());
        booking_date = in.readLong();
        course = (CourseItem) in.readValue(CourseItem.class.getClassLoader());
        if (in.readByte() == 0x01) {
            flights = new ArrayList<Flight>();
            in.readList(flights, Flight.class.getClassLoader());
        } else {
            flights = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(caddy);
        dest.writeLong(booking_date);
        dest.writeValue(course);
        if (flights == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(flights);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<BookingCourseTransaction> CREATOR = new Parcelable.Creator<BookingCourseTransaction>() {
        @Override
        public BookingCourseTransaction createFromParcel(Parcel in) {
            return new BookingCourseTransaction(in);
        }

        @Override
        public BookingCourseTransaction[] newArray(int size) {
            return new BookingCourseTransaction[size];
        }
    };

    public static class Flight implements Parcelable {
        public int flight_number;
        public int player;
        public boolean showing;
        public String timeType;

        public Flight() {
        }

        protected Flight(Parcel in) {
            flight_number = in.readInt();
            player = in.readInt();
            showing = in.readByte() != 0x00;
            timeType = (String) in.readValue(ClassLoader.getSystemClassLoader());
//            timeType = (TeeTime) in.readValue(TeeTime.class.getClassLoader());
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(flight_number);
            dest.writeInt(player);
            dest.writeByte((byte) (showing ? 0x01 : 0x00));
            dest.writeValue(timeType);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Flight> CREATOR = new Parcelable.Creator<Flight>() {
            @Override
            public Flight createFromParcel(Parcel in) {
                return new Flight(in);
            }

            @Override
            public Flight[] newArray(int size) {
                return new Flight[size];
            }
        };
    }


    public static class FlightRequest {
        public String flightnumber;
        public String time_type;
        public String player;

        public FlightRequest(String _flightnumber, String _time_type, String _player) {
            flightnumber = _flightnumber;
            time_type = _time_type;
            player = _player;
        }
    }
}
