package app.ayo.golf.retrofit.service;

import java.util.ArrayList;

import app.ayo.golf.retrofit.pojo.APIResponse;
import app.ayo.golf.retrofit.pojo.Banner;
import app.ayo.golf.retrofit.pojo.BookingCourseDetails;
import app.ayo.golf.retrofit.pojo.BookingCourseResult;
import app.ayo.golf.retrofit.pojo.Caddy;
import app.ayo.golf.retrofit.pojo.CoachingClinic;
import app.ayo.golf.retrofit.pojo.CourseItem;
import app.ayo.golf.retrofit.pojo.DataBookingDetail;
import app.ayo.golf.retrofit.pojo.Entertainment;
import app.ayo.golf.retrofit.pojo.Golfer;
import app.ayo.golf.retrofit.pojo.News;
import app.ayo.golf.retrofit.pojo.ResponseServer;
import app.ayo.golf.retrofit.pojo.TeeTime;
import app.ayo.golf.retrofit.pojo.UploadObject;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by arifina on 7/25/2017.
 */

public interface ApiServices {
    @GET("golfApi.php?action=course")
    Call<ResponseServer> get_course();

    @POST("course")
    Call<APIResponse<ArrayList<CourseItem>>> get_course_list();

    @POST("caddy")
    Call<APIResponse<ArrayList<Caddy>>> get_caddy();

    @POST("article")
    Call<APIResponse<ArrayList<News>>> get_news();

    @FormUrlEncoded
    @POST("signup")
    Call<APIResponse<Golfer>> signup(
            @Field("name") String name,
            @Field("email") String email,
            @Field("pass") String pass,
            @Field("token") String token,
            @Field("channel") String channel
    );

    @FormUrlEncoded
    @POST("signin")
    Call<APIResponse<Golfer>> signin(
            @Field("email") String email,
            @Field("pass") String pass
    );

    @POST("coachingClinic")
    Call<APIResponse<CoachingClinic>> get_cctutorial();

    //INIT RETROFIT 2
    @POST("banner")
    Call<APIResponse<ArrayList<Banner>>> get_banner();

    @POST("advertise")
    Call<APIResponse<ArrayList<Entertainment>>> get_ent();

    @FormUrlEncoded
    @POST("profile")
    Call<APIResponse<Golfer>> profile(
            @Field("golfer_id") String golfer_id
    );


    @FormUrlEncoded
    @POST("teetime")
    Call<APIResponse<ArrayList<TeeTime>>> getTeeTimeAndPrice(
            @Field("course_id") String course_id
    );

//    @FormUrlEncoded
//    @POST("bookedDetail")
//    Call<APIResponse<BookingCourseDetails>> getBookedDetail(
//            @Field("booking_code") String booking_code
//    );

//    @Multipart
//    @FormUrlEncoded
//    @POST("profile")
//    Call<APIResponse<String>> edit_profile(
//            @Field("type") String type,
//            @Field("golfer_id") String golfer_id,
//            @Field("name") String golfer_name,
//            @Field("phone") String phone,
//            //@Field("pass") String pass,
//            @Field("pic") String pic
//            //@Part MultipartBody.Part file
//    );

    @Multipart
    @POST("profile")
    Call<APIResponse<Golfer>> edit_profile(@Part("type") RequestBody type,
                                           @Part("golfer_id") RequestBody golfer_id,
                                           @Part("name") RequestBody name,
                                           @Part("phone") RequestBody phone,
                                           @Part("gender") RequestBody gender,
                                           @Part("handicap_index") RequestBody handicap_index,
                                           @Part("birth_date") RequestBody birth_date,
                                           @Part("email") RequestBody email,
                                           @Part MultipartBody.Part file);

    @FormUrlEncoded
    @POST("booked")
    Call<APIResponse<BookingCourseResult>> booking(
            @Field("golfer_id") String golfer_id,
            @Field("course_id") String course_id,
            @Field("booked_date") String booked_date,
            @Field("caddy") String caddy,
            @Field("flights") String flights,
            @Field("payment") String payment);

    @FormUrlEncoded
    @POST("transactHistory")
    Call<APIResponse<ArrayList<BookingCourseResult>>> transactHistory(
            @Field("golfer_id") String golfer_id,
            @Field("payment_status") String payment_status);

    @FormUrlEncoded
    @POST("bookedDetail")
    Call<APIResponse<DataBookingDetail>> bookedDetail(
            @Field("golfer_id") String golfer_id,
            @Field("course_id") String course_id,
            @Field("booked_date") String booked_date,
            @Field("caddy") String caddy,
            @Field("flights") String flights,
            @Field("payment") String payment);

    // ------------------------------ Untuk Upload File/Foto Confirmation Ke server ---------------------------------------------------------- //
    @Multipart
    @POST("upload")
    Call<UploadObject> upload(
            @Part("description") MultipartBody.Part description,
            @Part RequestBody file);
}
