package app.ayo.golf.retrofit.pojo;

/**
 * Created by arifina on 9/27/2017.
 */

public class Banner {
    /*public String banner_image;
    public String banner_url;
    public String publish_date;
    public String expired_date;*/
    public String url;

    public Banner(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
