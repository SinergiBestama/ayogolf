package app.ayo.golf.retrofit.service;

import app.ayo.golf.entitytiket.searchairport.Airport;
import app.ayo.golf.entitytiket.searchairport.AirportItem;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by admin on 07/03/2018.
 */

public interface flight_api {

    //    https://api-sandbox.tiket.com/flight_api/all_airport?token=71cf2d3dec294394e267fbb0bf28916f4198f8d6&output=json
    @GET("all_airport")
    Call<AirportItem> searchAirport(@Query("token") String token, @Query("output") String output, @Query("q") String string);
}
