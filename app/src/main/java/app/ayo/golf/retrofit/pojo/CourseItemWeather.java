package app.ayo.golf.retrofit.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

import app.ayo.golf.entityweather.weather2.List;

public class CourseItemWeather implements Parcelable {
    private java.util.List<List> list = new ArrayList<>();
    public String icon;
    public String course_id;
    public String course_name;
    public String course_image;
    public String course_address;
    public String course_city;
    public double lat;
    public double lng;
    public String holes;
    public String par;
    public String length;
    public String open_time;
    public String close_time;
    public String est_in;
    public String designer;
    public ArrayList<CourseItem.Gallery> galleries;
    public ArrayList<CourseItem.Amenities> amenities;

    public CourseItemWeather() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.list);
        dest.writeString(this.icon);
        dest.writeString(this.course_id);
        dest.writeString(this.course_name);
        dest.writeString(this.course_image);
        dest.writeString(this.course_address);
        dest.writeString(this.course_city);
        dest.writeDouble(this.lat);
        dest.writeDouble(this.lng);
        dest.writeString(this.holes);
        dest.writeString(this.par);
        dest.writeString(this.length);
        dest.writeString(this.open_time);
        dest.writeString(this.close_time);
        dest.writeString(this.est_in);
        dest.writeString(this.designer);
        dest.writeTypedList(this.galleries);
        dest.writeTypedList(this.amenities);
    }

    protected CourseItemWeather(Parcel in) {
        this.list = new ArrayList<List>();
        in.readList(this.list, List.class.getClassLoader());
        this.icon = in.readString();
        this.course_id = in.readString();
        this.course_name = in.readString();
        this.course_image = in.readString();
        this.course_address = in.readString();
        this.course_city = in.readString();
        this.lat = in.readDouble();
        this.lng = in.readDouble();
        this.holes = in.readString();
        this.par = in.readString();
        this.length = in.readString();
        this.open_time = in.readString();
        this.close_time = in.readString();
        this.est_in = in.readString();
        this.designer = in.readString();
        this.galleries = in.createTypedArrayList(CourseItem.Gallery.CREATOR);
        this.amenities = in.createTypedArrayList(CourseItem.Amenities.CREATOR);
    }

    public static final Creator<CourseItemWeather> CREATOR = new Creator<CourseItemWeather>() {
        @Override
        public CourseItemWeather createFromParcel(Parcel source) {
            return new CourseItemWeather(source);
        }

        @Override
        public CourseItemWeather[] newArray(int size) {
            return new CourseItemWeather[size];
        }
    };
}
