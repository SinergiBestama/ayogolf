package app.ayo.golf.retrofit.service;

import java.util.concurrent.TimeUnit;

import app.ayo.golf.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by admin on 07/06/2018.
 */

public class InitRetrofitWeather {

    private static boolean ENABLE_LOGGING = BuildConfig.DEBUG;

    public static Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org/data/2.5/")
                .client(generateClientWeather())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private static OkHttpClient generateClientWeather() {
        OkHttpClient.Builder builderWeather = new OkHttpClient.Builder();
        if (ENABLE_LOGGING) {
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builderWeather.addInterceptor(httpLoggingInterceptor);
        }
        return builderWeather.build();
    }

    public static ApiServicesWeather getServicesWeather() {
        return getRetrofit().create(ApiServicesWeather.class);
    }


//    private static boolean ENABLE_LOGGING = BuildConfig.DEBUG;
//    private static String BASE_URL = "http://api.openweathermap.org/data/2.5/";
//
//    public static <T> T getServiceWeather(Class<T> services) {
//        Retrofit retrofit = new Retrofit.Builder()
////                .baseUrl(BASE_URL + services.getSimpleName() + "/")
//                .baseUrl(BASE_URL)
//                .client(generateClient())
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//        return retrofit.create(services);
//    }
//
//    private static OkHttpClient generateClient() {
//        OkHttpClient.Builder builder = new OkHttpClient.Builder();
//        builder.connectTimeout(60 * 1000, TimeUnit.MILLISECONDS)
//                .writeTimeout(60 * 1000, TimeUnit.MILLISECONDS)
//                .readTimeout(60 * 1000, TimeUnit.MILLISECONDS);
//
//        if (ENABLE_LOGGING) {
//            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
//            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//            builder.addInterceptor(loggingInterceptor);
//        }
//        return builder.build();
//    }
}
