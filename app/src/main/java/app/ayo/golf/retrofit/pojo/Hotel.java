package app.ayo.golf.retrofit.pojo;

import java.io.Serializable;

/**
 * Created by admin on 18/02/2018.
 */

public class Hotel implements Serializable {

    @Override
    public String toString() {
        return "HotelFind{" +
                "judul='" + judul + '\'' +
                ", hotel_id='" + hotel_id + '\'' +
                ", room_available='" + room_available + '\'' +
                ", tripadvisor_avg_rating=" + tripadvisor_avg_rating +
                ", total_price='" + total_price + '\'' +
                ", kelurahan_name='" + kelurahan_name + '\'' +
                ", photo_primary='" + photo_primary + '\'' +
                ", id='" + id + '\'' +
                ", wifi='" + wifi + '\'' +
                ", price='" + price + '\'' +
                ", province_name='" + province_name + '\'' +
                ", room_max_occupancies='" + room_max_occupancies + '\'' +
                ", address='" + address + '\'' +
                ", regional='" + regional + '\'' +
                ", name='" + name + '\'' +
                ", longitude='" + longitude + '\'' +
                ", kecamatan_name='" + kecamatan_name + '\'' +
                ", business_uri='" + business_uri + '\'' +
                ", latitude='" + latitude + '\'' +
                ", rating='" + rating + '\'' +
                ", star_rating='" + star_rating + '\'' +
                ", room_facility_name='" + room_facility_name + '\'' +
                ", promo_name='" + promo_name + '\'' +
                ", oldprice='" + oldprice + '\'' +
                ", callingPackage=" + callingPackage +
                '}';
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setCallingPackage(int callingPackage) {
        this.callingPackage = callingPackage;
    }

    private String value;
    private String judul;

    private String hotel_id;

    private String room_available;

    private Tripadvisor_avg_rating tripadvisor_avg_rating;

    private String total_price;

    private String kelurahan_name;

    private String photo_primary;

    private String id;

    private String wifi;

    private String price;

    public String province_name;

    private String room_max_occupancies;

    private String address;

    private String regional;

    private String name;

    private String longitude;

    private String kecamatan_name;

    private String business_uri;

    private String latitude;

    private String rating;

    private String star_rating;

    private String room_facility_name;

    private String promo_name;

    private String oldprice;
    private int callingPackage;

    public String getHotel_id() {
        return hotel_id;
    }

    public void setHotel_id(String hotel_id) {
        this.hotel_id = hotel_id;
    }

    public String getRoom_available() {
        return room_available;
    }

    public void setRoom_available(String room_available) {
        this.room_available = room_available;
    }

    public Tripadvisor_avg_rating getTripadvisor_avg_rating() {
        return tripadvisor_avg_rating;
    }

    public void setTripadvisor_avg_rating(Tripadvisor_avg_rating tripadvisor_avg_rating) {
        this.tripadvisor_avg_rating = tripadvisor_avg_rating;
    }

    public String getTotal_price() {
        return total_price;
    }

    public void setTotal_price(String total_price) {
        this.total_price = total_price;
    }

    public String getKelurahan_name() {
        return kelurahan_name;
    }

    public void setKelurahan_name(String kelurahan_name) {
        this.kelurahan_name = kelurahan_name;
    }

    public String getPhoto_primary() {
        return photo_primary;
    }

    public void setPhoto_primary(String photo_primary) {
        this.photo_primary = photo_primary;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWifi() {
        return wifi;
    }

    public void setWifi(String wifi) {
        this.wifi = wifi;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getProvince_name() {
        return province_name;
    }

    public void setProvince_name(String province_name) {
        this.province_name = province_name;
    }

    public String getRoom_max_occupancies() {
        return room_max_occupancies;
    }

    public void setRoom_max_occupancies(String room_max_occupancies) {
        this.room_max_occupancies = room_max_occupancies;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRegional() {
        return regional;
    }

    public void setRegional(String regional) {
        this.regional = regional;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getKecamatan_name() {
        return kecamatan_name;
    }

    public void setKecamatan_name(String kecamatan_name) {
        this.kecamatan_name = kecamatan_name;
    }

    public String getBusiness_uri() {
        return business_uri;
    }

    public void setBusiness_uri(String business_uri) {
        this.business_uri = business_uri;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getStar_rating() {
        return star_rating;
    }

    public void setStar_rating(String star_rating) {
        this.star_rating = star_rating;
    }

    public String getRoom_facility_name() {
        return room_facility_name;
    }

    public void setRoom_facility_name(String room_facility_name) {
        this.room_facility_name = room_facility_name;
    }

    public String getPromo_name() {
        return promo_name;
    }

    public void setPromo_name(String promo_name) {
        this.promo_name = promo_name;
    }

    public String getOldprice() {
        return oldprice;
    }

    public void setOldprice(String oldprice) {
        this.oldprice = oldprice;
    }

    public int getText(int position) {
        return 0;
    }

    public int getCallingPackage() {
        return callingPackage;
    }
}