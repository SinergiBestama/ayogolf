package app.ayo.golf.retrofit.service;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by arifina on 9/25/2017.
 */

public class InitRetrofit2 {
    //init library Retrofit di project
    public static Retrofit getInitRetrofit(){
        return  new Retrofit.Builder().baseUrl("http://sincerelylab.com/ayogolf/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    //api service membutuhkan init retrofit juga, sehingga hrs dipanggil classnya
    //supaya bisa dipanggil di class lain, tambahkan static
    public static ApiServices getInstanceRetrofit(){
        return getInitRetrofit().create(ApiServices.class);
    }
}
