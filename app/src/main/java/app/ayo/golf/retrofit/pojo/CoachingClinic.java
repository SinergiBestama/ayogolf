package app.ayo.golf.retrofit.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by arifina on 8/14/2017.
 */

public class CoachingClinic implements Parcelable {
    public ArrayList<Coach> coach;
    public ArrayList<Tutorial> tutorial;

    protected CoachingClinic(Parcel in) {
        if (in.readByte() == 0x01) {
            coach = new ArrayList<Coach>();
            in.readList(coach, Coach.class.getClassLoader());
        } else {
            coach = null;
        }
        if (in.readByte() == 0x01) {
            tutorial = new ArrayList<Tutorial>();
            in.readList(tutorial, Tutorial.class.getClassLoader());
        } else {
            tutorial = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (coach == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(coach);
        }
        if (tutorial == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(tutorial);
        }
    }


    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CoachingClinic> CREATOR = new Parcelable.Creator<CoachingClinic>() {
        @Override
        public CoachingClinic createFromParcel(Parcel in) {
            return new CoachingClinic(in);
        }

        @Override
        public CoachingClinic[] newArray(int size) {
            return new CoachingClinic[size];
        }
    };
}