package app.ayo.golf.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import app.ayo.golf.FlightActivity;
import app.ayo.golf.R;
import app.ayo.golf.entitytiket.searchairport.Airport;

/**
 * Created by admin on 07/03/2018.
 */

public class SearchAirportAdapter extends RecyclerView.Adapter<SearchAirportAdapter.AirportViewHolder> implements Filterable {

    private List<Airport> data;
    private List<Airport> arraylist;
    private SearchAirportTujuanAdapter.FlightListener listener;

    public SearchAirportAdapter(List<Airport> data, SearchAirportTujuanAdapter.FlightListener listener) {
        this.data = data;
        this.listener = listener;
    }

    @Override
    public AirportViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.item_search_airport, parent, false);
        return new AirportViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AirportViewHolder holder, final int position) {
        Airport airport = data.get(position);
        final AirportViewHolder airportViewHolder = (AirportViewHolder) holder;
        airportViewHolder.tvAllAirport.setText(airport.getLocationName());
        airportViewHolder.tvCodeAirport.setText("(" + airport.getAirportCode() + ")");
        airportViewHolder.tvAirportName.setText("" + airport.getAirportName());

        holder.itemView.setOnClickListener(e -> listener.flightSelected(data.get(position)));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void updateList(List<Airport> airports) {
        this.data = airports;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    arraylist = data;
                } else {
                    List<Airport> filteredList = new ArrayList<>();
                    for (Airport row : data) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getLocationName().toLowerCase().contains(charString.toLowerCase()) || row.getAirportName().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }
                    arraylist = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = arraylist;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                data = (List<Airport>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public void filter(String text) {

        text = text.toLowerCase(Locale.getDefault());
        data.clear();
        if (text.length() == 0) {
            data.addAll(arraylist);
        } else {
            for (Airport wp : arraylist) {
                if (wp.getLocationName().toLowerCase(Locale.getDefault()).contains(text)) {
                    data.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class AirportViewHolder extends RecyclerView.ViewHolder {

        TextView tvAllAirport, tvCodeAirport, tvAirportName;

        public AirportViewHolder(final View itemView) {
            super(itemView);

            tvAllAirport = (TextView) itemView.findViewById(R.id.tv_allAirport);
            tvAllAirport.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            tvCodeAirport = (TextView) itemView.findViewById(R.id.tv_codeAirport);
            tvAirportName = (TextView) itemView.findViewById(R.id.tv_airportName);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Toast.makeText(itemView.getContext(), "This : "+getAdapterPosition(), Toast.LENGTH_LONG).show();
                    String airportBerangkat = tvAllAirport.getText().toString() + tvCodeAirport.getText().toString() + tvAirportName.getText().toString();
                    Intent searchAirport = new Intent(view.getContext(), FlightActivity.class);
                    airportBerangkat = airportBerangkat.replaceAll("[^A-Z,a-z]", "-");
                    searchAirport.putExtra("airportKeberangkatan", airportBerangkat);
                    view.getContext().startActivity(searchAirport);
                }
            });
        }
    }
}
