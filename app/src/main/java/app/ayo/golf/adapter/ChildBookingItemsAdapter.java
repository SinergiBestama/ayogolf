package app.ayo.golf.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import app.ayo.golf.R;
import app.ayo.golf.retrofit.pojo.BookingCourseResult;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by admin on 01/06/2018.
 */

public class ChildBookingItemsAdapter extends RecyclerView.Adapter<ChildBookingItemsAdapter.ChildViewHolder> {

    BookingCourseResult bookingCourseResult;
    private ArrayList<BookingCourseResult.DetailFlight> childBookingItems = new ArrayList<>();
    Activity c;

    public ChildBookingItemsAdapter(ArrayList<BookingCourseResult.DetailFlight> childBookingItems, Activity c) {
        this.childBookingItems = childBookingItems;
        this.c = c;
    }

    @Override
    public ChildViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_child_booking_history, parent, false);
        return new ChildViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ChildViewHolder holder, int position) {
        if (holder instanceof ChildViewHolder) {
            ChildViewHolder viewHolder = (ChildViewHolder) holder;
            BookingCourseResult.DetailFlight listChild = bookingCourseResult.detailFlight.get(position);
            viewHolder.tvChildFlightNumber.setText(listChild.flightnumber);
            viewHolder.tvChildPlayer.setText(listChild.player);
        }

    }

    @Override
    public int getItemCount() {
        return bookingCourseResult.detailFlight.size();
    }

    public class ChildViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_child_flight_number)
        TextView tvChildFlightNumber;
        @BindView(R.id.tv_child_player)
        TextView tvChildPlayer;

        public ChildViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
