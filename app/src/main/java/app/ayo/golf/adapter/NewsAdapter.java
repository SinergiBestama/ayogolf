package app.ayo.golf.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import app.ayo.golf.NewsDetailActivity;
import app.ayo.golf.R;
import app.ayo.golf.retrofit.pojo.News;

/**
 * Created by Hendi on 10/1/17.
 */

public class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<News> data;
    RecyclerView r;
    Activity c;
    SimpleDateFormat sdf = new SimpleDateFormat("dd MM yyyy - HH:mm", Locale.getDefault());
    Calendar cal = Calendar.getInstance();

    public NewsAdapter(ArrayList<News> data, RecyclerView r, Activity c) {
        this.data = data;
        this.r = r;
        this.c = c;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.item_news, parent, false);
        final NewsVH holder = new NewsVH(v);
        holder.clickArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(c, NewsDetailActivity.class);
                i.putExtra("news",  data.get(holder.getAdapterPosition()));
                c.startActivity(i);
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder vh, int position) {
        NewsVH holder = (NewsVH) vh;

        // set title
        holder.tvNewsTitle.setText(data.get(position).news_title);
        // set time
        //cal.setTimeInMillis(data.get(position).publish_date);
        //holder.tvNewsTime.setText(sdf.format(cal.getTime()));

        holder.tvNewsTime.setText(data.get(position).publish_date);

        if (data.get(position).news_image.isEmpty())
            holder.ivNewsPic.setImageResource(R.drawable.no_photo);
        else{
            RequestOptions opt = new RequestOptions().placeholder(R.drawable.no_photo);
            Glide.with(c)
                    .load(data.get(position).news_image)
                    .apply(opt)
                    .into(holder.ivNewsPic);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class NewsVH extends RecyclerView.ViewHolder {
        ImageView ivNewsPic;
        TextView tvNewsTitle;
        TextView tvNewsTime;
        View clickArea;

        public NewsVH(View itemView) {
            super(itemView);
            ivNewsPic = (ImageView) itemView.findViewById(R.id.iv_news_pic);
            tvNewsTitle = (TextView) itemView.findViewById(R.id.tv_news_title);
            tvNewsTime = (TextView) itemView.findViewById(R.id.tv_news_time);
            clickArea = itemView.findViewById(R.id.ll_click_area);
        }
    }
}

