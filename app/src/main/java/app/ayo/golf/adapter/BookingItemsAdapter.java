package app.ayo.golf.adapter;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import app.ayo.golf.BookingHistoryDetail;
import app.ayo.golf.PaymentActivity;
import app.ayo.golf.R;
import app.ayo.golf.ReceiptActivity2;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.pojo.BookingCourseResult;
import app.ayo.golf.retrofit.pojo.BookingCourseTransaction;
import app.ayo.golf.retrofit.pojo.DataBookingDetail;
import app.ayo.golf.retrofit.pojo.DetailFlight;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Haries on 27/11/17.
 */

public class BookingItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Date currentTime = Calendar.getInstance().getTime();
    private ArrayList<BookingCourseResult> mBookingItems = new ArrayList<>();
    private boolean disableClick;
    private static final String KEY_REQUEST = "Booking_item";

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_booking_history, parent, false);
        final BookingItemHolder holder = new BookingItemHolder(view);
        holder.mClickArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), PaymentActivity.class);
                i.putExtra("booking_result", (Parcelable) mBookingItems.get(holder.getAdapterPosition()));
                v.getContext().startActivity(i);
            }
        });
        return holder;
    }

    public void setItems(ArrayList<BookingCourseResult> mBookingItems) {
        this.mBookingItems.clear();
        this.mBookingItems.addAll(mBookingItems);
        // sort by newest date
        Collections.sort(this.mBookingItems, new Comparator<BookingCourseResult>() {
            @Override
            public int compare(BookingCourseResult o1, BookingCourseResult o2) {
                Date date1 = Util.stringToDate(o1.created_date);
                Date date2 = Util.stringToDate(o2.created_date);
                return date2.compareTo(date1);
            }
        });
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof BookingItemHolder) {
            BookingItemHolder itemHolder = (BookingItemHolder) holder;
            BookingCourseResult item = mBookingItems.get(position);
            //================================== Only View =======================================//
            itemHolder.tvBookedFor.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            itemHolder.tvTotalView.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            //====================================================================================//
            itemHolder.tvBookingDate.setText(Util.formatDate(item.created_date, "EEE MMM dd, yyyy"));
            itemHolder.tvCourseName.setText(item.course_name);
            itemHolder.tvCourseName.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            itemHolder.tvStatus.setText(item.payment_status);
            itemHolder.tvStatusMessage.setText(item.status_message);
            itemHolder.tvBookCreateDate.setText(Util.formatDate(item.booked_date, "EEE MMM dd, yyyy"));
//            itemHolder.tvBookCreateDate.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            itemHolder.ivStatusIndicator.setImageResource(
                    "paid".equalsIgnoreCase(item.payment_status) ? R.drawable.ic_done :
                            "completed".equalsIgnoreCase(item.payment_status) ? R.drawable.ic_done :
                                    "canceled".equalsIgnoreCase(item.payment_status) ? R.drawable.ic_failed :
                                            R.drawable.ic_waiting
            );

            if ("completed".equalsIgnoreCase(item.payment_status) || "canceled".equalsIgnoreCase(item.payment_status))
                itemHolder.mClickArea.setEnabled(false);
            else
                itemHolder.mClickArea.setEnabled(true);
            long totalPrice = Long.parseLong(item.total);
            itemHolder.tvTotalPrice.setText(Util.formatToCurrency(totalPrice));
            itemHolder.tvTotalPrice.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        }
    }

    @Override
    public int getItemCount() {
        return mBookingItems.size();
    }

    public boolean isEmpty() {
        return getItemCount() < 1;
    }

    public void disableClick(boolean disable) {
        disableClick = disable;
    }

    public class BookingItemHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_status)
        TextView tvStatus;
        @BindView(R.id.tv_status_message)
        TextView tvStatusMessage;
        @BindView(R.id.iv_status_indicator)
        ImageView ivStatusIndicator;
        @BindView(R.id.tv_booking_date)
        TextView tvBookingDate;
        @BindView(R.id.tv_course_name)
        TextView tvCourseName;
        @BindView(R.id.tv_total_price)
        TextView tvTotalPrice;
        @BindView(R.id.ll_click_area)
        View mClickArea;
        @BindView(R.id.tv_book_create_date)
        TextView tvBookCreateDate;
        @BindView(R.id.tv_session_time)
        TextView tvSessionTime;
        @BindView(R.id.tv_book_player)
        TextView tvBookPlayer;
        @BindView(R.id.tv_booked_for)
        TextView tvBookedFor;
        @BindView(R.id.tv_total_view)
        TextView tvTotalView;

        public BookingItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

//    public void randomInProgress(){
//        BookingCourseResult book1 = new BookingCourseResult();
//        book1.booked_date = "25/10/2017";
//        book1.course_name = "Palm Hill Golf";
//        book1.status = "Unpaid";
//        book1.status_message = "Waiting for payment.";
//        book1.total = "1500000";
//
//        mBookingItems.add(book1);
//    }
//
//    public void randomHistory(){
//        BookingCourseResult book1 = new BookingCourseResult();
//        book1.booked_date = "21/10/2017";
//        book1.course_name = "Palm Hill Golf";
//        book1.status = "Canceled";
//        book1.status_message = "Booking is canceled by system. There's no payment received till due date.";
//        book1.total = "1500000";
//
//        mBookingItems.add(book1);
//
//        BookingCourseResult book2 = new BookingCourseResult();
//        book2.booked_date = "10/9/2017";
//        book2.course_name = "Senayan National Golf";
//        book2.status = "Completed";
//        book2.status_message = "Booking is paid and completed.";
//        book2.total = "3000000";
//
//        mBookingItems.add(book2);
//
//        BookingCourseResult book3 = new BookingCourseResult();
//        book3.booked_date = "21/10/2017";
//        book3.course_name = "Rancamaya Golfclub";
//        book3.status = "Canceled";
//        book3.status_message = "Booking is canceled by user";
//        book3.total = "1000000";
//
//        mBookingItems.add(book3);
//    }
}
