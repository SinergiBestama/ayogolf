package app.ayo.golf.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import app.ayo.golf.R;
import app.ayo.golf.retrofit.pojo.Banner;

/**
 * Created by arifina on 8/4/2017.
 */

public class BannerAdapter extends PagerAdapter {
    ArrayList<Banner> gambar;
    Activity activity;

    public BannerAdapter(ArrayList<Banner> gambar, Activity activity){
        this.gambar = gambar;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return gambar.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position){
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.custom_list_banner, container, false);
        //TextView txt = (TextView) v.findViewById(R.id.textpager);
        ImageView img = (ImageView) v.findViewById(R.id.imgbanner);

        //txt.setText(nama[position]);
        //img.setImageResource(gambar[position]);
        RequestOptions opt = new RequestOptions().placeholder(R.drawable.no_photo);
        Glide.with(activity)
                .load(gambar.get(position).url)
                .apply(opt)
                .into(img);

        container.addView(v);
        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {

        return view==((LinearLayout)object);
    }
}
