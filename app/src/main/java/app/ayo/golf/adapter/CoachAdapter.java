package app.ayo.golf.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import app.ayo.golf.CoachDetailActivity;
import app.ayo.golf.R;
import app.ayo.golf.retrofit.pojo.Coach;

/**
 * Created by arifina on 10/20/2017.
 */

public class CoachAdapter extends RecyclerView.Adapter<CoachAdapter.MyHolder> {
    ArrayList<Coach> data;
    Activity c;
    RecyclerView r;

    public CoachAdapter(ArrayList<Coach> data, Activity c, RecyclerView r) {
        this.data = data;
        this.c = c;
        this.r = r;
    }



    @Override
    public CoachAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.custom_list_coach,parent,false);
        final MyHolder holder = new MyHolder(v);

        holder.clickArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), CoachDetailActivity.class);
                intent.putExtra(
                        CoachDetailActivity.EXTRA_COACH_DETAIL, data.get(holder.getAdapterPosition()));
                v.getContext().startActivity(intent);
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(CoachAdapter.MyHolder holder, int position) {
        Glide.with(c)
                .load(data.get(position).coach_image)
                .into(holder.imgcoach);
        holder.nama.setText(data.get(position).coach_name);
        holder.title.setText(data.get(position).coach_title);
//        holder.desc.setText(data.get(position).coach_desc.substring(0,90)+"...");
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        TextView nama;
        TextView title;
        TextView desc;
        ImageView imgcoach;
        View clickArea;
        public MyHolder(View itemView) {
            super(itemView);
            imgcoach = (ImageView) itemView.findViewById(R.id.imgcoach);
            nama = (TextView) itemView.findViewById(R.id.txtcoachname);
            title = (TextView) itemView.findViewById(R.id.txtcoachtitle);
            desc = (TextView) itemView.findViewById(R.id.txtcoachdesc);
            clickArea = itemView.findViewById(R.id.ll_click_area);
        }
    }
}
