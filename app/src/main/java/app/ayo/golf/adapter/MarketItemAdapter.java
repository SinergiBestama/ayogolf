package app.ayo.golf.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import app.ayo.golf.R;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.pojo.MarketItem;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Haries on 15/11/17.
 */

public class MarketItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<MarketItem> marketItems;

    public MarketItemAdapter(ArrayList<MarketItem> marketItems) {
        //this.marketItems = marketItems;
        randomData();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_market, parent, false);
        MarketItemHolder itemHolder = new MarketItemHolder(view);
        itemHolder.mClickArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(v.getContext(), "Under Construction", Toast.LENGTH_SHORT).show();
            }
        });
        return itemHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof MarketItemHolder){
            final MarketItemHolder itemHolder = (MarketItemHolder) holder;
            itemHolder.mTvItemName.setText(marketItems.get(position).item_title);
            itemHolder.mTvItemPrice.setText(
                    Util.formatToCurrency(
                            marketItems.get(position).item_price
                    )
            );
//            RequestOptions opt = new RequestOptions().placeholder(R.drawable.no_photo);
//            Glide.with(itemHolder.itemView.getContext())
//                    .load(marketItems.get(position).item_pic)
//                    .apply(opt)
//                    .into(itemHolder.mIvItemPic);
            itemHolder.mIvItemPic.setImageResource(marketItems.get(position).item_pic);
        }
    }

    @Override
    public int getItemCount() {
        return marketItems.size();
    }

    public class MarketItemHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_item_pic) ImageView mIvItemPic;
        @BindView(R.id.tv_item_name) TextView mTvItemName;
        @BindView(R.id.tv_item_price) TextView mTvItemPrice;
        @BindView(R.id.ll_click_area) View mClickArea;

        public MarketItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    void randomData(){
        marketItems = new ArrayList<MarketItem>();

        marketItems.add(new MarketItem("0", "Polo Shirt Body Armour", R.drawable.baju1, 200000));
        marketItems.add(new MarketItem("1", "Polo Shirt Body Armour", R.drawable.baju2, 225000));
        marketItems.add(new MarketItem("2", "Polo Shirt Body Armour", R.drawable.baju3, 195000));
        marketItems.add(new MarketItem("3", "Cap Body Armour", R.drawable.sarung1, 105000));
        marketItems.add(new MarketItem("4", "Cap Body Armour", R.drawable.sarung2, 115000));
        marketItems.add(new MarketItem("5", "Cap Body Armour", R.drawable.sarung3, 95000));
        marketItems.add(new MarketItem("6", "Cap Body Armour", R.drawable.sarung1, 100000));
    }
}
