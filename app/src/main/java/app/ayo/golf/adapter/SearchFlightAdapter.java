package app.ayo.golf.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import app.ayo.golf.R;
import app.ayo.golf.entitytiket.searchflight.ResultFlight;
import app.ayo.golf.entitytiket.searchflight.ResultSearchFlight;

/**
 * Created by admin on 12/03/2018.
 */

public class SearchFlightAdapter extends RecyclerView.Adapter<SearchFlightAdapter.FlightViewHolder> {

    private List<ResultFlight> data;

    public SearchFlightAdapter(List<ResultFlight> data) {
        this.data = data;
    }

    @Override
    public FlightViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_search_flight, parent, false);
        return new FlightViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FlightViewHolder holder, int position) {
        ResultFlight object = data.get(position);
        final FlightViewHolder flightViewHolder = (FlightViewHolder) holder;

        if (data.get(position).getImage().isEmpty())
            holder.ivIconFlight.setImageResource(R.drawable.no_photo);
        else {
            RequestOptions opt = new RequestOptions().placeholder(R.drawable.no_photo);
            Glide.with(flightViewHolder.ivIconFlight.getContext())
                    .load(data.get(position).getImage())
                    .apply(opt)
                    .into(holder.ivIconFlight);

            //flightViewHolder.ivIconFlight.setImageResource(Integer.parseInt(object.getImgSrc()));

        }
        flightViewHolder.tvnamaArmadaFlight.setText(object.getAirlinesName());
    }

    @Override
    public int getItemCount() {
        return (data == null) ? 0 : data.size();
    }

//    public void updateList(List<ResultFlight> flightList) {
//        this.data = flightList;
//    }

    public class FlightViewHolder extends RecyclerView.ViewHolder {

        ImageView ivIconFlight;
        TextView tvnamaArmadaFlight;


        public FlightViewHolder(View itemView) {
            super(itemView);

            ivIconFlight = (ImageView) itemView.findViewById(R.id.iv_searchFlight);
            tvnamaArmadaFlight = (TextView) itemView.findViewById(R.id.tv_namaArmadaFlight);
        }
    }
}
