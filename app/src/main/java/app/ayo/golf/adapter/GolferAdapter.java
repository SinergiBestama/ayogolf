package app.ayo.golf.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import app.ayo.golf.R;
import app.ayo.golf.retrofit.pojo.Golfer;

/**
 * Created by arifina on 7/27/2017.
 */

public class GolferAdapter extends RecyclerView.Adapter<GolferAdapter.MyHolder> {

    Activity c;
    RecyclerView r;
    ArrayList<Golfer> data;

    public GolferAdapter(ArrayList<Golfer> data, RecyclerView r, Activity c) {
        this.data = data;
        this.r = r;
        this.c = c;
    }

    /*View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //array position
            int posisi = r.getChildLayoutPosition(v);
            Intent intent = new Intent(c,DetailBeritaActivity.class);
            intent.putExtra("jdl",data.get(posisi).getJudul());
            intent.putExtra("desk",data.get(posisi).getDeskripsi());
            intent.putExtra("gbr",data.get(posisi).getGambar());
            c.startActivity(intent);
        }
    };*/


    @Override
    public GolferAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.custom_list_golfer,parent,false);

        //v.setOnClickListener(listener);
        return new GolferAdapter.MyHolder(v);
    }

    @Override
    public void onBindViewHolder(GolferAdapter.MyHolder holder, int position) {
        holder.textname.setText(data.get(position).getGolfer_name());
        /*if(data.get(position).getGolfer_pic().isEmpty()){
            holder.image.setImageResource(R.drawable.no_photo);
        }
        else {*/
            Glide.with(c)
                    .load(data.get(position).golfer_pic)
                    .into(holder.image);
        //}
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView textname;
        public MyHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.imggolfer);
            textname = (TextView) itemView.findViewById(R.id.txtgolfer_name);
        }
    }
}
