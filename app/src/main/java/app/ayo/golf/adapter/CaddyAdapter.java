package app.ayo.golf.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import app.ayo.golf.R;
import app.ayo.golf.retrofit.pojo.Caddy;

/**
 * Created by arifina on 7/26/2017.
 */

public class CaddyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Caddy> data;
    RecyclerView r;
    Activity c;

    public CaddyAdapter(ArrayList<Caddy> data, RecyclerView r, Activity c) {
        this.data = data;
        this.r = r;
        this.c = c;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.item_caddy, parent, false);
        final CaddyVH holder = new CaddyVH(v);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dt = new Intent();
                dt.putExtra("caddy", data.get(holder.getAdapterPosition()));
                c.setResult(Activity.RESULT_OK, dt);
                c.finish();
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder vh, int position) {
        CaddyVH holder = (CaddyVH) vh;
        Caddy caddy = data.get(position);

        holder.tvCaddyName.setText(caddy.caddy_name);

        if (data.get(position).caddy_pic.isEmpty()) {
            holder.ivCaddyPic.setImageResource(R.mipmap.ic_noimg);
        } else {
            RequestOptions opt = new RequestOptions().placeholder(R.drawable.default_picture);
            Glide.with(c)
                    .load(data.get(position).caddy_pic)
                    .apply(opt)
                    .into(holder.ivCaddyPic);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class CaddyVH extends RecyclerView.ViewHolder {
        public ImageView ivCaddyPic;
        public TextView tvCaddyName;

        public CaddyVH(View itemView) {
            super(itemView);
            ivCaddyPic = (ImageView) itemView.findViewById(R.id.iv_caddy_pic);
            tvCaddyName = (TextView) itemView.findViewById(R.id.tv_caddy_name);
        }
    }
}
