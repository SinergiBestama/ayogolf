package app.ayo.golf.adapter;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.List;

import app.ayo.golf.retrofit.pojo.TeeTime;

/**
 * Created by Hendi on 10/4/17.
 */

public class MySpinnerAdapter<T> implements SpinnerAdapter {
    List<T> data;
    Context context;

    public MySpinnerAdapter(Context context, List<T> data){
        this.context = context;
        this.data = data;
    }

    @Override
    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        if(view == null){
            view = LayoutInflater.from(context).inflate(android.R.layout.simple_spinner_dropdown_item, null);
        }
        setLabel((TextView) view, data.get(i));
        return view;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public T getItem(int i) {
        return data.get(i) ;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null){
            view = LayoutInflater.from(context).inflate(android.R.layout.simple_spinner_dropdown_item, null);
        }
        setLabel((TextView) view, data.get(i));
        return view;
    }

    @Override
    public int getItemViewType(int i) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    private void setLabel(TextView tv, T item){
        if(item instanceof TeeTime) {
            TeeTime tt = (TeeTime) item;
            tv.setText(tt.time_type);
        }
        else if(item instanceof String) {
            tv.setText((CharSequence) item);
        }
        else{
            tv.setText("Not found");
        }
    }
}
