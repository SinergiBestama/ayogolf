package app.ayo.golf.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import app.ayo.golf.CCVideoActivity;
import app.ayo.golf.R;
import app.ayo.golf.helper.YoutubeHelper;
import app.ayo.golf.retrofit.pojo.Tutorial;

/**
 * Created by arifina on 8/24/2017.
 */

public class CCAdapter extends RecyclerView.Adapter<CCAdapter.MyHolder> {

    Activity c;
    RecyclerView r;
    ArrayList<Tutorial> data;

    public CCAdapter(Activity c, RecyclerView r, ArrayList<Tutorial> data) {
        this.c = c;
        this.r = r;
        this.data = data;
    }

    @Override
    public CCAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.custom_cc_video, parent, false);
        final MyHolder holder = new MyHolder(v);
        holder.clickArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(c, CCVideoActivity.class);
                intent.putExtra("tutorial", data.get(holder.getAdapterPosition()));
                c.startActivity(intent);
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(CCAdapter.MyHolder holder, int position) {
        holder.cctitle.setText(data.get(position).title);

        String ytID = YoutubeHelper.getYTId(data.get(position).source_url);
        String imgThumbnail = YoutubeHelper.getHQThumbnail(ytID);

        Bitmap bMap = ThumbnailUtils.createVideoThumbnail(imgThumbnail.concat(ytID), MediaStore.Images.Thumbnails.MINI_KIND);

//        Picasso.get()
//                .load(imgThumbnail)
//                .placeholder(R.drawable.no_photo)
//                .into(holder.ccvid);
        holder.ccvid.setImageBitmap(bMap);
        RequestOptions opt = new RequestOptions().placeholder(R.drawable.no_photo);
        Glide.with(c)
                .load(imgThumbnail)
                .apply(opt)
                .thumbnail(Glide.with(c).load(bMap))
                .into(holder.ccvid);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        ImageView ccvid;
        TextView cctitle;
        View clickArea;

        public MyHolder(View itemView) {
            super(itemView);
            ccvid = (ImageView) itemView.findViewById(R.id.imgthumbnail);
            cctitle = (TextView) itemView.findViewById(R.id.txttitle);
            clickArea = itemView.findViewById(R.id.ll_click_area);
        }
    }
}
