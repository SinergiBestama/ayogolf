package app.ayo.golf.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import app.ayo.golf.R;
import app.ayo.golf.retrofit.pojo.Entertainment;

/**
 * Created by arifina on 10/18/2017.
 */

public class EntAdapter extends RecyclerView.Adapter<EntAdapter.MyHolder>{
    Activity c;
    ArrayList<Entertainment> data;
    RecyclerView r;

    public EntAdapter(Activity c, ArrayList<Entertainment> data, RecyclerView r) {
        this.c = c;
        this.data = data;
        this.r = r;
    }

    @Override
    public EntAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.activity_custom_ent,parent,false);
        return new MyHolder(v);
    }

    @Override
    public void onBindViewHolder(EntAdapter.MyHolder holder, int position) {
        Glide.with(c)
                .load(data.get(position).banner_img)
                .into(holder.imgent);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        ImageView imgent;
        public MyHolder(View itemView) {
            super(itemView);
            imgent = (ImageView) itemView.findViewById(R.id.imgent);
        }
    }
}
