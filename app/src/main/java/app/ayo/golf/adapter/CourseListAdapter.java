package app.ayo.golf.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import app.ayo.golf.DetailCourseActivity;
import app.ayo.golf.entityweather.weather2.Example2;
import app.ayo.golf.retrofit.pojo.CourseItemWeather;
import app.ayo.golf.weatheractivity.DialogWeather;
import app.ayo.golf.R;
import app.ayo.golf.retrofit.pojo.CourseItem;

/**
 * Created by admin on 7/25/2017.
 */

public class CourseListAdapter extends RecyclerView.Adapter<CourseListAdapter.MyHolder> {
    private static final String ICON_PREFIX = "img_";
    ArrayList<Example2> data2;
    ArrayList<CourseItem> data;
    ArrayList<CourseItemWeather> data3 = new ArrayList<>();
    RecyclerView r;
    Activity c;
    Context context;
    CourseListAdapterListener listener;

    public void updateList(ArrayList<Example2> exampleList) {
        this.data2 = exampleList;
    }

    public void updateList2(ArrayList<CourseItem> courseList) {
        this.data = courseList;
    }

    public interface CourseListAdapterListener {
        void onBook(int courseIndex);
    }

    public CourseListAdapter(ArrayList<CourseItem> data, RecyclerView r, ArrayList<Example2> data2, Activity c) {
        this.data2 = data2;
        this.data = data;
        this.r = r;
        this.c = c;
    }

    public void setListener(CourseListAdapterListener listener) {
        this.listener = listener;
    }

    @Override
    public CourseListAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.custom_course_list, parent, false);
        final MyHolder holder = new MyHolder(v);
        holder.btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = holder.getAdapterPosition();
                CourseItem item = data.get(pos);
                Intent i = new Intent(view.getContext(), DetailCourseActivity.class);
                i.putExtra("course", item);
                view.getContext().startActivity(i);
            }
        });
        return holder;
    }


    @Override
    public void onBindViewHolder(CourseListAdapter.MyHolder holder, int position) {
        if (holder instanceof CourseListAdapter.MyHolder) {
            CourseListAdapter.MyHolder myHolder = (CourseListAdapter.MyHolder) holder;
            myHolder.textcourse.setText(data.get(position).course_name);
            myHolder.tv_city_weather.setText(data.get(position).course_city);
            myHolder.mTvWeatherTitle.setText(data.get(position).course_city);
            myHolder.mTvWeatherTitle.setTypeface(Typeface.DEFAULT_BOLD);
//            myHolder.iconWeatherAdapter.setImageResource(Util.getDrawableResourceIdByName(getApplicationContext(), ICON_PREFIX + data2.get(position).getList().get(position).getWeather().get(0).getIcon()));
//            .getList().get(position).getWeather().get(position).getIcon()));
//            holder.textaddress.setText(data.get(position).getAddress());

            myHolder.mBtnWeather.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    String cuaca = myHolder.tv_city_weather.getText().toString();
                    android.app.FragmentManager fm = c.getFragmentManager();
                    DialogWeather dialogFragment = new DialogWeather();
                    Bundle bundle = new Bundle();
                    bundle.putString("message", data.get(position).course_city);
                    dialogFragment.setArguments(bundle);
                    dialogFragment.show(fm, "sam");
                }
            });
            Glide.with(c)
                    .load(data.get(position).course_image)
                    .into(holder.image);

            if (position == 0 || position == 1) {
                holder.btnBook.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onBook(holder.getAdapterPosition());

                /*if (sesi.isLogin()){
                    int pos = holder.getAdapterPosition();
                    CourseItem item = data.get(pos);

                    Intent i = new Intent(v.getContext(),CoFormActivity.class);
                    i.putExtra("course", item);
                    v.getContext().startActivity(i);
                }
                else{
                    Intent i = new Intent(v.getContext(), LoginActivity.class);
                    c.startActivityForResult(i,201);
                }*/
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
    return data.size();
//        List<String> list = new ArrayList<String>();
//        list.addAll(Collections.singleton(String.valueOf(data)));
//        list.addAll(Collections.singleton(String.valueOf(data2)));
//        return list.size();
//        ArrayList list = new ArrayList();
//        list.addAll(new ArrayList(data));
//        list.addAll(new ArrayList(data2));
//        return list.size();
//        if (list == null || list.size()>0) {
//            return list.size();
//        } else {
//            return 0;
//        }
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView textcourse, mTvWeatherTitle, tv_city_weather;
        ImageView iconWeatherAdapter;
        Button btnBook;
        Button btnDetail;
        LinearLayout mBtnWeather;

        public MyHolder(View itemView) {
            super(itemView);
            iconWeatherAdapter = itemView.findViewById(R.id.icon_weather_adapter);
            image = (ImageView) itemView.findViewById(R.id.imgcourse);
            textcourse = (TextView) itemView.findViewById(R.id.txtcoursename);
            tv_city_weather = (TextView) itemView.findViewById(R.id.tv_city_weather);
            btnBook = (Button) itemView.findViewById(R.id.btnbook);
            btnDetail = (Button) itemView.findViewById(R.id.btndetail);
            mBtnWeather = itemView.findViewById(R.id.btn_weather);
            mTvWeatherTitle = itemView.findViewById(R.id.tv_weather_title);
        }
    }

//    public class CourseListWeather extends RecyclerView.Adapter<CourseListWeather.TwoViewHolder> {
//        ArrayList<Example2> data2;
//
//        public CourseListWeather(ArrayList<Example2> data2) {
//            this.data2 = data2;
//        }
//
//        @Override
//        public TwoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            View view = inflater.inflate(R.layout.custom_course_list, parent, false);
//            return new TwoViewHolder(view);
//        }
//
//        @Override
//        public void onBindViewHolder(TwoViewHolder holder, int position) {
//
//        }
//
//        @Override
//        public int getItemCount() {
//            return data2.size();
//        }
//
//        public void updateList(ArrayList<Example2> weathersList) {
//            this.data2 = weathersList;
//        }
//
//        public class TwoViewHolder extends RecyclerView.ViewHolder {
//            public TwoViewHolder(View itemView) {
//                super(itemView);
//            }
//        }
//    }
}