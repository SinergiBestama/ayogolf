package app.ayo.golf.adapter;

import android.app.LauncherActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import app.ayo.golf.FlightActivity;
import app.ayo.golf.R;
import app.ayo.golf.entitytiket.searchairport.Airport;

/**
 * Created by admin on 08/03/2018.
 */

public class SearchAirportTujuanAdapter extends RecyclerView.Adapter<SearchAirportTujuanAdapter.TujuanViewHolder> {

    private List<Airport> data;
    private FlightListener listener;

    public SearchAirportTujuanAdapter(List<Airport> data, FlightListener listener) {
        this.data = data;
        this.listener = listener;
    }

    @Override
    public TujuanViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.item_search_airport_tujuan, parent, false);

        return new TujuanViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TujuanViewHolder holder, int position) {
        Airport airport = data.get(position);
        final TujuanViewHolder tujuanViewHolder = (TujuanViewHolder) holder;
        tujuanViewHolder.tvAllAirportTujuan.setText(airport.getLocationName());
        tujuanViewHolder.tvCodeAirportTujuan.setText("(" + airport.getAirportCode() + ")");
        tujuanViewHolder.tvAirportNameTujuan.setText(airport.getAirportName());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void updateList(List<Airport> airports) {
        this.data = airports;
    }

    public interface FlightListener {
        void flightSelected(Airport airport);
    }

    @Override
    public void onBindViewHolder(TujuanViewHolder holder, int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
        listener.flightSelected(data.get(position));
    }

    public class TujuanViewHolder extends RecyclerView.ViewHolder {

        TextView tvAllAirportTujuan, tvCodeAirportTujuan, tvAirportNameTujuan;

        public TujuanViewHolder(final View itemView) {
            super(itemView);

            tvAllAirportTujuan = (TextView) itemView.findViewById(R.id.tv_allAirportTujuan);
            tvCodeAirportTujuan = (TextView) itemView.findViewById(R.id.tv_codeAirportTujuan);
            tvAirportNameTujuan = (TextView) itemView.findViewById(R.id.tv_airportNameTujuan);

            tvAllAirportTujuan.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    listener.flightSelected(data.get());
//                    //Toast.makeText(itemView.getContext(), "This : " + getAdapterPosition(), Toast.LENGTH_LONG).show();
//                    Intent searchAirportTujuan = new Intent(view.getContext(), FlightActivity.class);
//                    String airportKedatangan = tvAllAirportTujuan.getText().toString() + tvCodeAirportTujuan.getText().toString() + tvAirportNameTujuan.getText().toString();
//                    airportKedatangan = airportKedatangan.replaceAll("[^A-Z,a-z]", "-");
//                    searchAirportTujuan.putExtra("airportTujuan", airportKedatangan);
//                    view.getContext().startActivity(searchAirportTujuan);
//                }
//            });
        }
    }
}
