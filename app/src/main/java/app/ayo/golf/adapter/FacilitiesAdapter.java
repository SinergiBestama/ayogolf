package app.ayo.golf.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import app.ayo.golf.R;
import app.ayo.golf.retrofit.pojo.CourseItem;

/**
 * Created by arifina on 9/19/2017.
 */

public class FacilitiesAdapter  extends RecyclerView.Adapter<FacilitiesAdapter.MyHolder> {
    ArrayList<CourseItem.Amenities> data;
    Activity c;
    RecyclerView r;

    public FacilitiesAdapter(ArrayList<CourseItem.Amenities> data, Activity c, RecyclerView r) {
        this.data = data;
        this.c = c;
        this.r = r;
    }

    @Override
    public FacilitiesAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.custom_amenities,parent,false);
        return new MyHolder(v);
    }

    @Override
    public void onBindViewHolder(FacilitiesAdapter.MyHolder holder, int position) {
        Glide.with(c)
                .load(data.get(position).facility_image)
                .into(holder.image);
        holder.nama.setText(data.get(position).name);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView nama;
        public MyHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.imgamenities);
            nama = (TextView) itemView.findViewById(R.id.txtamenities);
        }
    }
}
