package app.ayo.golf.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import app.ayo.golf.DetailCourseActivity;
import app.ayo.golf.R;
import app.ayo.golf.retrofit.pojo.Course;

/**
 * Created by arifina on 7/25/2017.
 */

public class CourseAdapter extends RecyclerView.Adapter<CourseAdapter.MyHolder> {
    ArrayList<Course> data;
    RecyclerView r;
    Activity c;

    public CourseAdapter(ArrayList<Course> data, RecyclerView r, Activity c) {
        this.data = data;
        this.r = r;
        this.c = c;
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //array position
            int posisi = r.getChildLayoutPosition(v);
            Intent intent = new Intent(c, DetailCourseActivity.class);
            /*intent.putExtra("jdl",data.get(posisi).getJudul());
            intent.putExtra("desk",data.get(posisi).getDeskripsi());
            intent.putExtra("gbr",data.get(posisi).getGambar());*/
            c.startActivity(intent);
        }
    };

    @Override
    public CourseAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.custom_course_list, parent, false);

        v.setOnClickListener(listener);
        return new MyHolder(v);
    }

    @Override
    public void onBindViewHolder(CourseAdapter.MyHolder holder, int position) {
        holder.textcourse.setText(data.get(position).getCourse_name());
        //holder.textaddress.setText(data.get(position).getAddress());
        Glide.with(c)
                .load("http://117.54.3.25:1480/ayogolf/uploads/course/" + data.get(position).getCourse_image())
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView textcourse;

        //TextView textaddress;
        public MyHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.imgcourse);
            textcourse = (TextView) itemView.findViewById(R.id.txtcoursename);
            //textaddress = (TextView) itemView.findViewById(R.id.txtaddress);
        }
    }
}
