package app.ayo.golf.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import app.ayo.golf.retrofit.pojo.CourseItem;

/**
 * Created by arifina on 9/28/2017.
 */

public class GalleriesAdapter extends PagerAdapter{

    ArrayList<CourseItem.Gallery> gambar;
    Activity activity;

    public GalleriesAdapter(ArrayList<CourseItem.Gallery> gambar, Activity activity) {
        this.gambar = gambar;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return gambar.size();

    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(app.ayo.golf.R.layout.custom_list_banner, container, false);
        //TextView txt = (TextView) v.findViewById(R.id.textpager);
        ImageView img = (ImageView) v.findViewById(app.ayo.golf.R.id.imgbanner);

        //txt.setText(nama[position]);
        //img.setImageResource(gambar[position]);
        Glide.with(activity)
                .load(gambar.get(position).course_image)
                .into(img);

        container.addView(v);
        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==((LinearLayout)object);
    }
}
