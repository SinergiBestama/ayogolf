package app.ayo.golf.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import app.ayo.golf.CoFormActivity;
import app.ayo.golf.R;
import app.ayo.golf.retrofit.pojo.BookingCourseTransaction;

/**
 * Created by Hendi on 10/1/17.
 */

public class FlightButtonAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<BookingCourseTransaction.Flight> data;
    RecyclerView r;
    CoFormActivity c;
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy - HH:mm");
    Calendar cal = Calendar.getInstance();

    public FlightButtonAdapter(ArrayList<BookingCourseTransaction.Flight> data, RecyclerView r, Activity c) {
        this.data = data;
        this.r = r;
        this.c = (CoFormActivity) c;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.item_flight_button, parent, false);
        final FlightButtonVH holder = new FlightButtonVH(v);
        holder.btnFlight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                c.setCurrentFlight(holder.getAdapterPosition());
            }
        });
        holder.btnDeleteFlight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c.removeFlightAt(holder.getAdapterPosition());
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder vh, int position) {
        FlightButtonVH holder = (FlightButtonVH) vh;

        // set title
        holder.btnFlight.setText("Flight "+String.valueOf(data.get(position).flight_number));
        if(data.get(position).showing) {
            holder.btnFlight.setBackgroundResource(R.drawable.sh_rounded_color_primary);
            holder.btnFlight.setTextColor(Color.WHITE);
        }
        else {
            holder.btnFlight.setBackgroundResource(R.drawable.sh_rounded_color_outline);
            holder.btnFlight.setTextColor(ContextCompat.getColor(c, R.color.colorPrimary));
        }

        // hide/ show delete button
        if(position == data.size()-1 && data.size()>1){
            holder.btnDeleteFlight.setVisibility(View.VISIBLE);
        }
        else{
            holder.btnDeleteFlight.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class FlightButtonVH extends RecyclerView.ViewHolder {
        public TextView btnFlight;
        public View btnDeleteFlight;

        public FlightButtonVH(View itemView) {
            super(itemView);
            btnFlight = (TextView) itemView.findViewById(R.id.btn_flight);
            btnDeleteFlight = itemView.findViewById(R.id.btn_delete_flight);
        }
    }
}

