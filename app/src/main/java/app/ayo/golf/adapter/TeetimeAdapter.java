package app.ayo.golf.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import app.ayo.golf.R;
import app.ayo.golf.retrofit.pojo.TeeTime;

/**
 * Created by admin on 12/04/2018.
 */

public class TeetimeAdapter<T> extends BaseAdapter {

    private Activity activity;
    List<T> data;

    public TeetimeAdapter(Activity activity, List<T> data) {
        this.activity = activity;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.fragment_flight_page, null);

        TextView daysTeetime = convertView.findViewById(R.id.tv_day_teetime);

        TeeTime tt = (TeeTime) data;
        final MenuItem item = (MenuItem) data.get(position);
        daysTeetime.setText(tt.day);


        return convertView;
    }
}
