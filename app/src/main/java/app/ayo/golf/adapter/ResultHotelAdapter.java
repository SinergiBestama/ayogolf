package app.ayo.golf.adapter;

        import android.app.Activity;
        import android.content.Context;
        import android.content.Intent;
        import android.graphics.Typeface;
        import android.support.v7.widget.RecyclerView;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ImageView;
        import android.widget.RatingBar;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.bumptech.glide.Glide;
        import com.bumptech.glide.request.RequestOptions;

        import java.text.NumberFormat;
        import java.util.Currency;
        import java.util.List;
        import java.util.Locale;

        import app.ayo.golf.DetailHotel;
        import app.ayo.golf.R;
        import app.ayo.golf.entitytiket.hotelresult.Result;
        import app.ayo.golf.helper.Util;
        import butterknife.internal.Utils;

/**
 * Created by admin on 28/02/2018.
 */

public class ResultHotelAdapter extends RecyclerView.Adapter<ResultHotelAdapter.ViewHolder> {

    public List<Result> data;
    Activity c;

    public ResultHotelAdapter(List<Result> data) {
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_hotel_search, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Result hotelResult = data.get(position);
        final ViewHolder viewHolder = (ViewHolder) holder;

        if (data.get(position).getPhotoPrimary().isEmpty())
            holder.ivHotelResult.setImageResource(R.drawable.no_photo);
        else {
            RequestOptions opt = new RequestOptions().placeholder(R.drawable.no_photo);
            Glide.with(viewHolder.ivHotelResult.getContext())
                    .load(data.get(position).getPhotoPrimary())
                    .apply(opt)
                    .into(holder.ivHotelResult);

            viewHolder.tvNamaHotel.setText(hotelResult.getName());
            viewHolder.ratingHotelResult.setRating(Float.parseFloat(hotelResult.getStarRating()));
            viewHolder.tvHotelLocation.setText(hotelResult.getRegional());
            viewHolder.tvHargaHotel.setText("IDR " + hotelResult.getTotalPrice());
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void updateList(List<Result> resultListHotel) {
        this.data = resultListHotel;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView ivHotelResult;
        TextView tvNamaHotel, tvHotelLocation, tvHargaHotel;
        RatingBar ratingHotelResult;

        public ViewHolder(final View itemView) {
            super(itemView);

            ivHotelResult = (ImageView) itemView.findViewById(R.id.iv_imageHotel);
            tvNamaHotel = (TextView) itemView.findViewById(R.id.tv_namaHotel);
            tvNamaHotel.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            tvHotelLocation = (TextView) itemView.findViewById(R.id.tv_hotelLocation);
            tvHargaHotel = (TextView) itemView.findViewById(R.id.tv_hargaHotel);
            ratingHotelResult = (RatingBar) itemView.findViewById(R.id.ratingHotel);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent hotelLocation = new Intent(view.getContext(), DetailHotel.class);
                    String yourString = tvNamaHotel.getText().toString();
                    yourString = yourString.replaceAll("[^A-Z, a-z]", "");
                    hotelLocation.putExtra("Location", yourString);
//                    hotelLocation.putExtra("Location", tvHotelFind.getText().toString());
//                    hotelLocation.setType("text/plain");
                    view.getContext().startActivity(hotelLocation);
//                    Intent toDetailHotel = new Intent(view.getContext(), DetailHotel.class);
//                    view.getContext().startActivity(toDetailHotel);
                    //Toast.makeText(itemView.getContext(), "You Choose : " + getAdapterPosition(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}