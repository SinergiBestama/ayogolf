package app.ayo.golf.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import app.ayo.golf.R;
import app.ayo.golf.entitytiket.hoteldetailent.Photo;
import app.ayo.golf.retrofit.pojo.Banner;

/**
 * Created by admin on 09/03/2018.
 */

public class BannerHotelDetailAdapter extends PagerAdapter {

    List<Photo> photos;
    Activity activity;

    public BannerHotelDetailAdapter(List<Photo> photos, Activity activity) {
        this.photos = photos;
        this.activity = activity;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.custom_list_banner, container, false);
        return super.instantiateItem(container, position);
    }

    @Override
    public int getCount() {
        return photos.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return false;
    }
}
