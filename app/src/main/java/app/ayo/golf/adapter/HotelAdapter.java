package app.ayo.golf.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Path;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import app.ayo.golf.HotelActivity;
import app.ayo.golf.HotelLocation;
import app.ayo.golf.R;
import app.ayo.golf.entitytiket.findhotel.HotelFind;
import app.ayo.golf.retrofit.pojo.Hotel;

/**
 * Created by admin on 18/02/2018.
 */

public class HotelAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public List<HotelFind> data;

    public HotelAdapter(List<HotelFind> data) {
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_hotel_location, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        HotelFind hotel = data.get(position);
        final ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.tvHotelFind.setText(hotel.getLabel());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void updateList(List<HotelFind> hotels) {
        this.data = hotels;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvHotelFind;

        public ViewHolder(final View itemView) {
            super(itemView);

            tvHotelFind = (TextView) itemView.findViewById(R.id.tv_hotel_location);
            tvHotelFind.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent hotelLocation = new Intent(view.getContext(), HotelActivity.class);
                    String yourString = tvHotelFind.getText().toString();
                    yourString = yourString.replaceAll("[^A-Z, a-z]", "");
                    hotelLocation.putExtra("Location", yourString);
//                    hotelLocation.putExtra("Location", tvHotelFind.getText().toString());
//                    hotelLocation.setType("text/plain");
                    view.getContext().startActivity(hotelLocation);
                }
//                @Override
//                public void onClick(View view) {
////                    int pos = getAdapterPosition();
////
////                    if (pos != RecyclerView.NO_POSITION) {
////                        HotelFind clickedItem = data.get(pos);
//
//                        // 1. create an intent pass class name or intnet action name
//        Intent getHotel = new Intent("com.hmkcode.android.ANOTHER_ACTIVITY");
//
//        // 2. put key/value data
//        getHotel.putExtra("message", "Hello From MainActivity");
//
//        // 3. or you can add data to a bundle
//        Bundle extras = new Bundle();
//        extras.putString("status", "Data Received!");
//
//        // 4. add bundle to intent
//        getHotel.putExtras(extras);
//
//        // 5. start the activity
//
//
//
//
//                        //Toast.makeText(view.getContext(), "You Clicked"+ clickedItem.getLabel(),Toast.LENGTH_LONG).show();
//                    }
//                }
            });
        }
    }


}
