package app.ayo.golf;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Parcel;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telecom.Call;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import app.ayo.golf.adapter.ResultHotelAdapter;
import app.ayo.golf.helper.PermissionUtil;
import app.ayo.golf.helper.Util;
import app.ayo.golf.retrofit.pojo.APIResponse;
import app.ayo.golf.retrofit.pojo.BookingCourseResult;
import app.ayo.golf.retrofit.pojo.BookingCourseTransaction;
import app.ayo.golf.retrofit.pojo.DataBookingDetail;
import app.ayo.golf.retrofit.pojo.DetailFlight;
import app.ayo.golf.retrofit.service.ApiServices;
import app.ayo.golf.retrofit.service.InitRetrofit;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ReceiptActivity2 extends AppCompatActivity {
    @BindView(R.id.tv_course)
    TextView tvCourse;
    @BindView(R.id.tv_bookdate)
    TextView tvBookDate;
    @BindView(R.id.tv_caddy)
    TextView tvCaddy;
    @BindView(R.id.tv_flight)
    TextView tvFlight;
    @BindView(R.id.tv_player)
    TextView tvPlayer;
    @BindView(R.id.rv_flight_detail3)
    RecyclerView rvFlightDetail3;
    @BindView(R.id.tv_total)
    TextView tvTotal;
    @BindView(R.id.btn_confirm_payment)
    TextView btnConfirmPayment;
    private static final int REQUEST_PERMISSION = 222;
    DataBookingDetail dataBookingDetail;
    private DetailFlight getDetailFlight;
    BookingCourseResult mBookingCourseResult;
    private FlightDetailAdapter detailAdapter;
    private List<DataBookingDetail.DetailFlight> list = new ArrayList<>();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt2);
        ButterKnife.bind(this);

        //Init Action Bar
        Util.setCustomActionBar(this)
                .setTitle("Confirmation")
                .setLeftButton(R.drawable.button_back, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                })
                .setRightButton(null, null);

        dataBookingDetail = getIntent().getParcelableExtra("booking_detail");
//        mBookingCourseResult.trx_id = (String) getIntent().getSerializableExtra("detail_booking");
        btnConfirmPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toPaymentActivity = new Intent(ReceiptActivity2.this, Activity_Payment2.class);
                toPaymentActivity.putExtra("booking_result2", dataBookingDetail);
                startActivity(toPaymentActivity);
                finish();
            }
        });
        getBookingResult();

//        tvCourse.setText(courseResult.course_name);
//        tvBookDate.setText(Util.dateToString(new Date(courseResult.booked_date)));
//        if (courseResult.caddy != null) {
//            tvCaddy.setText(!TextUtils.isEmpty(courseResult.caddy) ?
//                    courseResult.caddy : "No caddy");
////            Glide.with(this)
//                    .load(courseResult.caddy)
//                    .into(ivCaddyPic);
//        }

//        rvFlightDetail3.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
//        rvFlightDetail3.setAdapter(new FlightDetailAdapter(list));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvFlightDetail3.setLayoutManager(layoutManager);
        rvFlightDetail3.setHasFixedSize(true);
        detailAdapter = new FlightDetailAdapter(list);
        rvFlightDetail3.setAdapter(detailAdapter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION) {
            if (PermissionUtil.hashPermission(this, permissions)) {
                picLoad();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void picLoad() {
        //Nothing Do AS
    }

    private void getBookingResult() {
//        ApiServices apiServices = InitRetrofit.getInstanceRetrofit();
//        retrofit2.Call<APIResponse<DataBookingDetail>> call = apiServices.bookedDetail("", "", "", "", "", "");
//        call.enqueue(new Callback<APIResponse<DataBookingDetail>>() {
//            @Override
//            public void onResponse(retrofit2.Call<APIResponse<DataBookingDetail>> call, Response<APIResponse<DataBookingDetail>> response) {
//                if (response.isSuccessful() && response.body().isSuccessful()) {
//                    Toast.makeText(getApplicationContext(), "Succes Juga", Toast.LENGTH_LONG).show();
//                }
//            }
//
//            @Override
//            public void onFailure(retrofit2.Call<APIResponse<DataBookingDetail>> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "Gagal" + t, Toast.LENGTH_LONG).show();
//            }
//        });
        tvCourse.setText(dataBookingDetail.getCourseName());
        tvCourse.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        tvBookDate.setText(dataBookingDetail.getBookedDate());
        tvBookDate.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        tvCaddy.setText(dataBookingDetail.getCaddy());
        tvCaddy.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        tvFlight.setText(dataBookingDetail.getFlightCount().toString() + " " + "Flight(s)");
        tvFlight.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        tvPlayer.setText(dataBookingDetail.getPlayer() + " " + "Player(s)");
        tvPlayer.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        long totalPrice = Long.parseLong(dataBookingDetail.getTotal());
        tvTotal.setText(Util.formatToCurrency(totalPrice));
        tvTotal.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
    }

    public class FlightDetailAdapter extends RecyclerView.Adapter<FlightDetailAdapter.DetailViewHolder> {

        private List<DataBookingDetail.DetailFlight> data = new ArrayList<>();

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }

        public FlightDetailAdapter(List<DataBookingDetail.DetailFlight> data) {
            this.data = data;
        }

        @Override
        public DetailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.item_flight_detail2, parent, false);
            return new DetailViewHolder(view);
        }

        @Override
        public void onBindViewHolder(DetailViewHolder holder, int position) {
            if (holder instanceof DetailViewHolder) {
                DetailViewHolder viewHolder = (DetailViewHolder) holder;
                DataBookingDetail.DetailFlight detailFlight1 = dataBookingDetail.getDetailFlight().get(position);
                viewHolder.tvFlightNumber2.setText("Flight " + detailFlight1.getFlightnumber());
                viewHolder.tvFlightNumber2.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                viewHolder.tvFlightTeeTime2.setText(detailFlight1.getTimeType());
                viewHolder.tvFlightTeeTime2.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                viewHolder.tvFlightPrice2.setText(Util.formatToCurrency(Long.valueOf(detailFlight1.getPrice())));
                viewHolder.tvFlightPrice2.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                viewHolder.tvFlightPlayer2.setText(detailFlight1.getPlayer() + " Player(s)");
                viewHolder.tvFlightPlayer2.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
                viewHolder.tvFlightTotal2.setText(Util.formatToCurrency(Long.valueOf(detailFlight1.getTotal())));
                viewHolder.tvFlightTotal2.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            }
        }

        @Override
        public int getItemCount() {
            return dataBookingDetail.getDetailFlight().size();
        }

        public class DetailViewHolder extends RecyclerView.ViewHolder {

            TextView tvFlightNumber2, tvFlightTeeTime2, tvFlightPrice2, tvFlightPlayer2, tvFlightTotal2;

            public DetailViewHolder(View itemView) {
                super(itemView);

                tvFlightNumber2 = itemView.findViewById(R.id.tv_flight_number2);
                tvFlightTeeTime2 = itemView.findViewById(R.id.tv_flight_teetime2);
                tvFlightPrice2 = itemView.findViewById(R.id.tv_flight_price2);
                tvFlightPlayer2 = itemView.findViewById(R.id.tv_flight_player2);
                tvFlightTotal2 = itemView.findViewById(R.id.tv_flight_total2);
            }
        }
    }
}
